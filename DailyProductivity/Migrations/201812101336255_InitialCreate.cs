namespace DailyProductivity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Agings",
                c => new
                    {
                        AgingID = c.Long(nullable: false, identity: true),
                        EmployeeID = c.Long(nullable: false),
                        PracticeID = c.Long(nullable: false),
                        DepartmentID = c.Long(nullable: false),
                        A_30 = c.Decimal(nullable: false, precision: 18, scale: 2),
                        A_60 = c.Decimal(nullable: false, precision: 18, scale: 2),
                        A_90 = c.Decimal(nullable: false, precision: 18, scale: 2),
                        A_120 = c.Decimal(nullable: false, precision: 18, scale: 2),
                        A_120_up = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Issues_Of_120_Up_Claims = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AgingDateTime = c.DateTime(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        LastUpdated = c.DateTime(nullable: false),
                        UpdatedBy = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.AgingID);
            
            CreateTable(
                "dbo.Assignments",
                c => new
                    {
                        AssignmentID = c.Long(nullable: false, identity: true),
                        EmployeeID = c.Long(nullable: false),
                        PracticeID = c.Long(nullable: false),
                        EnteredClaims = c.Long(nullable: false),
                        Eligibility = c.Long(nullable: false),
                        PF_eBridge = c.Long(nullable: false),
                        RejectionsEmdeon = c.Long(nullable: false),
                        RejectionsAvaility = c.Long(nullable: false),
                        ERADenials = c.Long(nullable: false),
                        ERAUnmatched = c.Long(nullable: false),
                        Appeals = c.Long(nullable: false),
                        Calls = c.Long(nullable: false),
                        AgingClaims = c.Long(nullable: false),
                        PTStatements = c.Long(nullable: false),
                        EOBpayment = c.Long(nullable: false),
                        Rejections = c.Long(nullable: false),
                        PTLResponses = c.Long(nullable: false),
                        Emails = c.Long(nullable: false),
                        EmailVerification = c.Long(nullable: false),
                        FaxedVerification = c.Long(nullable: false),
                        WebVerification = c.Long(nullable: false),
                        Others = c.Long(nullable: false),
                        DepartmentID = c.Long(nullable: false),
                        PendingClaims = c.Long(nullable: false),
                        PaidClaims = c.Long(nullable: false),
                        PreparedClaims = c.Long(nullable: false),
                        PreparedEncounters = c.Long(nullable: false),
                        MissingLog = c.Long(nullable: false),
                        QA = c.Long(nullable: false),
                        Multiple = c.Long(nullable: false),
                        NoPatientBill = c.Long(nullable: false),
                        UVTotalClaims = c.Long(nullable: false),
                        Done = c.Long(nullable: false),
                        BillingETC = c.Long(nullable: false),
                        ManuallyAdjustments = c.Long(nullable: false),
                        VerificationClaims = c.Long(nullable: false),
                        BillingAnalysis = c.Long(nullable: false),
                        ReceivedBills = c.Long(nullable: false),
                        AssignmentDateTime = c.DateTime(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        LastUpdated = c.DateTime(nullable: false),
                        UpdatedBy = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.AssignmentID);
            
            CreateTable(
                "dbo.Credentialing",
                c => new
                    {
                        CredentialingID = c.Long(nullable: false, identity: true),
                        EmployeeID = c.Long(nullable: false),
                        PracticeID = c.Long(nullable: false),
                        CredentialingCalls = c.Long(nullable: false),
                        CredentialingDocuments = c.Long(nullable: false),
                        EDICalls = c.Long(nullable: false),
                        EDISetup = c.Long(nullable: false),
                        EFTCalls = c.Long(nullable: false),
                        EFTSetup = c.Long(nullable: false),
                        ERACalls = c.Long(nullable: false),
                        ERASetup = c.Long(nullable: false),
                        Emails = c.Long(nullable: false),
                        EnrollmentTrackingSheet = c.Long(nullable: false),
                        WebLogins = c.Long(nullable: false),
                        CredentialingDateTime = c.DateTime(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        LastUpdated = c.DateTime(nullable: false),
                        UpdatedBy = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.CredentialingID);
            
            CreateTable(
                "dbo.Departments",
                c => new
                    {
                        DepartmentID = c.Long(nullable: false, identity: true),
                        DepartmentName = c.String(),
                    })
                .PrimaryKey(t => t.DepartmentID);
            
            CreateTable(
                "dbo.EmployeePractices",
                c => new
                    {
                        EmployeePracticesID = c.Long(nullable: false, identity: true),
                        EmployeeID = c.Long(nullable: false),
                        PracticesID = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.EmployeePracticesID);
            
            CreateTable(
                "dbo.Employees",
                c => new
                    {
                        EmployeeID = c.Long(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        UserName = c.String(),
                        Password = c.String(),
                        Email = c.String(),
                        Phone = c.String(),
                        EmployeeNo = c.String(),
                        Designation = c.String(),
                        RoleID = c.Long(nullable: false),
                        DepartmentID = c.Long(nullable: false),
                        ShiftTiming = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        UserPic = c.String(),
                    })
                .PrimaryKey(t => t.EmployeeID);
            
            CreateTable(
                "dbo.Financial",
                c => new
                    {
                        FinancialID = c.Long(nullable: false, identity: true),
                        EmployeeID = c.Long(nullable: false),
                        PracticeID = c.Long(nullable: false),
                        DailyCharges = c.Decimal(nullable: false, precision: 18, scale: 2),
                        DailyPayments = c.Decimal(nullable: false, precision: 18, scale: 2),
                        MTDCharges = c.Decimal(nullable: false, precision: 18, scale: 2),
                        MTDPayments = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AgingAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        DailyChargesECW = c.Decimal(nullable: false, precision: 18, scale: 2),
                        DailyPaymentsECW = c.Decimal(nullable: false, precision: 18, scale: 2),
                        DailyPaymentseTeClinic = c.Decimal(nullable: false, precision: 18, scale: 2),
                        MTDPaymentseTeClinic = c.Decimal(nullable: false, precision: 18, scale: 2),
                        MTDPaymentsECW = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AgingAmounteTeClinic = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AgingAmounteCW = c.Decimal(nullable: false, precision: 18, scale: 2),
                        EnteredChargeI = c.Decimal(nullable: false, precision: 18, scale: 2),
                        EnteredChargeP = c.Decimal(nullable: false, precision: 18, scale: 2),
                        DepartmentID = c.Long(nullable: false),
                        FinancialDateTime = c.DateTime(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        LastUpdated = c.DateTime(nullable: false),
                        UpdatedBy = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.FinancialID);
            
            CreateTable(
                "dbo.Managers",
                c => new
                    {
                        ManagerID = c.Long(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        UserName = c.String(),
                        Password = c.String(),
                        Email = c.String(),
                        Phone = c.String(),
                        EmployeeNo = c.String(),
                        Designation = c.String(),
                        RoleID = c.Long(nullable: false),
                        ShiftTiming = c.Long(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ManagerID);
            
            CreateTable(
                "dbo.Notificatoins",
                c => new
                    {
                        NotificationID = c.Long(nullable: false, identity: true),
                        Time = c.DateTime(nullable: false),
                        EmployeeID = c.Long(nullable: false),
                        Message = c.String(),
                        Link = c.String(),
                        IsRead = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.NotificationID);
            
            CreateTable(
                "dbo.PatientHelpDesk",
                c => new
                    {
                        PHDID = c.Long(nullable: false, identity: true),
                        EmployeeID = c.Long(nullable: false),
                        PracticeID = c.Long(nullable: false),
                        AVM = c.Long(nullable: false),
                        Emails = c.Long(nullable: false),
                        IC = c.Long(nullable: false),
                        ICIC = c.Long(nullable: false),
                        ICPC = c.Long(nullable: false),
                        IS = c.Long(nullable: false),
                        LVM = c.Long(nullable: false),
                        OGIC = c.Long(nullable: false),
                        OGPC = c.Long(nullable: false),
                        PC = c.Long(nullable: false),
                        PS = c.Long(nullable: false),
                        Pendings = c.Long(nullable: false),
                        PatientHelpDeskDateTime = c.DateTime(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        LastUpdated = c.DateTime(nullable: false),
                        UpdatedBy = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PHDID);
            
            CreateTable(
                "dbo.PhdAssignments",
                c => new
                    {
                        AssignmentID = c.Long(nullable: false, identity: true),
                        AssignmentName = c.String(),
                        AssignmentShortName = c.String(),
                    })
                .PrimaryKey(t => t.AssignmentID);
            
            CreateTable(
                "dbo.PPFinancials",
                c => new
                    {
                        FinancialID = c.Long(nullable: false, identity: true),
                        EmployeeID = c.Long(nullable: false),
                        PracticeID = c.Long(nullable: false),
                        PF_eBridge = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Rejections = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ERADenials = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ERAUnmatched = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Appeals = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ManuallyAdjustments = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AgingClaims = c.Decimal(nullable: false, precision: 18, scale: 2),
                        BillingETC = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Emails = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PTStatements = c.Decimal(nullable: false, precision: 18, scale: 2),
                        VerificationClaims = c.Decimal(nullable: false, precision: 18, scale: 2),
                        DepartmentID = c.Long(nullable: false),
                        FinancialDateTime = c.DateTime(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        LastUpdated = c.DateTime(nullable: false),
                        UpdatedBy = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.FinancialID);
            
            CreateTable(
                "dbo.Practices",
                c => new
                    {
                        PracticeID = c.Long(nullable: false, identity: true),
                        PracticeName = c.String(),
                        PracticeShortName = c.String(),
                        CategoryID = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PracticeID);
            
            CreateTable(
                "dbo.PracticesCategories",
                c => new
                    {
                        CategoryID = c.Long(nullable: false, identity: true),
                        CategoryName = c.String(),
                    })
                .PrimaryKey(t => t.CategoryID);
            
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        RoleID = c.Long(nullable: false, identity: true),
                        RoleName = c.String(),
                    })
                .PrimaryKey(t => t.RoleID);
            
            CreateTable(
                "dbo.ShiftTimings",
                c => new
                    {
                        STimingID = c.Long(nullable: false, identity: true),
                        Title = c.String(),
                    })
                .PrimaryKey(t => t.STimingID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ShiftTimings");
            DropTable("dbo.Roles");
            DropTable("dbo.PracticesCategories");
            DropTable("dbo.Practices");
            DropTable("dbo.PPFinancials");
            DropTable("dbo.PhdAssignments");
            DropTable("dbo.PatientHelpDesk");
            DropTable("dbo.Notificatoins");
            DropTable("dbo.Managers");
            DropTable("dbo.Financial");
            DropTable("dbo.Employees");
            DropTable("dbo.EmployeePractices");
            DropTable("dbo.Departments");
            DropTable("dbo.Credentialing");
            DropTable("dbo.Assignments");
            DropTable("dbo.Agings");
        }
    }
}
