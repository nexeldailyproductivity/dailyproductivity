﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DailyProductivity.Models.ViewModels
{
    public class VMAgings
    {
        public decimal A_30 { get; set; }
        public decimal A_60 { get; set; }
        public decimal A_90 { get; set; }
        public decimal A_120 { get; set; }
        public decimal A_120_up { get; set; }
        public decimal Issues_Of_120_Up_Claims { get; set; }
    }
}