﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DailyProductivity.Models.ViewModels
{
    public class VMCredentialingCount
    {
        public long CredentialingCalls { get; set; }
        public long CredentialingDocuments { get; set; }
        public long EDICalls { get; set; }
        public long EDISetup { get; set; }
        public long EFTCalls { get; set; }
        public long EFTSetup { get; set; }
        public long ERACalls { get; set; }
        public long ERASetup { get; set; }
        public long Emails { get; set; }
        public long EnrollmentTrackingSheet { get; set; }
        public long WebLogins { get; set; }
    }
}