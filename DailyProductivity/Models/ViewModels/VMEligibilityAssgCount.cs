﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DailyProductivity.Models.ViewModels
{
    public class VMEligibilityCount
    {
        public string PracticeName {get;set;}
        public long EmailVerification { get; set; }
        public long FaxedVerification { get; set; }
        public long WebVerification { get; set; }
        public long Calls { get; set; }
        public long Others { get; set; }
    }
}