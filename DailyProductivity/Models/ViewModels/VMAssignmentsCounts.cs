﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DailyProductivity.Models.ViewModels
{
    public class AssignmentsCounts
    {
        public string Month { get; set; }
        public long EnteredClaims { get; set; }
        public long Eligibility { get; set; }
        public long PF_eBridge { get; set; }

        public long ERADenials { get; set; }
        public long ERAUnmatched { get; set; }
        public long Appeals { get; set; }
        public long Calls { get; set; }
        public long AgingClaims { get; set; }
        public long Email { get; set; }

        public long PracticeID { get; set; }
        public string PracticeName { get; set; }
     
        public long RejectionsEmdeon { get; set; }
        public long RejectionsAvaility { get; set; }

        public long PTStatements { get; set; }
        public long EOBpayment { get; set; }
        public long Rejections { get; set; }
        public long PTLResponses { get; set; }
        public long Emails { get; set; }
        public long EmailVerification { get; set; }
        public long FaxedVerification { get; set; }
        public long WebVerification { get; set; }
        public long Others { get; set; }



        public long CredentialingCalls { get; set; }
        public long CredentialingDocuments { get; set; }
        public long EDICalls { get; set; }
        public long EDISetup { get; set; }
        public long EFTCalls { get; set; }
        public long EFTSetup { get; set; }
        public long ERACalls { get; set; }
        public long ERASetup { get; set; }
        public long EnrollmentTrackingSheet { get; set; }
        public long WebLogins { get; set; }

        public long AVM { get; set; }
       
        public long IC { get; set; }
        public long ICIC { get; set; }
        public long ICPC { get; set; }
        public long IS { get; set; }
        public long LVM { get; set; }
        public long OGIC { get; set; }
        public long OGPC { get; set; }
        public long PC { get; set; }
        public long PS { get; set; }
        public long Pendings { get; set; }


        public long PendingClaims { get; set; }
        public long PaidClaims { get; set; }
        public long PreparedClaims { get; set; }
        public long PreparedEncounters { get; set; }
        public long MissingLog { get; set; }
        public long QA { get; set; }

        public long Multiple { get; set; }
        public long NoPatientBill { get; set; }
        public long UVTotalClaims { get; set; }
        public long Done { get; set; }

        public long BillingETC { get; set; }
        public long ManuallyAdjustments { get; set; }
        public long VerificationClaims { get; set; }


        public long BillingAnalysis { get; set; }

        public long ReceivedBills { get; set; }
    }
}