﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DailyProductivity.Models.ViewModels
{
    public class BillingEmail
    {
        public long EnteredClaims {get;set;}
        public long PendingClaims { get; set; }
        public long PaidClaims { get; set; }
        public long PreparedClaims { get; set; }
        public long PreparedEncounters { get; set; }
        public long MissingLog { get; set; }
        public long QA { get; set; }
        public long Emails { get; set; }

        public long BillingAnalysis { get; set; }

        public long ReceivedBills { get; set; }

        public long Eligibility { get; set; }

        public decimal EnteredChargeI { get; set; }
        public decimal EnteredChargeP { get; set; }
        public DateTime WorkDate { get; set; }
        public DateTime DateOfService { get; set; }
        public long PracticeID { get; set; }
    }
}