﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DailyProductivity.Models.ViewModels
{
    public class VMUserDashboardChargesSummary
    {
        public long PracticeID { get; set; }
        public string PracticeName { get; set; }
        public decimal DailyCharges { get; set; }
        public decimal DailyPayments { get; set; }
        public decimal AgingAmount { get; set; }
        public string Month { get; set; }
        public int MonthValue { get; set; }
    }
}