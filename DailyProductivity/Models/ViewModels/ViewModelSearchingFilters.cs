﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DailyProductivity.Models.ViewModels
{
    public class Filters
    {
        public Departments Department { get; set; }
        public Employees Employee { get; set; }
        public long AssType { get; set; }
        public DateTime FilterDateTime { get; set; }
        public Filters()
        {
            Department = new Departments();
            Employee = new Employees();
            //Department.DepartmentID = 0;
            AssType = 1;
            FilterDateTime = DateTime.Now;
        }
    }

   
}