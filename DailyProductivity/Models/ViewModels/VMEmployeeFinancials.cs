﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DailyProductivity.Models.ViewModels
{
    public class VMFinancials
    {
        public decimal DailyCharges { get; set; }
        public decimal DailyPayments { get; set; }
        public decimal MTDCharges { get; set; }
        public decimal MTDPayments { get; set; }
        public decimal AgingAmount { get; set; }
        public decimal DailyChargesECW { get; set; }
        public decimal DailyPaymentsECW { get; set; }
        public decimal DailyPaymentseTeClinic { get; set; }
        public decimal MTDPaymentseTeClinic { get; set; }
        public decimal MTDPaymentsECW { get; set; }
        public decimal AgingAmounteTeClinic { get; set; }
        public decimal AgingAmounteCW { get; set; }

        public decimal EnteredChargeI { get; set; }
        public decimal EnteredChargeP { get; set; }


        public decimal Rejections { get; set; }
        public decimal ERADenials { get; set; }
        public decimal ERAUnmatched { get; set; }
        public decimal Appeals { get; set; }
        public decimal ManuallyAdjustments { get; set; }
        public decimal AgingClaims { get; set; }
        public decimal BillingETC { get; set; }

        public decimal Emails { get; set; }
        public decimal PTStatements { get; set; }
        public decimal VerificationClaims { get; set; }
        public decimal PF_eBridge { get; set; }


    }
}