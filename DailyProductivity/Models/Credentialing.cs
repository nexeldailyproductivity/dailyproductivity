﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DailyProductivity.Models
{
    public partial class Credentialing
    {
        [Key]
        public long CredentialingID { get; set; }
        public long EmployeeID { get; set; }
        public long PracticeID { get; set; }
        public long CredentialingCalls { get; set; }
        public long CredentialingDocuments { get; set; }
        public long EDICalls { get; set; }
        public long EDISetup { get; set; }
        public long EFTCalls { get; set; }
        public long EFTSetup { get; set; }
        public long ERACalls { get; set; }
        public long ERASetup { get; set; }
        public long Emails { get; set; }
        public long EnrollmentTrackingSheet { get; set; }
        public long WebLogins { get; set; }
        public DateTime CredentialingDateTime { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastUpdated { get; set; }
        public long UpdatedBy { get; set; }
        
        public Credentialing()
        {
            this.CreatedDate = DateTime.Now;
            this.LastUpdated = DateTime.UtcNow ;
        }

        public long CredentialingSum()
        {
            
                return (this.CredentialingCalls + this.CredentialingDocuments + this.EDICalls + this.EDISetup + this.EFTCalls + this.EFTSetup + this.ERACalls + this.ERASetup + 
                    this.Emails + this.EnrollmentTrackingSheet + this.WebLogins
                    );
           
         
        }
    }
}