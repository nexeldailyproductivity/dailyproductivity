﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DailyProductivity.Models
{
    public class EmployeePractices
    {
        [Key]
        public long EmployeePracticesID { get; set; }

        public long EmployeeID { get; set; }


        public long PracticesID { get; set; }
    }
}