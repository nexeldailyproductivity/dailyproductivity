﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DailyProductivity.Models
{
    public partial class Employees
    {
        [Key]
        public long EmployeeID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string EmployeeNo { get; set; }
        public string Designation { get; set; }
        public long RoleID { get; set; }
        public long DepartmentID { get; set; }
        public int ShiftTiming { get; set; }
        public bool IsActive { get; set; }
        public string UserPic { get; set; }
    }
    public class Managers
    {
        [Key]
        public long ManagerID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string EmployeeNo { get; set; }
        public string Designation { get; set; }
        public long RoleID { get; set; }
        public long ShiftTiming { get; set; }
        public bool IsActive { get; set; }
    }
   
}