﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DailyProductivity.Models
{
    public class Eligibility
    {
        [Key]
        public long EligibilityID { get; set; }
        public long EmployeeID { get; set; }
        public long PracticeID { get; set; }
        public long Calls { get; set; }
        public long EmailVerification { get; set; }
        public long FaxedVerification { get; set; }
        public long Others { get; set; }
        public long WebVerification { get; set; }
        public DateTime EligibilityDateTime { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastUpdated { get; set; }
        public long UpdatedBy { get; set; }

        public Eligibility()
        {
            this.CreatedDate = DateTime.Now;
            this.LastUpdated = DateTime.UtcNow;
        }
    }
}