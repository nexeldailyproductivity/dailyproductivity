﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DailyProductivity.Models
{
    public partial class Notificatoins
    {
        [Key]
        public long NotificationID { get; set; }
        public DateTime Time { get; set; }
        public long EmployeeID { get; set; }
        public string Message { get; set; }
        public string Link { get; set; }
        public bool IsRead { get; set; }
       public Notificatoins()
        {
            this.Time = DateTime.Now;
        }
    }
}