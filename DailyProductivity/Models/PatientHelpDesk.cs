﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DailyProductivity.Models
{
    public class PatientHelpDesk
    {
        [Key]
        public long PHDID { get; set; }
        public long EmployeeID { get; set; }
        public long PracticeID { get; set; }
        public long AVM { get; set; }
        public long Emails { get; set; }
        public long IC { get; set; }
        public long ICIC { get; set; }
        public long ICPC { get; set; }
        public long IS { get; set; }
        public long LVM { get; set; }
        public long OGIC { get; set; }
        public long OGPC { get; set; }
        public long PC { get; set; }
        public long PS { get; set; }
        public long Pendings { get; set; }

        public DateTime PatientHelpDeskDateTime { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastUpdated { get; set; }
        public long UpdatedBy { get; set; }

      
        public PatientHelpDesk()
        {
          
            this.CreatedDate = DateTime.Now;
            this.LastUpdated = DateTime.UtcNow;
        }
    }
    //public class PatientHelpDesk
    //{
    //    [Key]
    //    public long PHDID { get; set; }
    //    public long UserID { get; set; }
    //    public long PracticeID { get; set; }
    //    public long AVM_Affordable { get; set; }
    //    public long AVM_Brace { get; set; }
    //    public long AVM_ETC { get; set; }
    //    public long AVM_MAMEdical { get; set; }
    //    public long AVM_MEP { get; set; }
    //    public long AVM_PEP { get; set; }
    //    public long AVM_UCMDS { get; set; }
    //    public long AVM_Webster { get; set; }
    //    public long AVM_Xenialz { get; set; }
    //    public long Emails_Affordable { get; set; }
    //    public long Emails_Brace { get; set; }
    //    public long Emails_ETC { get; set; }
    //    public long Emails_MAMEdical { get; set; }
    //    public long Emails_MEP { get; set; }
    //    public long Emails_PEP { get; set; }
    //    public long Emails_UCMDS { get; set; }
    //    public long Emails_Webster { get; set; }
    //    public long Emails_Xenialz { get; set; }
    //    public long ICIC_Affordable { get; set; }
    //    public long ICIC_Brace { get; set; }
    //    public long ICIC_ETC { get; set; }
    //    public long ICIC_MAMEdical { get; set; }
    //    public long ICIC_MEP { get; set; }
    //    public long ICIC_PEP { get; set; }
    //    public long ICIC_UCMDS { get; set; }
    //    public long ICIC_Webster { get; set; }
    //    public long ICIC_Xenialz { get; set; }
    //    public long ICPC_Affordable { get; set; }
    //    public long ICPC_Brace { get; set; }
    //    public long ICPC_ETC { get; set; }
    //    public long ICPC_MAMEdical { get; set; }
    //    public long ICPC_MEP { get; set; }
    //    public long ICPC_PEP { get; set; }
    //    public long ICPC_UCMDS { get; set; }
    //    public long ICPC_Webster { get; set; }
    //    public long ICPC_Xenialz { get; set; }
    //    public long IC_Affordable { get; set; }
    //    public long IC_Brace { get; set; }
    //    public long IC_ETC { get; set; }
    //    public long IC_MAMEdical { get; set; }
    //    public long IC_MEP { get; set; }
    //    public long IC_PEP { get; set; }
    //    public long IC_UCMDS { get; set; }
    //    public long IC_Webster { get; set; }
    //    public long IC_Xenialz { get; set; }
    //    public long IS_Affordable { get; set; }
    //    public long IS_Brace { get; set; }
    //    public long IS_ETC { get; set; }
    //    public long IS_MAMEdical { get; set; }
    //    public long IS_MEP { get; set; }
    //    public long IS_PEP { get; set; }
    //    public long IS_UCMDS { get; set; }
    //    public long IS_Webster { get; set; }
    //    public long IS_Xenialz { get; set; }
    //    public long LVM_Affordable { get; set; }
    //    public long LVM_Brace { get; set; }
    //    public long LVM_ETC { get; set; }
    //    public long LVM_MAMEdical { get; set; }
    //    public long LVM_MEP { get; set; }
    //    public long LVM_PEP { get; set; }
    //    public long LVM_UCMDS { get; set; }
    //    public long LVM_Webster { get; set; }
    //    public long LVM_Xenialz { get; set; }
    //    public long OGIC_Affordable { get; set; }
    //    public long OGIC_Brace { get; set; }
    //    public long OGIC_ETC { get; set; }
    //    public long OGIC_MAMEdical { get; set; }
    //    public long OGIC_MEP { get; set; }
    //    public long OGIC_PEP { get; set; }
    //    public long OGIC_UCMDS { get; set; }
    //    public long OGIC_Webster { get; set; }
    //    public long OGIC_Xenialz { get; set; }
    //    public long OGPC_Affordable { get; set; }
    //    public long OGPC_Brace { get; set; }
    //    public long OGPC_ETC { get; set; }
    //    public long OGPC_MAMEdical { get; set; }
    //    public long OGPC_MEP { get; set; }
    //    public long OGPC_PEP { get; set; }
    //    public long OGPC_UCMDS { get; set; }
    //    public long OGPC_Webster { get; set; }
    //    public long OGPC_Xenialz { get; set; }
    //    public long PC_Affordable { get; set; }
    //    public long PC_Brace { get; set; }
    //    public long PC_ETC { get; set; }
    //    public long PC_MAMEdical { get; set; }
    //    public long PC_MEP { get; set; }
    //    public long PC_PEP { get; set; }
    //    public long PC_UCMDS { get; set; }
    //    public long PC_Webster { get; set; }
    //    public long PC_Xenialz { get; set; }
    //    public long PS_Affordable { get; set; }
    //    public long PS_Brace { get; set; }
    //    public long PS_ETC { get; set; }
    //    public long PS_MAMEdical { get; set; }
    //    public long PS_MEP { get; set; }
    //    public long PS_PEP { get; set; }
    //    public long PS_UCMDS { get; set; }
    //    public long PS_Webster { get; set; }
    //    public long PS_Xenialz { get; set; }
    //    public DateTime PatientHelpDeskDateTime { get; set; }
    //    public DateTime CreatedDate { get; set; }
    //    public DateTime LastUpdated { get; set; }
    //    public long UpdatedBy { get; set; }

    //    public long Pendings_UCMDS { get; set; }

    //    public long Pendings_ETC { get; set; }
    //    public long Pendings_MA { get; set; }
    //    public long Pendings_Webster { get; set; }

    //    public long Pendings_AUC { get; set; }
    //    public bool IsPHDCalls { get; set; }
    //    public PatientHelpDesk()
    //    {
    //        this.CreatedDate = DateTime.Now;
    //        this.LastUpdated = DateTime.UtcNow;
    //    }
    //}
}