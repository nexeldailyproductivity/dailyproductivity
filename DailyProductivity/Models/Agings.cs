﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DailyProductivity.Models
{
    public class Agings
    {
        [Key]
        public long AgingID { get; set; }
        public long EmployeeID { get; set; }
        public long PracticeID { get; set; }
        public long DepartmentID { get; set; }
     public decimal A_30 { get; set; }
        public decimal A_60 { get; set; }
        public decimal A_90 { get; set; }
        public decimal A_120 { get; set; }
        public decimal A_120_up { get; set; }
        public decimal Issues_Of_120_Up_Claims { get; set; }
        public DateTime AgingDateTime { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastUpdated { get; set; }
        public long UpdatedBy { get; set; }

        public Agings()
        {
            this.CreatedDate = DateTime.Now;
            this.LastUpdated = DateTime.UtcNow;
        }
        public decimal AgingsSum()
        {
            return (A_30 + A_60+ A_90 + A_120+ A_120_up);
        }

    }
}