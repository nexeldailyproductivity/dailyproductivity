﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DailyProductivity.Models
{
    public partial class ShiftTimings
    {
        [Key]
        public long STimingID { get; set; }
        public string Title { get; set; }

    }
}