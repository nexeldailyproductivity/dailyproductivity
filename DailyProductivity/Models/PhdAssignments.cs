﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DailyProductivity.Models
{
    public class PhdAssignments
    {
        [Key]
        public long AssignmentID { get; set; }
        public string AssignmentName { get; set; }
        public string AssignmentShortName { get; set; }
    }
}