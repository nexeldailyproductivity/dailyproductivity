﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DailyProductivity.Models
{
    public class UsersMetaData
    {
        [Display(Name = "Username")]
        //  [MinLength(3, ErrorMessage = "Username must be atleast 3 characters")]
        //  [MaxLength(20, ErrorMessage = "Username must no exceed 20 characters")]
        [Required(ErrorMessage = "Please enter username")]
        public string UserName { get; set; }
        [Display(Name = "First Name")]
        //  [MinLength(3, ErrorMessage = "First Name must be atleast 3 characters")]
        //  [MaxLength(20, ErrorMessage = "First Name must no exceed 20 characters")]
        [Required(ErrorMessage = "Please enter first name")]
        public string FirstName { get; set; }
        [Display(Name = "Employee Number")]
        //  [MinLength(5, ErrorMessage = "Employee Number must be atleast 5 characters")]
        //  [MaxLength(10, ErrorMessage = "Employee Number must no exceed 10 characters")]
        [Required(ErrorMessage = "Please enter employee number")]
        public string EmployeeNo { get; set; }
        [Display(Name = "Employee Designation")]
        //  [MinLength(5, ErrorMessage = "Employee Designation must be atleast 5 characters")]
        //  [MaxLength(10, ErrorMessage = "Employee Designation must no exceed 10 characters")]
        [Required(ErrorMessage = "Please enter employee designation")]
        public string Designation { get; set; }
        [Display(Name = "Password")]
        //  [MinLength(5, ErrorMessage = "Password must be atleast 5 characters")]
        //  [MaxLength(10, ErrorMessage = "Password must no exceed 10 characters")]
        [Required(ErrorMessage = "Please enter password")]
        public string Password { get; set; }
        [Display(Name = "Email")]
        // [MinLength(5, ErrorMessage = "Email must be atleast 5 characters")]
        [Required(ErrorMessage = "Please enter email")]
        public string Email { get; set; }
        [Display(Name = "Phonenumber")]
        //  [MinLength(11, ErrorMessage = "Phonenumber must be atleast 11 characters")]
        //   [MaxLength(11, ErrorMessage = "Phonenumber must no exceed 11 characters")]
        [Required(ErrorMessage = "Please enter phonenumber")]
        public long Phone { get; set; }
        [Display(Name = "City name")]
        // [MinLength(3, ErrorMessage = "City name must be atleast 3 characters")]
        [Required(ErrorMessage = "Please enter city")]
        public string City { get; set; }
        [Display(Name = "Address")]
        //    [MinLength(5, ErrorMessage = "City name must be atleast 5  characters")]
        [Required(ErrorMessage = "Please enter valid address")]
        public string Address { get; set; }
        [Display(Name = "Role")]

        //   [Required(ErrorMessage = "Please enter valid role")]
        public long Role { get; set; }



        public bool IsActive { get; set; }

    }
    public partial class PracticesMetaData
    {

    }
}