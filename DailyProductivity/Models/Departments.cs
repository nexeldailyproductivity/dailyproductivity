﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DailyProductivity.Models
{
    public partial class Departments
    {
        [Key]
        public long DepartmentID { get; set; }
        public string DepartmentName { get; set; }

    }
}