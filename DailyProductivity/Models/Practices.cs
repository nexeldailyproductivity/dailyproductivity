﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DailyProductivity.Models
{
    public partial class Practices
    {
        [Key]
        public long PracticeID { get; set; }

        public string PracticeName { get; set; }
        public string PracticeShortName { get; set; }
        public long CategoryID { get; set; }
    }
}