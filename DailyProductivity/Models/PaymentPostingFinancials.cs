﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DailyProductivity.Models
{
    public class PPFinancials
    {
        [Key]
        public long FinancialID { get; set; }
        public long EmployeeID { get; set; }
        public long PracticeID { get; set; }
        public decimal PF_eBridge { get; set; }
        public decimal Rejections { get; set; }
        public decimal ERADenials { get; set; }
        public decimal ERAUnmatched { get; set; }
        public decimal Appeals { get; set; }
        public decimal ManuallyAdjustments { get; set; }
        public decimal AgingClaims { get; set; }
        public decimal BillingETC { get; set; }
        public decimal Emails { get; set; }
        public decimal PTStatements { get; set; }
        public decimal VerificationClaims { get; set; }
        public long DepartmentID { get; set; }

        public DateTime FinancialDateTime { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastUpdated { get; set; }
        public long UpdatedBy { get; set; }

        public PPFinancials()
        {
            this.CreatedDate = DateTime.Now;
            this.LastUpdated = DateTime.UtcNow;
        }

        public decimal PPostingSum()
        {
            return (this.PF_eBridge + this.Rejections + this.ERADenials + this.ERAUnmatched + this.BillingETC + this.Appeals + this.ManuallyAdjustments + this.AgingClaims + this.Emails + this.PTStatements + this.VerificationClaims);

        }

    }
}