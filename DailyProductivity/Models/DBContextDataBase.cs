﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Web.Security;

namespace DailyProductivity.Models
{
    public class DBContextDataBase : DbContext
    {
        public DBContextDataBase() : base("name=PMSEntities")
        {
        }

        public DbSet<Employees> Employees { get; set; }
        public DbSet<Departments> Department { get; set; }
        public DbSet<ShiftTimings> ShiftTiming { get; set; }
        public DbSet<Roles> Role { get; set; }
        public DbSet<Practices> Practices { get; set; }
        public DbSet<PracticesCategories> PracticesCategories { get; set; }

        public DbSet<EmployeePractices> EmployeePractices { get; set; }

        public DbSet<Credentialing> Credentialing { get; set; }
        public DbSet<PatientHelpDesk> PatientHelpDesk { get; set; }
        public DbSet<Assignments> Assignments { get; set; }
        public DbSet<Agings> Aging { get; set; }
        public DbSet<Financial> Financials { get; set; }
        public DbSet<Notificatoins> Notificatoins { get; set; }

        public DbSet<PhdAssignments> PhdAssignments { get; set; }

        public DbSet<PPFinancials> PPFinancials { get; set; }

        public DbSet<Managers> Managers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}