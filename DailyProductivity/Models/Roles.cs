﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DailyProductivity.Models
{
    public class Roles
    {
        [Key]
        public long RoleID { get; set; }
        public string RoleName { get; set; }
    }
}