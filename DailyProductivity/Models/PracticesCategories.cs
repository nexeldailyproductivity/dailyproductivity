﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DailyProductivity.Models
{
    public partial class PracticesCategories
    {
        [Key]
        public long CategoryID { set; get; }
        public string CategoryName { get; set; }
    }
}