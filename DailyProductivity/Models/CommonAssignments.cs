﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DailyProductivity.Models
{
    public class Assignments
    {
        [Key]
        public long AssignmentID { get; set; }
        public long EmployeeID { get; set; }
        public long PracticeID { get; set; }
        public long EnteredClaims { get; set; }
        public long Eligibility { get; set; }
        public long PF_eBridge { get; set; }
        public long RejectionsEmdeon { get; set; }
        public long RejectionsAvaility { get; set; }
        public long ERADenials { get; set; }
        public long ERAUnmatched { get; set; }
        public long Appeals { get; set; }
        public long Calls { get; set; }
        public long AgingClaims { get; set; }
        //public long Email { get; set; }
        public long PTStatements { get; set; }
        public long EOBpayment { get; set; }
        public long Rejections { get; set; }
        public long PTLResponses { get; set; }
        public long Emails { get; set; }
        public long EmailVerification { get; set; }
        public long FaxedVerification { get; set; }
        public long WebVerification { get; set; }
        public long Others { get; set; }
        public long DepartmentID { get; set; }
        public long PendingClaims { get; set; }
        public long PaidClaims { get; set; }
        public long PreparedClaims { get; set; }
        public long PreparedEncounters { get; set; }
        public long MissingLog { get; set; }
        public long QA { get; set; }
        public long Multiple { get; set; }
        public long NoPatientBill { get; set; }
        public long UVTotalClaims { get; set; }
        public long Done { get; set; }

        public long BillingETC { get; set; }
        public long ManuallyAdjustments { get; set; }
        public long VerificationClaims { get; set; }
        public long BillingAnalysis { get; set; }

        

        public long ReceivedBills { get; set; }



        public DateTime AssignmentDateTime { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastUpdated { get; set; }
        public long UpdatedBy { get; set; }

        public Assignments()
        {
            this.CreatedDate = DateTime.Now;
            this.LastUpdated = DateTime.UtcNow;
        }
        public long RcmgMASum()
        {
            return (this.EnteredClaims + this.Eligibility + this.PF_eBridge + this.RejectionsEmdeon + this.RejectionsAvaility + this.ERADenials + this.ERAUnmatched + this.Appeals + this.Calls + this.AgingClaims + this.Emails + this.PTStatements + this.EOBpayment);
        }
        public long RcmgSum()
        {
            return (this.PF_eBridge + this.Rejections + this.ERADenials + this.ERAUnmatched + this.PTLResponses + this.Appeals + this.Calls + this.AgingClaims + this.Emails + this.PTStatements);
        }
        public long DMESum()
        {
            return (this.Appeals + this.Calls + this.AgingClaims + this.Emails + this.Others);
        }
        public long EligibilitySum()
        {
            return (this.Calls + this.WebVerification + this.FaxedVerification + this.EmailVerification + this.Others);
        }
        public long PPostingSum()
        {
            return (this.PF_eBridge + this.Rejections + this.ERADenials + this.ERAUnmatched + this.BillingETC + this.Appeals + this.ManuallyAdjustments + this.AgingClaims + this.Emails + this.PTStatements + this.VerificationClaims);

        }
    }
}