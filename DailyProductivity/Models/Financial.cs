﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DailyProductivity.Models
{
    public class Financial
    {
        [Key]
        public long FinancialID { get; set; }
        public long EmployeeID { get; set; }
        public long PracticeID { get; set; }
        public decimal DailyCharges { get; set; }
        public decimal DailyPayments { get; set; }
        public decimal MTDCharges { get; set; }
        public decimal MTDPayments { get; set; }
        public decimal AgingAmount{ get; set; }

        //////MA 
        public decimal DailyChargesECW { get; set; }
        public decimal DailyPaymentsECW { get; set; }
        public decimal DailyPaymentseTeClinic { get; set; }
        public decimal MTDPaymentseTeClinic { get; set; }
        public decimal MTDPaymentsECW { get; set; }
        public decimal AgingAmounteTeClinic { get; set; }
        public decimal AgingAmounteCW { get; set; }

        public decimal EnteredChargeI { get; set; }
        public decimal EnteredChargeP { get; set; }
        public long DepartmentID { get; set; }
        public DateTime FinancialDateTime { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastUpdated { get; set; }
        public long UpdatedBy { get; set; }

        public Financial()
        {
            this.CreatedDate = DateTime.Now;
            this.LastUpdated = DateTime.UtcNow;
        }


    }
}