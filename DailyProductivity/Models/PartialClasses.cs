﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DailyProductivity.Models
{
   
        public partial class Employees
    {
            [NotMapped]
            public bool RememberMe { get; set; }

            [NotMapped]
            public string[] Practices { get; set; }

            [NotMapped]

            public string returnUrl { get; set; }
            [NotMapped]
            public string ShiftTimings { get; set; }
        }
    public partial class Departments
    {
        [NotMapped]
        public string EDID { get; set; }
    }
  
}