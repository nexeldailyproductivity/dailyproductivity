﻿using  DailyProductivity.Models;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DailyProductivity.App_Start
{
    public class IsAdminAttribute:ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {



            var url = new UrlHelper(filterContext.RequestContext);
            var returnUrl = HttpContext.Current.Request.Url.AbsolutePath;
            var Url = url.Action("login" , "auth" , new { Area = "" }).ToString();

            if (HttpContext.Current.Session["ManagerID"] == null)
            {
                filterContext.Result = new RedirectResult(Url);
                return;
            }
            else
            {
                long UserID = long.Parse(HttpContext.Current.Session["ManagerID"].ToString());
                DBContextDataBase db = new DBContextDataBase();
               var UserDetails = db.Managers.Find(UserID);
                if (UserDetails==null || UserDetails.RoleID != 1)
                {
                    Url = url.Action("login", "auth",new {Area = "", returnUrl = returnUrl }).ToString();
                    filterContext.Result = new RedirectResult(Url);
                    return;
                }
            }

            base.OnActionExecuting(filterContext);
        }
    }
}