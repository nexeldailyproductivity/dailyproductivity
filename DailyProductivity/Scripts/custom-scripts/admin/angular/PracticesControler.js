﻿App.controller("NewPractice", function ($scope, PracticeServices,App) {
   
    


    App.GET($AdminUrl_S + "CheckManagerLoginSession").then(function (response) {
        var ManagerDetailObjct = {};
        if (response.data == "exception") {
            swal("Exception", "Invalid server response, Please try again ", "error").then(value=> {
                window.location.href = '/auth/logout'
            });
        }
        else if (response.data == "null") {
            swal("Exception", "Invalid server response, Please try again ", "error").then(value=> {
                window.location.href = '/home/dashboard'
            });
        }
        else if (response.data == "invalid") {
            swal("Exception", "Invalid server response, Please try again ", "error").then(value=> {
                window.location.href = '/auth/logout'
            });
        }
        else {
            ManagerDetailObjct = response.data;
            var services = $AdminUrl_S;
            if (ManagerDetailObjct.RoleID == 1) {
                App.GET(services + "GetPracticeCategories").then(function (response) {

                    if (response.data == "exception") { swal("Error ", "Invalid server response", "error"); }
                    else if (response.data == "null") { swal("Error ", "Invalid server response", "error"); }
                    else {
                        console.log(response.data);
                        $scope.PracticeCategoryList = response.data;
                    }
                });
                $scope.SavePractice = function (Practice) {
                   
                    App.POST(Practice, services + "SaveNewPractice").then(function (response) {
                        if (response.data == "exception") {
                            swal("Error ", "Invalid server response", "error");
                        }
                        else if (response.data == "fails") {

                        }
                        else if (response.data == "added") {
                            swal({
                                title: "Success !!",
                                text: "Practice Added Successfully",
                                icon: "success",
                                buttons: true,
                                dangerMode: true,
                            });
                        }
                    });
                }
                $scope.GetPracticeShortName = function (Practice) {
                    var matches = Practice.match(/\b(\w)/g); // ['J','S','O','N']
                    var acronym = matches.join('');
                    $scope.Practice.PracticeShortName = acronym;
                }
                $scope.Cancel = function () {
                    window.location.href = "../manage";
                }
                
            }
            else {
                swal("Warning", "You are not authorised to access this page", "error").then(value=> {
                    window.location.href = "/auth/logout";
                });
            }
        }
    });




});
App.controller("PracticeDetailList", function ($scope, PracticeServices,App) {



   



    App.GET($AdminUrl_S + "CheckManagerLoginSession").then(function (response) {
        var ManagerDetailObjct = {};
        if (response.data == "exception") {
            swal("Exception", "Invalid server response, Please try again ", "error").then(value=> {
                window.location.href = '/auth/logout'
            });
        }
        else if (response.data == "null") {
            swal("Exception", "Invalid server response, Please try again ", "error").then(value=> {
                window.location.href = '/home/dashboard'
            });
        }
        else if (response.data == "invalid") {
            swal("Exception", "Invalid server response, Please try again ", "error").then(value=> {
                window.location.href = '/auth/logout'
            });
        }
        else {
            ManagerDetailObjct = response.data;
            var services = $AdminUrl_S;
            if (ManagerDetailObjct.RoleID == 1) {


                App.GET(services + "GetPracticeDetailList").then(function (response) {
                    if (response.data == "exception") {
                        swal("Error ", "Invalid server response", "error");
                    }
                    else if (response.data == "null") {
                        swal("Error ", "Invalid server response", "error");
                    }
                    else {
                        console.log(response.data);
                        var PracticeUserDetailList = response.data;
                        //  $(document).ready(function () {
                        console.log(PracticeUserDetailList);

                        var table = $('#practice-datatable').DataTable({
                            aaData: PracticeUserDetailList,
                            dom: "Bfrtip",
                            processing: true,

                            columns: [
                                    {
                                        "mRender": function (data, type, row) {
                                            return '<span class="badge badge-pill badge-pink ks-label" style="width: 90px; height: 31px; padding: 9px; font-size: 13px;">' + row.PracticeID + '</span>'
                                        }
                                    },
                                    
                                    { data: "PracticeName" },
                                    {
                                        "mRender": function (data, type, row) {
                                            if (row.CategoryID == '1') {
                                                return '<label class="badge badge-info">' + row.CategoryName + '</label>';
                                            }
                                            else  { return '<label class="badge badge-primary">' + row.CategoryName + '</label>'; }


                                        }
                                    },
                                    {
                                        "mRender": function (data, type, row) {
                                            return '<a class="btn btn-sm btn-success" href=editpractice/' + row.EPID + '>Manage';
                                        }
                                    }

                            ],


                            //  lengthChange: false,
                            buttons: [

                                'colvis',
                                {
                                    extend: 'copyHtml5',
                                    exportOptions: {
                                        columns: [0, 1, 2]
                                    }
                                },
                                {
                                    extend: 'excelHtml5',
                                    exportOptions: {
                                        columns: [0, 1, 2]
                                    }
                                },
                                {
                                    extend: 'pdfHtml5',
                                    exportOptions: {
                                        columns: [0, 1, 2]
                                    }
                                },
                                {
                                    extend: 'csvHtml5',
                                    exportOptions: {
                                        columns: [0, 1, 2]
                                    }
                                },
                            ],
                            initComplete: function () {
                                $('.dataTables_wrapper select').select2({
                                    minimumResultsForSearch: Infinity
                                });
                            },



                        });

                        table.buttons().container().appendTo('#ks-datatable_wrapper .col-md-6:eq(0)');
                        // });
                    }

                });;
            }
            else {
                swal("Warning", "You are not authorised to access this page", "error").then(value=> {
                    window.location.href = "/auth/logout";
                });
            }
        }
    });
});
App.controller("UpdatePractice", function (PracticeServices, $scope , App) {
    App.GET($AdminUrl_S + "CheckManagerLoginSession").then(function (response) {
        var ManagerDetailObjct = {};
        if (response.data == "exception") {
            swal("Exception", "Invalid server response, Please try again ", "error").then(value=> {
                window.location.href = '/auth/logout'
            });
        }
        else if (response.data == "null") {
            swal("Exception", "Invalid server response, Please try again ", "error").then(value=> {
                window.location.href = '/home/dashboard'
            });
        }
        else if (response.data == "invalid") {
            swal("Exception", "Invalid server response, Please try again ", "error").then(value=> {
                window.location.href = '/auth/logout'
            });
        }
        else {
            ManagerDetailObjct = response.data;
            var services = $AdminUrl_S;
            if (ManagerDetailObjct.RoleID == 1) {
                
                if (PracticeID != null || PracticeID != 0 || !PracticeID) {
                    App.GET(services + "GetPracticeCategories").then(function (response) {

                        if (response.data == "exception") { swal("Error ", "Invalid server response", "error"); }
                        else if (response.data == "null") { swal("Error ", "Invalid server response", "error"); }
                        else {
                            console.log(response.data);
                            $scope.PracticeCategoryList = response.data;
                            var Practice = { PracticeID: PracticeID };
                            App.POST(Practice, services + "GetPracticeDetailByID").then(function (response) {
                                console.log(response.data);
                                if (response.data == "exception") {
                                    swal("Error ", "Invalid server response", "error");
                                }
                                else if (response.data == "null") {
                                    swal("Error ", "Invalid server response", "error");
                                }
                                else {
                                    $scope.Practice = response.data;

                                }
                            });
                        }
                    });
                    
                    $scope.Cancel = function () {
                        window.location.href = "../manage";
                    }
                    $scope.UpdatePractice = function (data) {
                        $scope.message = '';

                        if ($scope.fmUpdtPractice.$valid) {

                            App.POST(data,services + "UpdatePractice").then(function (response) {


                                if (response.data == "exception") {

                                    swal({
                                        title: "Error !!",
                                        text: "Invalid Server Response.Please Try Again Later",
                                        icon: "error",
                                        buttons: true,
                                        dangerMode: true,
                                    }).then((ok) => {
                                        window.location.href = window.location.href = "../addpractice";
                                    });
                                }

                                else if (response.data == "updated") {

                                    swal({
                                        title: "Success !!",
                                        text: "UpdatePractice Updated Successfully",
                                        icon: "success",
                                        buttons: true,
                                        dangerMode: true,
                                    }).then(value=>{
                                        window.location.href = "../manage";
                                    });
                                   
                                }


                            });

                        }
                        else {
                            $scope.message = 'Please fill the required fields';
                        }
                    };
                    $scope.DeletePractice = function (Practice) {
                        swal({
                            title: "Confirmation",
                            text: "Are you sure to delete ?",
                            icon: "warning",
                            buttons: {

                                cancel: true,
                                confirm: true,
                            },
                        })
                        .then((value) => {
                            if (value) {
                                App.POST(Practice, services + "DeletePractice").then(function (response) {
                                    if (response.data === "deleted") {

                                        swal("Success! Practice has been deleted!", {
                                            icon: "success",
                                        }).then(value=> {
                                            window.location.href = "../manage";
                                        });

                                    }
                                    else if (response.data === "fails") {
                                        swal("Alert! Error occure while deleting practice!", {
                                            icon: "error",
                                        }).then(value=> {
                                            window.location.href = "../manage";
                                        });

                                    }
                                    else if (response.data === "exception") {


                                        swal("Error ", "Invalid server response", "error").then(value=> {
                                            window.location.href = "../manage";
                                        });

                                    }
                                });
                            }
                        });
                    }
                    $scope.GetPracticeShortName = function (Practice) {
                        var matches = Practice.match(/\b(\w)/g); // ['J','S','O','N']
                        var acronym = matches.join('');
                        $scope.Practice.PracticeShortName = acronym;
                    }
                }
                else {
                    swal("Error", "Invalid Request.. No User Found", "error");
                }
            }
            else {
                swal("Warning", "You are not authorised to access this page", "error").then(value=> {
                    window.location.href = "/auth/logout";
                });
            }
        }
    });

});
App.factory("PracticeServices", function ($http,$q) {

    return {
        GetPracticeCategories: function () {
            return $http.get($AdminUrl_S + "GetPracticeCategories");
        },
        SaveNewPractice: function (data) {
            var defer = $q.defer();
            console.log(data);
            $http({
                url: $AdminUrl_S + 'SaveNewPractice',
                method: "POST",
                data: data
            }).then(function (d) {
                defer.resolve(d);
            }), function (e) {
                //callback after failed
                alert("Error");

                defer.reject(e);
            };
            return defer.promise;
        },
        GetPracticeDetailList: function () {
            return $http.get($AdminUrl_S + "GetPracticeDetailList");
        },
        GetPracticeDetailByID: function (data) {
            var defer = $q.defer();
            console.log(data);
            $http({
                url: $AdminUrl_S + 'GetPracticeDetailByID',
                method: "POST",
                data: data
            }).then(function (d) {
                defer.resolve(d);
            }), function (e) {
                //callback after failed
                alert("Error");

                defer.reject(e);
            };
            return defer.promise;
        },
        DeletePractice: function (data) {
            var defer = $q.defer();
            console.log(data);
            $http({
                url: $AdminUrl_S + 'DeletePractice',
                method: "POST",
                data: data
            }).then(function (d) {
                defer.resolve(d);
            }), function (e) {
                //callback after failed
                alert("Error");

                defer.reject(e);
            };
            return defer.promise;
        },
        UpdatePractice: function (data) {
            var defer = $q.defer();
            console.log(data);
            $http({
                url: $AdminUrl_S + 'UpdatePractice',
                method: "POST",
                data: data
            }).then(function (d) {
                defer.resolve(d);
            }), function (e) {
                //callback after failed
                alert("Error");

                defer.reject(e);
            };
            return defer.promise;
        },
    }
});