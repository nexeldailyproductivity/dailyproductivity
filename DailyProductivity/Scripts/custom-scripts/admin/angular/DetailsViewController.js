﻿App.controller("DepartmentalDetails", function ($scope, App, PerformanceServices,$filter) {
    App.GET($AdminUrl_S + "CheckManagerLoginSession").then(function (response) {
        var ManagerDetailObjct = {};
        if (response.data == "exception") {
            swal("Exception", "Invalid server response, Please try again ", "error").then(value => {
                window.location.href = '/auth/logout'
            });
        }
        else if (response.data == "null") {
            swal("Exception", "Invalid server response, Please try again ", "error").then(value => {
                window.location.href = '/home/dashboard';
            });
        }
        else if (response.data == "invalid") {
            swal("Exception", "Invalid server response, Please try again ", "error").then(value => {
                window.location.href = '/auth/logout';
            });
        }
        else {
            ManagerDetailObjct = response.data;
            var services = $AdminUrl_S;
           
            $scope.Filter = {}
            $('.ks-datepicker .flatpickr').flatpickr({
                onChange: function (ObjDate) {
                    //$scope.Filter = {
                    //    //$filter('date')(ObjDate, "yyyy-mm-dd 00:00:00.000") ,
                    //    FilterDateTime: ObjDate
                    //};
                    //console.log($scope.Filter);
                    $scope.Filter.FilterDateTime = ObjDate;

                    console.log($scope.Filter);
                },
                enableTime: false,
                dateFormat: "yyyy-mm-dd 00:00:00.000"
            });
            
            if (ManagerDetailObjct.RoleID == 1) {

                App.GET(services + "GetDepartments").then(function (response) {
                    if (response.data == "exception") {
                        swal("Exception", "Invalid server response, Please try again ", "error").then(value => {
                            window.location.href = '/auth/logout';
                        });
                    }
                    else if (response.data == "null") {
                        swal("Exception", "No detail found  ", "error").then(value => {
                            window.location.href = './departments';
                        });
                    }
                    else
                    {
                        $scope.DepartmentsList = response.data;
                        $scope.Filter.Department = response.data[0];
                        PerformanceServices.GetAssignmentDataByDepartment({ Department: response.data[0] }).then(function (response) {
                            if (response.data == "exception") {
                                swal("Exception", "Invalid server response, Please try again ", "error").then(value => {
                                  //  window.location.href = '/auth/logout'
                                });
                            }
                            else if (response.data == "null") {
                                swal("Exception", "No detail found  ", "error").then(value => {
                                   
                                });
                            }
                            else {
                                console.log(response.data);
                               // alert(JSON.stringify(response.data));
                                $scope.DetailList = response.data;
                            }
                        });
                    }
                });

                $scope.GetDepartmentalReport = function (Filter) {
                  
                   
                    PerformanceServices.GetAssignmentDataByDepartment(Filter).then(function (response) {
                        if (response.data == "exception") {
                            swal("Exception", "Invalid server response, Please try again ", "error").then(value => {
                                //  window.location.href = '/auth/logout'
                            });
                        }
                        else if (response.data == "null") {
                            swal("Exception", "No detail found  ", "error").then(value => {

                            });
                        }
                        else {
                            console.log(response.data);
                          //  alert(JSON.stringify(response.data));
                            $scope.DetailList = response.data;
                        }
                    });



                }
                $scope.GetDepartmentAgingsData = function () {
                    console.log($scope.Filter);
                    debugger;
                    if ($scope.Filter.Department.DepartmentID == 3 || $scope.Filter.Department.DepartmentID == 4 || $scope.Filter.Department.DepartmentID == 5 || $scope.Filter.Department.DepartmentID == 9 || $scope.Filter.Department.DepartmentID == 7) {
                        Filter = $scope.Filter; debugger;
                        PerformanceServices.GetAgingsDataByDepartment(Filter).then(function (response) {
                            if (response.data == "exception") {
                                swal("Exception", "Invalid server response, Please try again ", "error").then(value => {
                                    //  window.location.href = '/auth/logout'
                                });
                            }
                            else if (response.data == "null") {
                                swal("Exception", "No detail found  ", "error").then(value => {

                                });
                            }
                            else {
                                console.log(response.data);
                                $scope.AgingDetailList = response.data;
                            }
                        });

                    }
                }
                $scope.GetDepartmentFinancialsData = function () {
                    
                    if ($scope.Filter.Department.DepartmentID == 3 || $scope.Filter.Department.DepartmentID == 4 || $scope.Filter.Department.DepartmentID == 5 || $scope.Filter.Department.DepartmentID == 9 || $scope.Filter.Department.DepartmentID == 7)
                    {
                        Filter = $scope.Filter; debugger;
                        PerformanceServices.GetFinancialDataByDepartment(Filter).then(function (response) {
                            if (response.data == "exception") {
                                swal("Exception", "Invalid server response, Please try again ", "error").then(value => {
                                    //  window.location.href = '/auth/logout'
                                });
                            }
                            else if (response.data == "null") {
                                swal("Exception", "No detail found  ", "error").then(value => {

                                });
                            }
                            else {
                                console.log(response.data);
                                $scope.FinancialsDetailList = response.data;
                            }
                        });

                    }
                }
                $scope.GetDepartmentAssignmentData = function () {
                    

                }

                SetNULL = function() {
                    if (typeof $scope.AgingDetailList != "undefined") {
                        $scope.AgingDetailList = {}
                    }
                    if (typeof $scope.DetailList != "undefined") {
                        $scope.DetailList = {}
                    }
                    if (typeof $scope.FinancialsDetailList != "undefined") {
                        $scope.FinancialsDetailList = {}
                    }
                }
            }
            else {
                swal("Warning", "You are not authorised to access this page", "error").then(value => {
                    window.location.href = "/auth/logout";
                });
            }
        }
    });
});
App.factory("PerformanceServices", function (App) {
    return {
        GetAssignmentDataByDepartment: function(Data) {

            return App.POST(Data, $AdminUrl_S + "GetAssignmentDataByDepartment");
        },
        GetFinancialDataByDepartment: function(Data) {

            return App.POST(Data, $AdminUrl_S + "GetFinancialsDataByDepartment");
        },
        GetAgingsDataByDepartment: function(Data) {

            return App.POST(Data, $AdminUrl_S + "GetAgingsDataDataByDepartment");
        },

        GetAssignmentDataOfEmployee: function(Data) {

            return App.POST(Data, $AdminUrl_S + "GetAssignmentDataOfEmployee");
        },
        GetFinancialDataOfEmployee: function (Data) {

            return App.POST(Data, $AdminUrl_S + "GetFinancialDataOfEmployee");
        },
        GetAgingDataOfEmployee: function (Data) {

            return App.POST(Data, $AdminUrl_S + "GetAgingDataOfEmployee");
        }
    }
});

App.controller("EmployeePerformance", function ($scope, App, PerformanceServices, $filter) {
    App.GET($AdminUrl_S + "CheckManagerLoginSession").then(function (response) {
        var ManagerDetailObjct = {};
        if (response.data == "exception") {
            swal("Exception", "Invalid server response, Please try again ", "error").then(value => {
                window.location.href = '/auth/logout'
            });
        } else if (response.data == "null") {
            swal("Exception", "Invalid server response, Please try again ", "error").then(value => {
                window.location.href = '/home/dashboard'
            });
        } else if (response.data == "invalid") {
            swal("Exception", "Invalid server response, Please try again ", "error").then(value => {
                window.location.href = '/auth/logout'
            });
        } else {
            ManagerDetailObjct = response.data;
            var services = $AdminUrl_S;
            if (ManagerDetailObjct.RoleID == 1) {



                $scope.Filter = {}

                $('.ks-datepicker .flatpickr').flatpickr({
                    onChange: function (ObjDate) {
                        $scope.Filter.FilterDateTime = ObjDate;
                        
                        console.log($scope.Filter);
                    },
                    enableTime: false,
                    dateFormat: "yyyy-mm-dd 00:00:00.000",
                });
                App.GET(services + "GetEmployeeDetailList").then(function (response) {
                    if (response.data == "exception") {
                        swal("Exception", "Invalid server response, Please try again ", "error").then(value => {
                            window.location.href = '/auth/logout'
                        });
                    }
                    else if (response.data == "null") {
                        swal("Exception", "No detail found  ", "error").then(value => {
                            window.location.href = './employees';
                        });
                    }
                    else {
                        $scope.EmployeeList = response.data;
                        $scope.Filter.Employee = response.data[0];
                        PerformanceServices.GetAssignmentDataOfEmployee({ Employee: response.data[0] }).then(function (response) {
                            if (response.data == "exception") {
                                swal("Exception", "Invalid server response, Please try again ", "error").then(value => {
                                    //  window.location.href = '/auth/logout'
                                });
                            }
                            else if (response.data == "null") {
                                swal("Exception", "No detail found  ", "error").then(value => {

                                });
                            }
                            else {
                                console.log(response.data);
                                // alert(JSON.stringify(response.data));
                                $scope.DetailList = response.data;
                            }
                        });

                    }
                });
                $scope.GetEmployeeAssignmentData = function (Filter) {

                    PerformanceServices.GetAssignmentDataOfEmployee(Filter).then(function (response) {
                        if (response.data == "exception") {
                            swal("Exception", "Invalid server response, Please try again ", "error").then(value => {
                                //  window.location.href = '/auth/logout'
                            });
                        }
                        else if (response.data == "null") {
                            swal("Exception", "No detail found  ", "error").then(value => {

                            });
                        }
                        else {
                            console.log(response.data);
                            // alert(JSON.stringify(response.data));
                            $scope.DetailList = response.data;
                        }
                    });


                }
                $scope.GetEmployeeAgingsData = function () {
                    if ($scope.Filter.Employee.DepartmentID == 3 || $scope.Filter.Employee.DepartmentID == 4 || $scope.Filter.Employee.DepartmentID == 5 || $scope.Filter.Employee.DepartmentID == 9 || $scope.Filter.Employee.DepartmentID == 7) {
                        Filter = $scope.Filter; debugger;
                        PerformanceServices.GetAgingDataOfEmployee(Filter).then(function (response) {
                            if (response.data == "exception") {
                                swal("Exception", "Invalid server response, Please try again ", "error").then(value => {
                                    //  window.location.href = '/auth/logout'
                                });
                            }
                            else if (response.data == "null") {
                                swal("Exception", "No detail found  ", "error").then(value => {

                                });
                            }
                            else {
                                console.log(response.data);
                                //  alert(JSON.stringify(response.data));
                                $scope.AgingDetailList = response.data;
                            }
                        });

                    }
                }
                $scope.GetEmployeeFinancialsData = function () {
                    if ($scope.Filter.Employee.DepartmentID == 3 || $scope.Filter.Employee.DepartmentID == 4 || $scope.Filter.Employee.DepartmentID == 5 || $scope.Filter.Employee.DepartmentID == 9 || $scope.Filter.Employee.DepartmentID == 7) {
                        Filter = $scope.Filter; debugger;
                        PerformanceServices.GetFinancialDataOfEmployee(Filter).then(function (response) {
                            if (response.data == "exception") {
                                swal("Exception", "Invalid server response, Please try again ", "error").then(value => {
                                    //  window.location.href = '/auth/logout'
                                });
                            }
                            else if (response.data == "null") {
                                swal("Exception", "No detail found  ", "error").then(value => {

                                });
                            }
                            else {
                                console.log(response.data);
                                //  alert(JSON.stringify(response.data));
                                $scope.FinancialsDetailList = response.data;
                            }
                        });

                    }
                }
                $scope.ApplyFilters = function() {
                    PerformanceServices.GetAssignmentDataOfEmployee($scope.Filter).then(function (response) {
                        if (response.data == "exception") {
                            swal("Exception", "Invalid server response, Please try again ", "error").then(value => {
                                //  window.location.href = '/auth/logout'
                            });
                        }
                        else if (response.data == "null") {
                            swal("Exception", "No detail found  ", "error").then(value => {

                            });
                        }
                        else {
                            console.log(response.data);
                            // alert(JSON.stringify(response.data));
                            $scope.DetailList = response.data;
                        }
                    });
                }
            }
            else {
                swal("Warning", "You are not authorized to access this page", "error").then(value => {
                    window.location.href = "/auth/logout";
                });
            }
        }
    }
    );
});


App.controller("PracticePerformance", function ($scope, App, PerformanceServices, $filter) {
    App.GET($AdminUrl_S + "CheckManagerLoginSession").then(function (response) {
        var ManagerDetailObjct = {};
        if (response.data == "exception") {
            swal("Exception", "Invalid server response, Please try again ", "error").then(value => {
                window.location.href = '/auth/logout'
            });
        } else if (response.data == "null") {
            swal("Exception", "Invalid server response, Please try again ", "error").then(value => {
                window.location.href = '/home/dashboard'
            });
        } else if (response.data == "invalid") {
            swal("Exception", "Invalid server response, Please try again ", "error").then(value => {
                window.location.href = '/auth/logout'
            });
        } else {
            ManagerDetailObjct = response.data;
            var services = $AdminUrl_S;
            if (ManagerDetailObjct.RoleID == 1) {



                $scope.Filter = {}

                $('.ks-datepicker .flatpickr').flatpickr({
                    onChange: function (ObjDate) {
                        $scope.Filter.FilterDateTime = ObjDate;

                        console.log($scope.Filter);
                    },
                    enableTime: false,
                    dateFormat: "yyyy-mm-dd 00:00:00.000",
                });
                App.GET(services + "GetAllPractices").then(function (response) {
                    if (response.data == "exception") {
                        swal("Exception", "Invalid server response, Please try again ", "error").then(value => {
                            window.location.href = '/auth/logout'
                        });
                    }
                    else if (response.data == "null") {
                        swal("Exception", "No detail found  ", "error").then(value => {
                            window.location.href = './employees';
                        });
                    }
                    else {
                        $scope.PracticeList = response.data;
                        PerformanceServices.GetAssignmentDataOfEmployee().then(function (response) {
                            if (response.data == "exception") {
                                swal("Exception", "Invalid server response, Please try again ", "error").then(value => {
                                    //  window.location.href = '/auth/logout'
                                });
                            }
                            else if (response.data == "null") {
                                swal("Exception", "No detail found  ", "error").then(value => {

                                });
                            }
                            else {
                                console.log(response.data);
                                // alert(JSON.stringify(response.data));
                                $scope.DetailList = response.data;
                            }
                        });

                    }
                });
               
            }
            else {
                swal("Warning", "You are not authorized to access this page", "error").then(value => {
                    window.location.href = "/auth/logout";
                });
            }
        }
    }
    );
});