﻿App.controller("NewManager", function ($scope, App) {
    App.GET($AdminUrl_S + "CheckManagerLoginSession").then(function (response) {
        var ManagerDetailObjct = {};
        if (response.data == "exception") {
            swal("Exception", "Invalid server response, Please try again ", "error").then(value=> {
                window.location.href = '/auth/logout'
            });
        }
        else if (response.data == "null") {
            swal("Exception", "Invalid server response, Please try again ", "error").then(value=> {
                window.location.href = '/home/dashboard'
            });
        }
        else if (response.data == "invalid") {
            swal("Exception", "Invalid server response, Please try again ", "error").then(value=> {
                window.location.href = '/auth/logout'
            });
        }
        else
        {
            ManagerDetailObjct = response.data;
            var services = $AdminUrl_S;
            if (ManagerDetailObjct.RoleID == 1) {
             

                App.GET(services + "GetDepartments").then(function (response) {
                    if (response.data == "null") {
                        swal("Error ", "No departments found", "error");
                    }
                    else if (response.data == "exception") {
                        swal("Error ", "Invalid server response", "error");
                    }
                    else {

                        $scope.DepartmentList = response.data;
                        $scope.DepartmentID = $scope.DepartmentList[1].DepartmentName;
                    }

                });
              
                App.GET(services + "GetShiftTimings").then(function (response) {
                    if (response.data == "null") {
                        swal("Error ", "No timing details found", "error");
                    }
                    else if (response.data == "exception") {
                        swal("Error ", "Invalid server response", "error");
                    }
                    else {

                        $scope.AllTimingList = response.data;

                    }

                });
                $scope.SaveManager = function (Manager) {
                    if ($scope.fmNewManager.$valid) {
                        console.log(Manager);
                        debugger;
                        Manager.RoleID = 1;

                        App.POST(Manager, services + "SaveManager").then(function (response) {


                            if (response.data == "exception") {

                                swal({
                                    title: "Error !!",
                                    text: "Invalid Server Response.Please Try Again Later",
                                    icon: "error",
                                    buttons: true,
                                    dangerMode: true,
                                }).then((ok) => {
                                    window.location.href = "manager";
                                });
                            }
                            else if (response.data == "alreadyexisit") {
                                swal("Already Exist!", "User already exist please choose another.", "info");
                            }
                            else if (response.data == "fillfields") {
                                swal("Fill Fields!", "Please fill all the fileds", "info");
                            }
                            else if (response.data == "success") {

                                swal({
                                    title: "Success !!",
                                    text: "Manager Added Successfully",
                                    icon: "success",
                                    buttons: true,
                                    dangerMode: true,
                                }).then((ok) => {
                                    window.location.href = "manager";
                                });

                            }


                        });

                    }
                };
                $scope.Cancel = function () {
                    window.location.href = "managers";
                }
            }
            else {
                swal("Warning", "You are not authorised to access this page", "error").then(value=> {
                    window.location.href = "/auth/logout";
                });
            }
        }
    });
});

App.controller("ManagerDetailList", function ($scope, App) {
    App.GET($AdminUrl_S + "CheckManagerLoginSession").then(function (response) {
        var ManagerDetailObjct = {};
        if (response.data == "exception") {
            swal("Exception", "Invalid server response, Please try again ", "error").then(value=> {
                window.location.href = '/auth/logout'
            });
        }
        else if (response.data == "null") {
            swal("Exception", "Invalid server response, Please try again ", "error").then(value=> {
                window.location.href = '/home/dashboard'
            });
        }
        else if (response.data == "invalid") {
            swal("Exception", "Invalid server response, Please try again ", "error").then(value=> {
                window.location.href = '/auth/logout'
            });
        }
        else {
            ManagerDetailObjct = response.data;
            var services = $AdminUrl_S;
            if (ManagerDetailObjct.RoleID == 1) {
                App.GET(services + "GetManagersDetailList").then(function (response) {
                    if (response.data == "exception") {
                        swal("Error ", "Invalid server response", "error");
                    }
                    else if (response.data == "null") {
                        swal("Error ", "Invalid server response", "error");
                    }
                    else {
                        console.log(response.data);
                        var ManagerDetailList = response.data;
                        //  $(document).ready(function () {
                       //console.log(UserDetailList);

                        var table = $('#ks-datatable').DataTable({
                            aaData: ManagerDetailList,
                            dom: "Bfrtip",
                            processing: true,

                            columns: [
                                    {
                                        "mRender": function (data, type, row) {
                                            return '<span class="badge badge-pill badge-pink ks-label" style="width: 90px; height: 31px; padding: 9px; font-size: 13px;">' + row.ManagerID + '</span>'
                                        }
                                    },
                                    {
                                        "mRender": function (data, type, row) {
                                            return row.FirstName + ' ' + row.LastName;
                                        }
                                    },
                                    { data: "UserName" },
                                    { data: "Email" },
                                    { data: "EmployeeNo" },
                                    { data: "Designation" },
                                    { data: "RoleName" },
                                    { data: "Title" },
                                    {
                                        "mRender": function (data, type, row) {
                                            if (row.Status == 'Active') {
                                                return '<label class="badge badge-info">' + row.Status + '</label>';
                                            }
                                            else if (row.Status != 'Active') { return '<label class="badge badge-danger">' + row.Status + '</label>'; }


                                        }
                                    },
                                    {
                                        "mRender": function (data, type, row) {
                                            return '<a class="btn btn-sm btn-success" href=manage/' + row.EMID + '>Manage';
                                        }
                                    }

                            ],


                            //  lengthChange: false,
                            buttons: [

                                'colvis',
                                {
                                    extend: 'copyHtml5',
                                    exportOptions: {
                                        columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
                                    }
                                },
                                {
                                    extend: 'excelHtml5',
                                    exportOptions: {
                                        columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
                                    }
                                },
                                {
                                    extend: 'pdfHtml5',
                                    exportOptions: {
                                        columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
                                    }
                                },
                                {
                                    extend: 'csvHtml5',
                                    exportOptions: {
                                        columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
                                    }
                                },
                            ],
                            initComplete: function () {
                                $('.dataTables_wrapper select').select2({
                                    minimumResultsForSearch: Infinity
                                });
                            },



                        });

                        table.buttons().container().appendTo('#ks-datatable_wrapper .col-md-6:eq(0)');
                        // });
                    }

                });;
            }
            else {
                swal("Warning", "You are not authorised to access this page", "error").then(value=> {
                    window.location.href = "/auth/logout";
                });
            }
        }
    });
});

App.controller("UpdateManager", function ($scope, App) {
    App.GET($AdminUrl_S + "CheckManagerLoginSession").then(function (response) {
        var ManagerDetailObjct = {};
        if (response.data == "exception") {
            swal("Exception", "Invalid server response, Please try again ", "error").then(value=> {
                window.location.href = '/auth/logout'
            });
        }
        else if (response.data == "null") {
            swal("Exception", "Invalid server response, Please try again ", "error").then(value=> {
                window.location.href = '/home/dashboard'
            });
        }
        else if (response.data == "invalid") {
            swal("Exception", "Invalid server response, Please try again ", "error").then(value=> {
                window.location.href = '/auth/logout'
            });
        }
        else {
            ManagerDetailObjct = response.data;
            var services = $AdminUrl_S;
            if (ManagerDetailObjct.RoleID == 1) {
                

                App.GET(services + "GetDepartments").then(function (response) {
                    if (response.data == "null") {
                        swal("Error ", "Invalid server response", "error");
                    }
                    else if (response.data == "exception") {
                        swal("Error ", "Invalid server response", "error");
                    }
                    else {

                        $scope.DepartmentList = response.data;
                        $scope.DepartmentID = $scope.DepartmentList[1].DepartmentName;
                    }

                });
               
                App.GET(services + "GetShiftTimings").then(function (response) {
                    if (response.data == "null") {
                        swal("Error ", "Invalid server response", "error");
                    }
                    else if (response.data == "exception") {
                        swal("Error ", "Invalid server response", "error");
                    }
                    else {

                        $scope.AllTimingList = response.data;

                    }

                });
                if (ManagerID != null || ManagerID != 0 || !ManagerID) {

                    var Manager = { ManagerID: ManagerID };
                    App.POST(Manager, services + "GetManagerDetailByID").then(function (response) {
                        if (response.data == "exception") {
                            swal("Error ", "Invalid server response", "error");
                        }
                        else if (response.data == "null") {
                            swal("Error ", "Invalid server response", "error");
                        }
                        else {
                            console.log(response.data);
                            $scope.Manager = response.data.Manager;
                            $scope.Manager.Practices = response.data.Practices;

                            //$scope.Manager = response.data.Usr;
                            // $scope.ManagerPractices = response.data.Practices;
                        }
                    });

                }
                else {
                    swal("Error", "Invalid Request.. No User Found", "error");
                }

                $scope.UpdateManager = function (data) {
                    $scope.message = '';

                    if ($scope.fmUptdManager.$valid) {
                        console.log(data);
                        debugger;
                        App.POST(data, services + "UpdateManager").then(function (response) {


                            if (response.data == "exception") {

                                swal({
                                    title: "Error !!",
                                    text: "Invalid Server Response.Please Try Again Later",
                                    icon: "error",
                                    buttons: true,
                                    dangerMode: true,
                                }).then((ok) => {
                                    window.location.href = "./managers";
                                });
                            }

                            else if (response.data == "updated") {

                                swal({
                                    title: "Success !!",
                                    text: "Manager Updated Successfully",
                                    icon: "success",
                                    buttons: true,
                                    dangerMode: true,
                                }).then((ok) => {
                                    window.location.href = "../managers";
                                });

                            }


                        });

                    }
                    else {
                        $scope.message = 'Please fill the required fields';
                    }
                };
                $scope.DeleteManager = function (User) {
                    swal({
                        title: "Confirmation",
                        text: "Are you sure to delete ?",
                        icon: "warning",
                        buttons: {

                            cancel: true,
                            confirm: true,
                        },
                    })
                    .then((value) => {
                        if (value) {
                            App.POST(User, services + "DeleteManager").then(function (response) {
                                if (response.data === "deleted") {

                                    swal("Success! Manager has been deleted!", {
                                        icon: "success",
                                    }).then((ok) => {
                                        window.location.href = "../managers";
                                    });

                                }
                                else if (response.data === "fails") {
                                    swal("Alert! Error occure while deleting Manager!", {
                                        icon: "error",
                                    }).then((ok) => {
                                        window.location.href = "../managers";
                                    });
                                }
                                else if (response.data === "exception") {


                                    swal("Error! Invalid Server Response. Try Again!", {
                                        icon: "error",
                                    }).then((ok) => {
                                        window.location.href = "../Managers";
                                    });
                                }
                            });
                        }
                    });
                }
            }
        }
    });
});