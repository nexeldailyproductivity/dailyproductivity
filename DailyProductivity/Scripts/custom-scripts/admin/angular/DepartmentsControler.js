﻿App.controller("NewDepartment", function ($scope, App)
{
    App.GET($AdminUrl_S + "CheckManagerLoginSession").then(function (response) {
        var ManagerDetailObjct = {};
        if (response.data == "exception")
        {
            swal("Exception", "Invalid server response, Please try again ", "error").then(value=> {
                window.location.href = '/auth/logout'
            });
        }
        else if (response.data == "null")
        {
            swal("Exception", "Invalid server response, Please try again ", "error").then(value=> {
                window.location.href = '/home/dashboard'
            });
        }
        else if (response.data == "invalid")
        {
            swal("Exception", "Invalid server response, Please try again ", "error").then(value=> {
                window.location.href = '/auth/logout'
            });
        }
        else
        {
            ManagerDetailObjct = response.data;
            var services = $AdminUrl_S;
            if (ManagerDetailObjct.RoleID == 1)
            {
                $scope.SaveDepartment = function (Department)
                {
                    if (this.FmNewDepartment.$valid)
                    {
                        App.POST(Department, services + "SaveDepartmentDetails").then(function (response) {
                            if (response.data == "exception") {
                                swal("Error ", "Invalid server response", "error");
                            }
                            else if (response.data == "null") {
                                swal("Error ", "Invalid details provided", "error");
                            }
                            else if (response.data == "success") {
                                swal({
                                    title: "Success !!",
                                    text: "Department Added Successfully",
                                    icon: "success",
                                    buttons: true,
                                    dangerMode: true,
                                });
                            }
                        });
                    }
                    else
                    {
                        swal("Error ", "Data is not in well managed form", "error");
                    }
                }

                $scope.Cancel = function ()
                {
                    window.location.href = "../manage";
                }
            }
            else {
                swal("Warning", "You are not authorised to access this page", "error").then(value=>
                {
                    window.location.href = "/auth/logout";
                });
            }
        }
    });
});
App.controller("DepartmentDetailList", function ($scope, App) {







    App.GET($AdminUrl_S + "CheckManagerLoginSession").then(function (response) {
        var ManagerDetailObjct = {};
        if (response.data == "exception") {
            swal("Exception", "Invalid server response, Please try again ", "error").then(value=> {
                window.location.href = '/auth/logout'
            });
        }
        else if (response.data == "null") {
            swal("Exception", "Invalid server response, Please try again ", "error").then(value=> {
                window.location.href = '/home/dashboard'
            });
        }
        else if (response.data == "invalid") {
            swal("Exception", "Invalid server response, Please try again ", "error").then(value=> {
                window.location.href = '/auth/logout'
            });
        }
        else {
            ManagerDetailObjct = response.data;
            var services = $AdminUrl_S;
            if (ManagerDetailObjct.RoleID == 1) {


                App.GET(services + "GetDepartments").then(function (response) {
                    var DepartmentsDetail = [];
                    if (response.data == "exception") {
                        swal("Error ", "Invalid server response", "error");
                    }
                    else if (response.data == "null") {
                        swal("Error ", "No data found", "error");
                    }
                    else {
                        console.log(response.data);
                         DepartmentsDetail = response.data;
                        //  $(document).ready(function () {
                        console.log(DepartmentsDetail);

                      
                        // });
                    }
                    var table = $('#departments-datatable').DataTable({
                        aaData: DepartmentsDetail,
                        dom: "Bfrtip",
                        processing: true,

                        columns: [
                                {
                                    "mRender": function (data, type, row) {
                                        return '<span class="badge badge-pill badge-pink ks-label" style="width: 90px; height: 31px; padding: 9px; font-size: 13px;">' + row.DepartmentID + '</span>'
                                    }
                                },

                                { data: "DepartmentName" },
                                {
                                    "mRender": function (data, type, row) {
                                        return '<a class="btn btn-sm btn-success" href=manage/' + row.EDID + '>Manage';
                                    }
                                }

                        ],


                        //  lengthChange: false,
                        buttons: [

                            'colvis',
                            {
                                extend: 'copyHtml5',
                                exportOptions: {
                                    columns: [0, 1]
                                }
                            },
                            {
                                extend: 'excelHtml5',
                                exportOptions: {
                                    columns: [0, 1]
                                }
                            },
                            {
                                extend: 'pdfHtml5',
                                exportOptions: {
                                    columns: [0, 1]
                                }
                            },
                            {
                                extend: 'csvHtml5',
                                exportOptions: {
                                    columns: [0, 1]
                                }
                            },
                        ],
                        initComplete: function () {
                            $('.dataTables_wrapper select').select2({
                                minimumResultsForSearch: Infinity
                            });
                        },



                    });

                    table.buttons().container().appendTo('#ks-datatable_wrapper .col-md-6:eq(0)');
                });;
            }
            else {
                swal("Warning", "You are not authorised to access this page", "error").then(value=> {
                    window.location.href = "/auth/logout";
                });
            }
        }
    });
});

App.controller("UpdateDepartment", function ($scope, App) {
    App.GET($AdminUrl_S + "CheckManagerLoginSession").then(function (response) {
        var ManagerDetailObjct = {};
        if (response.data == "exception") {
            swal("Exception", "Invalid server response, Please try again ", "error").then(value=> {
                window.location.href = '/auth/logout'
            });
        }
        else if (response.data == "null") {
            swal("Exception", "Invalid server response, Please try again ", "error").then(value=> {
                window.location.href = '/home/dashboard'
            });
        }
        else if (response.data == "invalid") {
            swal("Exception", "Invalid server response, Please try again ", "error").then(value=> {
                window.location.href = '/auth/logout'
            });
        }
        else {
            ManagerDetailObjct = response.data;
            var services = $AdminUrl_S;
            if (ManagerDetailObjct.RoleID == 1) {

                if (DepartmentID != "" && DepartmentID != null && DepartmentID != 0) {
                    App.POST({ DepartmentID: DepartmentID }, services + "GetDepartmentDetails").then(function (response) {
                        if (response.data == "exception") {
                            swal("Exception", "Invalid server response, Please try again ", "error").then(value=> {
                                window.location.href = '/auth/logout'
                            });
                        }
                        else if (response.data == "null") {
                            swal("Exception", "No detail found  ", "error").then(value=> {
                                window.location.href = './departments'
                            });
                        }
                        else {
                            $scope.Department = response.data;
                        }
                    });
                }
                else { swal("Error ", "Invalid details provided", "error").then(value=> { window.location.href = "/departments"; }); }

                $scope.UpdateDepartment = function (Department) {
                    if (this.FmUpdtDepartment.$valid) {
                        App.POST(Department, services + "SaveDepartmentDetails").then(function (response) {
                            if (response.data == "exception") {
                                swal("Error ", "Invalid server response", "error");
                            }
                            else if (response.data == "null") {
                                swal("Error ", "Invalid details provided", "error");
                            }
                            else if (response.data == "success") {
                                swal({
                                    title: "Success !!",
                                    text: "Department Updated Successfully",
                                    icon: "success",
                                    buttons: true,
                                    dangerMode: true,
                                });
                            }
                        });
                    }
                    else {
                        swal("Error ", "Data is not in well managed form", "error");
                    }
                }
                $scope.DeleteDepartment = function (Department) {
                    swal({
                        title: "Confirmation",
                        text: "Are you sure to delete ?",
                        icon: "warning",
                        buttons: {

                            cancel: true,
                            confirm: true,
                        },
                    })
                    .then((value) => {
                        if (value) {
                            App.POST(Department, services + "DeleteDepartment").then(function (response) {
                                if (response.data === "deleted") {

                                    swal("Success! Department has been deleted!", {
                                        icon: "success",
                                    }).then(value=> {
                                        window.location.href = "departments";
                                    });

                                }
                                else if (response.data === "fails") {
                                    swal("Alert! Error occure while deleting practice!", {
                                        icon: "error",
                                    }).then(value=> {
                                        window.location.href = "/departments";
                                    });

                                }
                                else if (response.data === "exception") {


                                    swal("Error ", "Invalid server response", "error").then(value=> {
                                        window.location.href = "/departments";
                                    });

                                }
                            });
                        }
                    });
                }
            }
            else {
                swal("Warning", "You are not authorised to access this page", "error").then(value=> {
                    window.location.href = "/auth/logout";
                });
            }
        }
    });
});