﻿

App.controller("ManagersDetailList", function ($scope, App) {
    App.GET($AdminUrl_S + "CheckManagerLoginSession").then(function (response) {
        var AdminDetailObjct = {};
        if (response.data == "exception") {
            swal("Exception", "Invalid server response, Please try again ", "error").then(value=> {
                window.location.href = '/auth/logout'
            });
        }
        else if (response.data == "null") {
            swal("Exception", "Invalid server response, Please try again ", "error").then(value=> {
                window.location.href = '/home/dashboard'
            });
        }
        else if (response.data == "invalid") {
            swal("Exception", "Invalid server response, Please try again ", "error").then(value=> {
                window.location.href = '/auth/logout'
            });
        }
        else {
            AdminDetailObjct = response.data;
            var services = $AdminUrl_S;
            if (AdminDetailObjct.RoleID == 1) {
                App.GET(services + "GetManagersDetailList").then(function (response) {
                    var data = null;
                    if (response.data == "exception") {
                        swal("Error ", "Invalid server response", "error");
                    }
                    else if (response.data == "null") {
                        swal("Error ", "No Data Found", "info");
                    }
                    else {
                        console.log(response.data);
                        data = response.data;
                    }
                    var table = $('#ks-datatable').DataTable({
                        aaData: data,
                        dom: "Bfrtip",
                        processing: true,

                        columns: [
                                {
                                    "mRender": function (data, type, row) {
                                        return '<span class="badge badge-pill badge-pink ks-label" style="width: 90px; height: 31px; padding: 9px; font-size: 13px;">' + row.UserID + '</span>'
                                    }
                                },
                                {
                                    "mRender": function (data, type, row) {
                                        return row.FirstName + ' ' + row.LastName;
                                    }
                                },
                                { data: "UserName" },
                                { data: "Email" },
                                { data: "EmployeeNo" },
                                { data: "Designation" },
                                { data: "RoleName" },
                                { data: "Title" },
                                {
                                    "mRender": function (data, type, row) {
                                        if (row.Status == 'Active') {
                                            return '<label class="badge badge-info">' + row.Status + '</label>';
                                        }
                                        else if (row.Status != 'Active') { return '<label class="badge badge-danger">' + row.Status + '</label>'; }


                                    }
                                },
                                {
                                    "mRender": function (data, type, row) {
                                        return '<a class="btn btn-sm btn-success" href=edituser/' + row.UserID + '>Manage';
                                    }
                                }

                        ],


                        //  lengthChange: false,
                        buttons: [
                            'copyHtml5',
                            'excelHtml5',
                            'csvHtml5',
                            'pdfHtml5',
                            'colvis'
                        ],
                        initComplete: function () {
                            $('.dataTables_wrapper select').select2({
                                minimumResultsForSearch: Infinity
                            });
                        },



                    });

                    table.buttons().container().appendTo('#ks-datatable_wrapper .col-md-6:eq(0)');


                });;
            }
            else {
                swal("Warning", "You are not authorised to access this page", "error").then(value=> {
                    window.location.href = "/auth/logout";
                });
            }
        }
    });
});

App.factory('UserService', function ($http, $q) {

    return {

        GetRoles: function () {
            return $http.get($AdminUrl_S + "GetRoles");
        },
        GetDepartments: function () {
            return $http.get($AdminUrl_S + "GetDepartments");
        },
        GetAllPractices: function () {
            return $http.get($AdminUrl_S + "GetAllPractices");
        },
        GetShiftTimings: function () {
            return $http.get($AdminUrl_S + "GetShiftTimings");
        },
        SaveUser: function (data) {
            var defer = $q.defer();
            console.log(data);
            $http({
                url: $AdminUrl_S + 'SaveUser',
                method: "POST",
                data: data
            }).then(function (d) {
                defer.resolve(d);
            }), function (e) {
                //callback after failed
                alert("Error");

                defer.reject(e);
            };
            return defer.promise;
        },
        DeleteUser: function (data) {
            var defer = $q.defer();
            console.log(data);
            $http({
                url: $AdminUrl_S + 'DeleteUser',
                method: "POST",
                data: data
            }).then(function (d) {
                defer.resolve(d);
            }), function (e) {
                //callback after failed
                alert("Error");

                defer.reject(e);
            };
            return defer.promise;
        },
        GetUserDetailList: function () {
            return $http.get($AdminUrl_S + "GetUserDetailList");
        },
        GetUserDetailByID: function (data) {

            var defer = $q.defer();
            console.log(data);
            $http({
                url: $AdminUrl_S + 'GetUserDetailByID',
                method: "POST",
                data: data
            }).then(function (d) {
                defer.resolve(d);
            }), function (e) {
                //callback after failed
                alert("Error");

                defer.reject(e);
            };
            return defer.promise;
        },
        UpdateUser: function (data) {
            var defer = $q.defer();
            console.log(data);
            $http({
                url: $AdminUrl_S + 'UpdateUser',
                method: "POST",
                data: data
            }).then(function (d) {
                defer.resolve(d);
            }), function (e) {
                //callback after failed
                alert("Error");

                defer.reject(e);
            };
            return defer.promise;
        },
    }




});