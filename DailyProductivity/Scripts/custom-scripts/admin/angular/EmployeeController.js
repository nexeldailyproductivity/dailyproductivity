﻿App.controller("NewEmployee", function ($scope, App, $uibModal, Upload, $timeout) {
    App.GET($AdminUrl_S + "CheckManagerLoginSession").then(function (response) {
        var ManagerDetailObjct = {};
        if (response.data == "exception") {
            swal("Exception", "Invalid server response, Please try again ", "error").then(value=> {
                window.location.href = '/auth/logout'
            });
        }
        else if (response.data == "null") {
            swal("Exception", "Invalid server response, Please try again ", "error").then(value=> {
                window.location.href = '/home/dashboard'
            });
        }
        else if (response.data == "invalid") {
            swal("Exception", "Invalid server response, Please try again ", "error").then(value=> {
                window.location.href = '/auth/logout'
            });
        }
        else {
            ManagerDetailObjct = response.data;
            var services = $AdminUrl_S;
            if (ManagerDetailObjct.RoleID == 1) {
                App.GET(services + "GetRoles").then(function (response) {
                    if (response.data == "null") {
                        swal("Error ", "Invalid server response", "error");
                    }
                    else if (response.data == "exception") {
                        swal("Error ", "Roles list is empty", "error");
                    }
                    else {

                        $scope.RoleList = response.data;
                        $scope.Role = $scope.RoleList[1].RoleName;
                    }

                });

                App.GET(services + "GetDepartments").then(function (response) {
                    if (response.data == "null") {
                        swal("Error ", "No departments found", "error");
                    }
                    else if (response.data == "exception") {
                        swal("Error ", "Invalid server response", "error");
                    }
                    else {

                        $scope.DepartmentList = response.data;
                        $scope.DepartmentID = $scope.DepartmentList[0].DepartmentName;
                    }

                });
                App.GET(services + "GetAllPractices").then(function (response) {
                    if (response.data == "null") {
                        swal("Error ", "No practices found", "error");
                    }
                    else if (response.data == "exception") {
                        swal("Error ", "Invalid server response", "error");
                    }
                    else {

                        $scope.AllPracticesList = response.data;

                    }

                });
                App.GET(services + "GetShiftTimings").then(function (response) {
                    if (response.data == "null") {
                        swal("Error ", "No timing details found", "error");
                    }
                    else if (response.data == "exception") {
                        swal("Error ", "Invalid server response", "error");
                    }
                    else {

                        $scope.AllTimingList = response.data;

                    }

                });
                $scope.SaveEmployee = function (Employee) {
                    if ($scope.fmNewEmployee.$valid) {
                        console.log(Employee);
                         ;
                        Employee.RoleID = 2;

                        if ($scope.CropedProfileImage != null) {

                            Upload.upload({
                                url: services + 'Upload',
                                data: {
                                    file: Upload.dataUrltoBlob($scope.CropedProfileImage, "pro_" + Employee.EmployeeNo)
                                },
                            }).then(function (response) {
                                Employee.UserPic = response.data.path;
                                $timeout(function () {

                                    $scope.result = response.data;
                                });
                            }, function (response) {


                                if (response.status > 0)
                                    $scope.errorMsg = response.status + ': ' + response.data;
                            }, function (evt) {
                                $scope.progress = parseInt(100.0 * evt.loaded / evt.total);
                            });
                             ;
                        }
                        App.POST(Employee, services + "SaveEmployee").then(function (response) {


                            if (response.data == "exception") {

                                swal({
                                    title: "Error !!",
                                    text: "Invalid Server Response.Please Try Again Later",
                                    icon: "error",
                                    buttons: true,
                                    dangerMode: true,
                                }).then((ok) => {
                                    window.location.href = $AdminUrl + 'user/adduser';
                                });
                            }
                            else if (response.data == "alreadyexisit") {
                                swal("Already Exist!", "User already exist please choose another.", "info");
                            }
                            else if (response.data == "fillfields") {
                                swal("Fill Fields!", "Please fill all the fileds", "info");
                            }
                            else if (response.data == "success") {

                                swal({
                                    title: "Success !!",
                                    text: "Employee Added Successfully",
                                    icon: "success",
                                    buttons: true,
                                    dangerMode: true,
                                }).then((ok) => {
                                    window.location.href = "employee";
                                });

                            }


                        });

                    }
                };
                $scope.Cancel = function () {
                    window.location.href = "manage";
                }


                $scope.CancelImage = function () {
                    $scope.CropedProfileImage = null;
                    $("#upload").val("");
                }



                






               


                $scope.upload = function (dataUrl, name) {
                    console.log(dataUrl);  ;
                    Upload.upload({
                        url: 'http://localhost:54651/Theme/',
                        data: {
                            file: Upload.dataUrltoBlob(dataUrl, "imgmyyyy")
                        },
                    }).then(function (response) {
                        $timeout(function () {
                            $scope.result = response.data;
                        });
                    }, function (response) {
                        if (response.status > 0) $scope.errorMsg = response.status
                            + ': ' + response.data;
                    }, function (evt) {
                        $scope.progress = parseInt(100.0 * evt.loaded / evt.total);
                    });
                }

                $scope.cropped = {
                    source: 'https://raw.githubusercontent.com/Foliotek/Croppie/master/demo/demo-1.jpg'
                };
                
                $('#upload').on('change', function () {
                    var input = this;
                   
                    var ProfileUploaderModel = $uibModal.open({
                        animation: true,
                        templateUrl: 'ProfilePicUploadTmp',
                        controller: 'ProfilePicUploadCtrl',
                        backdrop: false,
                        size: 'lg',
                        resolve: {
                            input: input
                        }
                    });
                    ProfileUploaderModel.result.then(function (response) {
                        $scope.progress = 0;
                        console.log(response);  ;
                        $scope.CropedProfileImage = response.image.cropedimage;
                     //   $scope.upload = function (dataUrl, name) {

                      
                        
                       // }

                    });
                 
                });
               
                
            }
            else {
                swal("Warning", "You are not authorised to access this page", "error").then(value=> {
                    window.location.href = "/auth/logout";
                });
            }
        }
    });
});

App.controller("EmployeeDetailList", function ($scope, App) {
    App.GET($AdminUrl_S + "CheckManagerLoginSession").then(function (response) {
        var ManagerDetailObjct = {};
        if (response.data == "exception") {
            swal("Exception", "Invalid server response, Please try again ", "error").then(value=> {
                window.location.href = '/auth/logout'
            });
        }
        else if (response.data == "null") {
            swal("Exception", "Invalid server response, Please try again ", "error").then(value=> {
                window.location.href = '/home/dashboard'
            });
        }
        else if (response.data == "invalid") {
            swal("Exception", "Invalid server response, Please try again ", "error").then(value=> {
                window.location.href = '/auth/logout'
            });
        }
        else {
            ManagerDetailObjct = response.data;
            var services = $AdminUrl_S;
            if (ManagerDetailObjct.RoleID == 1) {
                App.GET(services + "GetEmployeeDetailList").then(function (response) {
                    if (response.data == "exception") {
                        swal("Error ", "Invalid server response", "error");
                    }
                    else if (response.data == "null") {
                        swal("Error ", "Invalid server response", "error");
                    }
                    else {
                        console.log(response.data);
                        var UserDetailList = response.data;
                        //  $(document).ready(function () {
                        console.log(UserDetailList);

                        var table = $('#ks-datatable').DataTable({
                            aaData: UserDetailList,
                            dom: "Bfrtip",
                            processing: true,

                            columns: [
                                    {
                                        "mRender": function (data, type, row) {
                                            return '<span class="badge badge-pill badge-pink ks-label" style="width: 90px; height: 31px; padding: 9px; font-size: 13px;">' + row.EmployeeID + '</span>'
                                        }
                                    },
                                    {
                                        "mRender": function (data, type, row) {
                                            return row.FirstName + ' ' + row.LastName;
                                        }
                                    },
                                    { data: "UserName" },
                                    { data: "Email" },
                                    { data: "EmployeeNo" },
                                    { data: "Designation" },
                                    { data: "DepartmentName" },
                                    { data: "RoleName" },
                                    { data: "Title" },
                                    {
                                        "mRender": function (data, type, row) {
                                            if (row.Status == 'Active') {
                                                return '<label class="badge badge-info">' + row.Status + '</label>';
                                            }
                                            else if (row.Status != 'Active') { return '<label class="badge badge-danger">' + row.Status + '</label>'; }


                                        }
                                    },
                                    {
                                        "mRender": function (data, type, row) {
                                            return '<a class="btn btn-sm btn-success" href=manage/' + row.EUID + '>Manage';
                                        }
                                    }

                            ],


                            //  lengthChange: false,
                            buttons: [

                                'colvis',
                                {
                                    extend: 'copyHtml5',
                                    exportOptions: {
                                        columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
                                    }
                                },
                                {
                                    extend: 'excelHtml5',
                                    exportOptions: {
                                        columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
                                    }
                                },
                                {
                                    extend: 'pdfHtml5',
                                    exportOptions: {
                                        columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
                                    }
                                },
                                {
                                    extend: 'csvHtml5',
                                    exportOptions: {
                                        columns: [0, 1, 2, 3,4,5,6,7,8,9]
                                    }
                                },
                            ],
                            initComplete: function () {
                                $('.dataTables_wrapper select').select2({
                                    minimumResultsForSearch: Infinity
                                });
                            },



                        });

                        table.buttons().container().appendTo('#ks-datatable_wrapper .col-md-6:eq(0)');
                        // });
                    }

                });;
            }
            else {
                swal("Warning", "You are not authorised to access this page", "error").then(value=> {
                    window.location.href = "/auth/logout";
                });
            }
        }
    });
});

App.controller("UpdateEmployee", function ($scope, App) {
    App.GET($AdminUrl_S + "CheckManagerLoginSession").then(function (response) {
        var ManagerDetailObjct = {};
        if (response.data == "exception") {
            swal("Exception", "Invalid server response, Please try again ", "error").then(value=> {
                window.location.href = '/auth/logout'
            });
        }
        else if (response.data == "null") {
            swal("Exception", "Invalid server response, Please try again ", "error").then(value=> {
                window.location.href = '/home/dashboard'
            });
        }
        else if (response.data == "invalid") {
            swal("Exception", "Invalid server response, Please try again ", "error").then(value=> {
                window.location.href = '/auth/logout'
            });
        }
        else {
            ManagerDetailObjct = response.data;
            var services = $AdminUrl_S;
            if (ManagerDetailObjct.RoleID == 1) {
                App.GET(services + "GetRoles").then(function (response) {
                    if (response.data == "null") {
                        swal("Error ", "Invalid server response", "error");
                    }
                    else if (response.data == "exception") {
                        swal("Error ", "Invalid server response", "error");
                    }
                    else {

                        $scope.RoleList = response.data;
                        $scope.Role = $scope.RoleList[1].RoleName;
                    }

                });

                App.GET(services + "GetDepartments").then(function (response) {
                    if (response.data == "null") {
                        swal("Error ", "Invalid server response", "error");
                    }
                    else if (response.data == "exception") {
                        swal("Error ", "Invalid server response", "error");
                    }
                    else {

                        $scope.DepartmentList = response.data;
                        $scope.Employee.DepartmentID = $scope.DepartmentList[0].DepartmentID;
                       
                    }

                });
                App.GET(services + "GetAllPractices").then(function (response) {
                    if (response.data == "null") {
                        swal("Error ", "Invalid server response", "error");
                    }
                    else if (response.data == "exception") {
                        swal("Error ", "Invalid server response", "error");
                    }
                    else {

                        $scope.AllPracticesList = response.data;

                    }

                });
                App.GET(services + "GetShiftTimings").then(function (response) {
                    if (response.data == "null") {
                        swal("Error ", "Invalid server response", "error");
                    }
                    else if (response.data == "exception") {
                        swal("Error ", "Invalid server response", "error");
                    }
                    else {

                        $scope.AllTimingList = response.data;

                    }

                });
                if (EmployeeID != null || EmployeeID != 0 || !EmployeeID) {

                    var Employee = { EmployeeID: EmployeeID };
                    App.POST(Employee, services + "GetEmployeeDetailByID").then(function (response) {
                        if (response.data == "exception") {
                            swal("Error ", "Invalid server response", "error");
                        }
                        else if (response.data == "null") {
                            swal("Error ", "Invalid server response", "error");
                        }
                        else {
                            
                            $scope.DepartmentID = 1;
                           console.log(response.data); 
                          //  $scope.Employee.Practices = response.data.Practices.PracticeID;
                           $scope.Employee = response.data.Employee;
                           //  $scope.EmployeePractices = response.data.Practices.PracticeID;
                        }
                    });

                }
                else {
                    swal("Error", "Invalid Request.. No User Found", "error");
                }

                $scope.UpdateEmployee = function (data) {
                    $scope.message = '';
                    console.log(data);
                     ;

                    if ($scope.fmUpdtEmployee.$valid) {
                        console.log(data);
                         ;
                        App.POST(data, services + "UpdateEmployees").then(function (response) {


                            if (response.data == "exception") {

                                swal({
                                    title: "Error !!",
                                    text: "Invalid Server Response.Please Try Again Later",
                                    icon: "error",
                                    buttons: true,
                                    dangerMode: true,
                                }).then((ok) => {
                                    window.location.href = "../employees";
                                });
                            }

                            else if (response.data == "updated") {

                                swal({
                                    title: "Success !!",
                                    text: "Employee Updated Successfully",
                                    icon: "success",
                                    buttons: true,
                                    dangerMode: true,
                                }).then((ok) => {
                                    window.location.href = "../employees";
                                });

                            }


                        });

                    }
                    else {
                        $scope.message = 'Please fill the required fields';
                    }
                };
                $scope.DeleteEmployee = function (User) {
                    swal({
                        title: "Confirmation",
                        text: "Are you sure to delete ?",
                        icon: "warning",
                        buttons: {

                            cancel: true,
                            confirm: true,
                        },
                    })
                    .then((value) => {
                        if (value) {
                            App.POST(User, services + "DeleteEmployees").then(function (response) {
                                if (response.data === "deleted") {

                                    swal("Success! Employee has been deleted!", {
                                        icon: "success",
                                    }).then((ok) => {
                                        window.location.href = "../employees";
                                    });

                                }
                                else if (response.data === "fails") {
                                    swal("Alert! Error occure while deleting Employee!", {
                                        icon: "error",
                                    }).then((ok) => {
                                        window.location.href = "../employees";
                                    });
                                }
                                else if (response.data === "exception") {


                                    swal("Error! Invalid Server Response. Try Again!", {
                                        icon: "error",
                                    }).then((ok) => {
                                        window.location.href = "../employees";
                                    });
                                }
                            });
                        }
                    });
                }
            }
        }
    });







});

App.controller("ProfilePicUploadCtrl", function ($scope, input, $uibModalInstance) {
    
    $scope.cropped = {
        source: 'https://raw.githubusercontent.com/Foliotek/Croppie/master/demo/demo-1.jpg'
    };

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                // bind new Image to Component

                 $scope.$apply(function () {
                         $scope.cropped.source = e.target.result;
                     
                });
               
            }

            reader.readAsDataURL(input.files[0]);
        }

    $scope.Cropedone = function (image) {
        console.log(image);
        $uibModalInstance.close({ msg: "success", image: image });
    }
    

});