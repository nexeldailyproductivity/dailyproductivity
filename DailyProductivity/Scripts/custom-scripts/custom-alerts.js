﻿(function ($) {
    $(document).ready(function () {
        $.alert({
            title: 'A draggable dialog',
            content: 'This dialog is draggable, use the title to drag it around. It wont touch the screen borders',
            type: 'info',
            draggable: true
        });
        setTimeout(1);
        
    });
})(jQuery);