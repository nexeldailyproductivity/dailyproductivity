﻿class Notify {
   
   
    constructor(pubnub) {
        this.pubnub = new PubNub();
        alert(this.pubnub.getUUID());
    }

     do_subscribe(key) {
        if (key == "") {
            this.pubnub.subscribeKey = "";
        }
        this.pubnub.subscribeKey = key;
    }
     do_publish(key) {
        if (key == "") {
            this.pubnub.publishKey = "";
        }
        this.pubnub.publishKey = key;
    }

     send_notification(title, body, footer) {
        this.pubnub.publish(
            {
                message: {
                    header: '',
                    body: '',
                    footer: ''
                },
                channel: 'PMS'
            },
            function (status, response) {
                if (status.error) {
                    console.log(status)
                } else {
                    console.log("message Published w/ timetoken", response.timetoken)
                }
            }
        );
    }
     receive_notificatin() {
        pubnub.addListener({
            message: function (message) {
                console.log('message came in: ', message)
            }
        });
     }

};

class User {

    constructor(name) {
        this.name = name;
    }

    sayHi() {
        alert(this.name);
    }

}

