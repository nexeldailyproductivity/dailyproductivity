﻿
/* Path for local */
var $rootpath = "http://localhost:64298/";
/* Path for live */
//var $rootpath = "/pms/";
var $AdminUrl = $rootpath + "admin/";
var $AdminUrl_S = $rootpath + "admin/services/";
var $services = $rootpath + "services/";
var depd = ['ui.bootstrap', 'ngAnimate', 'angularCroppie', 'ngFileUpload']
var App = angular.module("PMS", depd);
App.factory("App", function ($http, $q) {
    return {
        POST: function (data, url) {

            var defer = $q.defer();

            $http({
                url: url,
                method: "POST",
                data: data
            }).then(function (d) {
                defer.resolve(d);
            }), function (e) {
                //callback after failed
                alert("Error");

                defer.reject(e);
            };
            return defer.promise;
        },
        GET: function (url) {
            return $http.get(url);
        },
    }
});
var options = [{
    'Id': 1,
    'Name': 'Batman',
    'Costume': 'Black'
}, {
    'Id': 2,
    'Name': 'Superman',
    'Costume': 'Red & Blue'
}];




