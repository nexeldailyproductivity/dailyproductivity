﻿App.controller("DoProductivity", function ($scope, $filter, $uibModal, App) {
    function send_notification(msg) {
        var pubnub = new PubNub({
            subscribeKey: "sub-c-24faa17c-a470-11e8-bb88-163ac01f2f4e",

            publishKey: "pub-c-5220b6c6-253e-40f7-ab0e-af9f93394609",
        });


        pubnub.publish(
            {
                message: msg,
                channel: 'Activity'
            },
            function (status, response) {
                if (status.error) {
                } else {
                    var Notifications = {
                        EmployeeID: msg.user.EmployeeID,
                        Message: msg.title,
                        Link: '/notification/' + msg.user.EmployeeID,

                    }
                    App.POST(Notifications, $services + "SaveNotifications");
                    console.log("message Published w/ timetoken", response)
                }
            }
        );
    }

    App.GET($services + "CheckEmployeeLoginSession").then(function (response) {
        var EmployeeDetailObjct = {};
        if (response.data == "exception") {
            swal("Exception", "Invalid server response, Please try again ", "error").then(value=> {
                window.location.href = '/auth/logout'
            });
        }
        else if (response.data == "null") {
            swal("Exception", "Invalid server response, Please try again ", "error").then(value=> {
                window.location.href = '/home/dashboard'
            });
        }
        else if (response.data == "invalid") {
            swal("Exception", "Invalid server response, Please try again ", "error").then(value=> {
                window.location.href = '/auth/logout'
            });
        }
        else {
            var EmployeePractices = {};
            EmployeeDetailObjct = response.data;
            if (EmployeeDetailObjct.RoleID != 1) {
                $scope.ObjEmployee = EmployeeDetailObjct;
                App.POST(EmployeeDetailObjct, $services + "GetEmployeesPracticesList").then(function (response) {

                    if (response.data == "null") {
                        swal("Error ", "Invalid server response", "error");
                    }
                    else if (response.data == "exception") {
                        swal("Error ", "Invalid server response", "error");
                    }
                    else {

                        $scope.EmployeePracticesList = response.data;


                    }
                    $("#loader").hide();
                });

                if (EmployeeDetailObjct.DepartmentID == 1) {
                    $scope.SaveCredentials = function (CredentialsData, Practice) {
                        if (!$("#DateTime").val()) {
                            swal("Error ", "Please select date first", "error");
                        }
                        else if (Practice == null) {
                            swal("Error ", "Please select practice first", "error");
                        }
                        else if (!this.C_form.$valid) {
                            swal("Error ", "Data is not in well managed form", "error");
                        }
                        else {
                            CredentialsData.CredentialingDateTime = $("#DateTime").val();
                            CredentialsData.EmployeeID = EmployeeDetailObjct.EmployeeID;
                            CredentialsData.PracticeID = Practice.PracticeID;
                            App.POST(CredentialsData, $services + "SaveCredentials").then(function (response) {
                                if (response.data == "exception") {
                                    swal("Error ", "Invalid server response", "error");
                                }
                                else if (response.data == "fails") {
                                    swal("Error ", "An error occure while saving", "error");
                                }
                                else if (response.data == "exists") {

                                    swal("Alert", "Record for selected date already exists.", "info").then(value=> {
                                        //  window.location.href = "/productivity/assignments";

                                    });

                                }
                                else if (response.data == "success") {

                                    msg = {
                                        'user': EmployeeDetailObjct,
                                        'title': EmployeeDetailObjct.FirstName + '' + EmployeeDetailObjct.LastName + ' Just Added Credentials Details'
                                    }
                                    send_notification(msg);

                                    swal("Success", "Successfully added", "success").then(value=> {


                                    });

                                }
                            });
                        }
                    }
                }
                else if (EmployeeDetailObjct.DepartmentID == 2) {
                    $scope.SavePatientHelpDeskPHD = function (PatientHelpDeskList) {
                        if (!$("#DateTime").val()) {
                            swal("Error ", "Please select date first", "error");
                        }
                        else if (!this.phd_form.$valid) {
                            swal("Error ", "Data is not in well managed form", "error");
                        }
                        else {
                            PatientHelpDeskData = [];
                            angular.forEach(PatientHelpDeskList, function (PatientHelpDesk, key) {
                                PatientHelpDesk.PracticeID = key;
                                PatientHelpDesk.EmployeeID = EmployeeID,
                                PatientHelpDesk.PatientHelpDeskDateTime = $("#DateTime").val()

                                this.push(PatientHelpDesk);

                            }, PatientHelpDeskData);

                            App.POST(PatientHelpDeskData, $services + "SavePatientHelpDeskPHD").then(function (response) {
                                if (response.data == "exception") {
                                    swal("Error ", "Invalid server response", "error");
                                }
                                else if (response.data == "fails") {
                                    swal("Error ", "An error occure while saving", "error");
                                }
                                else if (response.data == "exists") {

                                    swal("Alert", "Record for selected date already exists.", "info").then(value=> {

                                    });
                                }
                                else if (response.data == "success") {
                                    msg = {

                                        'user': EmployeeDetailObjct,
                                        'title': EmployeeDetailObjct.FirstName + '' + EmployeeDetailObjct.LastName + ' Just Added Patient Help Desk Details'
                                    }
                                    send_notification(msg);
                                    swal("Success", "Successfully added", "success").then(value=> {
                                        window.location.href = "/productivity/assignments";

                                    });

                                }
                            });;
                        }
                    }
                }
                else if (EmployeeDetailObjct.DepartmentID == 3 || EmployeeDetailObjct.DepartmentID == 4 || EmployeeDetailObjct.DepartmentID == 5 || EmployeeDetailObjct.DepartmentID == 6 || EmployeeDetailObjct.DepartmentID == 7 || EmployeeDetailObjct.DepartmentID == 8 || EmployeeDetailObjct.DepartmentID == 9) {


                    $scope.SaveAssignmentsData = function (AssignmentData, Practice) {

                        if (!$("#DateTime").val()) {
                            swal("Error ", "Please select date first", "error");
                        }
                        else if (Practice == null) {
                            swal("Error ", "Please select practice first", "error");
                        }
                        else if (!this.a_form.$valid) {
                            swal("Error ", "Data is not in well managed form", "error");
                        }
                        else {
                            AssignmentData.EmployeeID = EmployeeDetailObjct.EmployeeID;
                            AssignmentData.AssignmentDateTime = $("#DateTime").val();
                            AssignmentData.PracticeID = Practice.PracticeID;
                            AssignmentData.DepartmentID = EmployeeDetailObjct.DepartmentID;
                            App.POST(AssignmentData, $services + "SaveAssignmentData").then(function (response) {
                                if (response.data == "exception") {
                                    swal("Error ", "Invalid server response", "error");
                                }
                                else if (response.data == "fails") {
                                    swal("Error ", "An error occure while saving", "error");
                                }
                                else if (response.data == "exists") {

                                    swal("Alert", "Record for selected date already exists.", "info").then(value=> {

                                    });

                                }
                                else if (response.data == "success") {
                                    msg = {

                                        'user': EmployeeDetailObjct,
                                        'title': EmployeeDetailObjct.FirstName + '' + EmployeeDetailObjct.LastName + ' Just Added (' + Practice.PracticeName + ')  Assignment & Claims Details'
                                    }
                                    send_notification(msg);
                                    swal("Success", "Successfully added", "success").then(value=> {
                                        // window.location.href = "/productivity/assignments";

                                    });

                                }

                            });


                        }
                    }
                    $scope.SaveFinancials = function (FinancialsData, Practice) {
                        if (!$("#DateTime").val()) {
                            swal("Error ", "Please select date first", "error");
                        }

                        else if (Practice == null) {
                            swal("Error ", "Please select practice first", "error");
                        }
                        else if (!this.f_form.$valid) {
                            swal("Error ", "Data is not in well managed form", "error");
                        }
                        else {
                            console.log(EmployeeDetailObjct);
                            debugger;
                            FinancialsData.EmployeeID = EmployeeDetailObjct.EmployeeID;
                            FinancialsData.FinancialDateTime = $("#DateTime").val();
                            FinancialsData.PracticeID = Practice.PracticeID;
                            FinancialsData.DepartmentID = EmployeeDetailObjct.DepartmentID;
                            App.POST(FinancialsData, $services + "SaveFinancialsData").then(function (response) {
                                if (response.data == "exception") {
                                    swal("Error ", "Invalid server response", "error");
                                }
                                else if (response.data == "fails") {
                                    swal("Error ", "An error occure while saving", "error");
                                }
                                else if (response.data == "exists") {

                                    swal("Alert", "Record for selected date already exists.", "info").then(value=> {
                                        window.location.href = "/productivity/assignments";

                                    });

                                }
                                else if (response.data == "success") {
                                    msg = {

                                        'user': EmployeeDetailObjct,
                                        'title': EmployeeDetailObjct.FirstName + '' + EmployeeDetailObjct.LastName + ' Just Added (' + Practice.PracticeName + ') Financials Details'
                                    }
                                    send_notification(msg);
                                    swal("Success", "Successfully added", "success").then(value=> {
                                        //  window.location.href = "/productivity/assignments";

                                    });

                                }

                            });
                        }
                    }
                    $scope.SaveAgings = function (AgingData, Practice) {
                        if (!$("#DateTime").val()) {
                            swal("Error ", "Please select date first", "error");
                        }

                        else if (Practice == null) {
                            swal("Error ", "Please select practice first", "error");
                        }
                        else if (!this.AG_form.$valid) {
                            swal("Error ", "Data is not in well managed form", "error");
                        }
                        else {
                            AgingData.EmployeeID = EmployeeDetailObjct.EmployeeID;
                            AgingData.AgingDateTime = $("#DateTime").val();
                            AgingData.PracticeID = Practice.PracticeID;
                            AgingData.DepartmentID = EmployeeDetailObjct.DepartmentID;
                            App.POST(AgingData, $services + "SaveAgings").then(function (response) {
                                if (response.data == "exception") {
                                    swal("Error ", "Invalid server response", "error");
                                }
                                else if (response.data == "fails") {
                                    swal("Error ", "An error occure while saving", "error");
                                }
                                else if (response.data == "exists") {

                                    swal("Alert", "Record for selected date already exists.", "info").then(value=> {

                                    });

                                }
                                else if (response.data == "success") {
                                    msg = {

                                        'user': EmployeeDetailObjct,
                                        'title': EmployeeDetailObjct.FirstName + '' + EmployeeDetailObjct.LastName + ' Just Added ' + Practice.PracticeName + ' Aging Details'
                                    }
                                    send_notification(msg);
                                    swal("Success", "Successfully added", "success").then(value=> {
                                        // window.location.href = "/productivity/assignments";

                                    });

                                }

                            });
                        }
                    }

                    $scope.SavePPFinancials = function (FinancialsData, Practice) {
                        if (!$("#DateTime").val()) {
                            swal("Error ", "Please select date first", "error");
                        }

                        else if (Practice == null) {
                            swal("Error ", "Please select practice first", "error");
                        }
                        else if (!this.f_form.$valid) {
                            swal("Error ", "Data is not in well managed form", "error");
                        }
                        else {
                            console.log(EmployeeDetailObjct);
                            debugger;
                            FinancialsData.EmployeeID = EmployeeDetailObjct.EmployeeID;
                            FinancialsData.FinancialDateTime = $("#DateTime").val();
                            FinancialsData.PracticeID = Practice.PracticeID;
                            FinancialsData.DepartmentID = EmployeeDetailObjct.DepartmentID;
                            App.POST(FinancialsData, $services + "SavePPFinancialsData").then(function (response) {
                                if (response.data == "exception") {
                                    swal("Error ", "Invalid server response", "error");
                                }
                                else if (response.data == "fails") {
                                    swal("Error ", "An error occure while saving", "error");
                                }
                                else if (response.data == "exists") {

                                    swal("Alert", "Record for selected date already exists.", "info").then(value=> {
                                        window.location.href = "/productivity/assignments";

                                    });

                                }
                                else if (response.data == "success") {
                                    msg = {

                                        'user': EmployeeDetailObjct,
                                        'title': EmployeeDetailObjct.FirstName + '' + EmployeeDetailObjct.LastName + ' Just Added (' + Practice.PracticeName + ') Payment Posting Financials Details'
                                    }
                                    send_notification(msg);
                                    swal("Success", "Successfully added", "success").then(value=> {
                                        //  window.location.href = "/productivity/assignments";

                                    });

                                }

                            });
                        }
                    }
                }



                $scope.DoCorrection = function (DepartmentID) {
                    var FindProductivityModel = $uibModal.open(
                                {
                                    animation: true,
                                    templateUrl: 'FindProductivityModel',
                                    controller: 'FindProductivityController',
                                    backdrop: false,
                                    resolve: {
                                        EmployeeDetails: EmployeeDetailObjct
                                    }
                                }
                            );
                    FindProductivityModel.result.then(function (response) {

                        if (response.msg == "found") {
                            if (EmployeeDetailObjct.DepartmentID == 1) {

                                var CredentialModel = $uibModal.open({
                                    animation: true,
                                    templateUrl: 'ProductivityForms',
                                    backdrop: false,
                                    size: 'lg',
                                    controller: 'CredentialModelController',
                                    resolve: {
                                        Details: {
                                            EmployeeDetails: EmployeeDetailObjct,
                                            CredentialingData: response.data.record,
                                            PracticeDetail: response.data.Practice
                                        }
                                    }

                                });
                                CredentialModel.result.then(function (response) {
                                    if (response.msg == "success") {
                                        msg = {
                                            'user': EmployeeDetailObjct,
                                            'title': EmployeeDetailObjct.FirstName + '' + EmployeeDetailObjct.LastName + ' Just Updated Credentials Details'
                                        }
                                        send_notification(msg);
                                        swal("Success", "Successfully updated", "success").then(value=> {
                                            window.location.href = "/productivity/assignments";
                                        });
                                    }
                                });
                            }
                            else if (EmployeeDetailObjct.DepartmentID == 2) {
                                var CredentialModel = $uibModal.open({
                                    animation: true,
                                    templateUrl: 'ProductivityForms',
                                    backdrop: false,
                                    size: 'lg',
                                    controller: 'CredentialModelController',
                                    resolve: {
                                        Details: {
                                            EmployeeDetails: EmployeeDetailObjct,
                                            CredentialingData: response.data.record,
                                            PracticeDetail: response.data.Practice
                                        }
                                    }

                                });
                            }
                            else if (EmployeeDetailObjct.DepartmentID == 3 || EmployeeDetailObjct.DepartmentID == 4 || EmployeeDetailObjct.DepartmentID == 5 || EmployeeDetailObjct.DepartmentID == 6 || EmployeeDetailObjct.DepartmentID == 7 || EmployeeDetailObjct.DepartmentID == 8) {
                              
                                var AssignmentModel = $uibModal.open({
                                    animation: true,
                                    templateUrl: 'ProductivityForms',
                                    backdrop: false,
                                    size: 'lg',
                                    controller: 'AssignmentModelController',
                                    resolve: {
                                        Details: {
                                            EmployeeDetails: EmployeeDetailObjct,
                                            LogDetail: {
                                                AssignmentsLog: response.data.record.AssignmentsLog,
                                                FinancialLog: response.data.record.FinancialLog,
                                                AgingLog: response.data.record.AgingLog,
                                                FilterDate: response.data.FilterDate
                                            },
                                            PracticeDetail: response.data.Practice
                                        }
                                    }

                                });
                                AssignmentModel.result.then(function (response) {
                                    console.log(response); debugger;
                                    if (response.msg == "success") {
                                        msg = {
                                            'user': EmployeeDetailObjct,
                                            'title': EmployeeDetailObjct.FirstName + '' + EmployeeDetailObjct.LastName + ' Just Updated Credentials Details'
                                        }
                                        send_notification(msg);
                                        swal("Success", "Successfully updated", "success").then(value=> {
                                            //window.location.href = "/productivity/assignments";
                                        });
                                    }
                                });
                            }
                            else if (EmployeeDetailObjct.DepartmentID == 9)
                            {
                                var AssignmentModel = $uibModal.open({
                                    animation: true,
                                    templateUrl: 'ProductivityForms',
                                    backdrop: false,
                                    size: 'lg',
                                    controller: 'AssignmentModelController',
                                    resolve: {
                                        Details: {
                                            EmployeeDetails: EmployeeDetailObjct,
                                            LogDetail: {
                                                AssignmentsLog: response.data.record.AssignmentsLog,
                                                FinancialLog: response.data.record.FinancialLog,
                                                FilterDate: response.data.FilterDate
                                            },
                                            PracticeDetail: response.data.Practice
                                        }
                                    }

                                });
                                AssignmentModel.result.then(function (response) {
                                    console.log(response); debugger;
                                    if (response.msg == "success") {
                                        msg = {
                                            'user': EmployeeDetailObjct,
                                            'title': EmployeeDetailObjct.FirstName + '' + EmployeeDetailObjct.LastName + ' Just Updated Credentials Details'
                                        }
                                        send_notification(msg);
                                        swal("Success", "Successfully updated", "success").then(value=> {
                                            //window.location.href = "/productivity/assignments";
                                        });
                                    }
                                });
                            }
                        }


                    });

                }
                $scope.SendEmail = function () {
                    if (EmployeeDetailObjct.DepartmentID == 7) {
                        var FindProductivityModel = $uibModal.open(
                               {
                                   animation: true,
                                   templateUrl: 'EmailProductivityModelBilling',
                                   controller: function ($scope, EmployeeDetails) {

                                       $scope.ObjEmployee = EmployeeDetails;

                                       $scope.FindProductivity = function (FilterDate) {
                                           objFilter = {
                                               AssignmentDateTime: FilterDate,
                                               EmployeeID: EmployeeDetails.EmployeeID,
                                               FinancialDateTime: FilterDate
                                           }
                                           AssignmentLog = []; FinancialLog = [];
                                           App.POST(objFilter, $services + "GetEmployeesAssignmentsList").then(function (assgnment) {
                                               App.POST(objFilter, $services + "GetEmployeesFinancialList").then(function (financial) {
                                                   if (assgnment.data != "null" && financial.data != "null") {
                                                       var EmailConfirmationModal = $uibModal.open(
                                                          {
                                                              animation: true,
                                                              templateUrl: 'EmailConfirmationModal',
                                                              controller: 'EmailConfirmationController',
                                                              backdrop: false,
                                                              resolve: {
                                                                  Details: {
                                                                      EmployeeDetails: EmployeeDetails,
                                                                      AssignmentData: assgnment.data,
                                                                      FinancialData: financial,
                                                                      FilterDate: FilterDate
                                                                  }
                                                              }
                                                          }

                                                      );
                                                       EmailConfirmationModal.result.then(function (response) {
                                                           if (response.msg == "success") {
                                                               swal("Success", "Email sent successfully to your supervisor", "success");
                                                           }
                                                       });
                                                       $scope.Cancel = function () {
                                                           window.location.href = "/home/dashboard";
                                                       }
                                                   }
                                                   else {
                                                       swal("Sory no data found");
                                                   }
                                               });
                                           });
                                       }

                                   },
                                   backdrop: false,
                                   resolve: {
                                       EmployeeDetails: EmployeeDetailObjct
                                   }
                               }
                           );
                        
                    }
                    else {

                        var FindProductivityModel = $uibModal.open(
                                {
                                    animation: true,
                                    templateUrl: 'FindProductivityModel',
                                    controller: 'FindProductivityController',
                                    backdrop: false,
                                    resolve: {
                                        EmployeeDetails: EmployeeDetailObjct
                                    }
                                }
                            );
                        FindProductivityModel.result.then(function (response) {
                            console.log(response); debugger;
                            if (response.msg == "found") {

                                if (EmployeeDetailObjct.DepartmentID == 1) {
                                    var EmailConfirmationModal = $uibModal.open(
                                   {
                                       animation: true,
                                       templateUrl: 'EmailConfirmationModal',
                                       controller: 'EmailConfirmationController',
                                       backdrop: false,
                                       resolve: {
                                           Details: {
                                               EmployeeDetails: EmployeeDetailObjct,
                                               CredentialingData: response.data.record,
                                               PracticeDetail: response.data.Practice
                                           }
                                       }
                                   }
                               );
                                    EmailConfirmationModal.result.then(function (response) {
                                        if (response.msg == "success") {
                                            swal("Success", "Email sent successfully to your supervisor", "success");
                                        }
                                    });
                                }
                                else if (EmployeeDetailObjct.DepartmentID == 3 || EmployeeDetailObjct.DepartmentID == 4 || EmployeeDetailObjct.DepartmentID == 5 || EmployeeDetailObjct.DepartmentID == 7) {
                                    var EmailConfirmationModal = $uibModal.open(
                                   {
                                       animation: true,
                                       templateUrl: 'EmailConfirmationModal',
                                       controller: 'EmailConfirmationController',
                                       backdrop: false,
                                       resolve: {
                                           Details: {
                                               EmployeeDetails: EmployeeDetailObjct,
                                               AssignmentData: response.data.record.AssignmentsLog,
                                               FinancialData: response.data.record.FinancialLog,
                                               AgingData: response.data.record.AgingLog,
                                               PracticeDetail: response.data.Practice,
                                               FilterDate: response.data.FilterDate
                                           }
                                       }
                                   }

                               );
                                    EmailConfirmationModal.result.then(function (response) {
                                        if (response.msg == "success") {
                                            swal("Success", "Email sent successfully to your supervisor", "success");
                                        }
                                    });
                                }
                                else if (EmployeeDetailObjct.DepartmentID == 6 || EmployeeDetailObjct.DepartmentID == 8) {
                                    var EmailConfirmationModal = $uibModal.open(
                                   {
                                       animation: true,
                                       templateUrl: 'EmailConfirmationModal',
                                       controller: 'EmailConfirmationController',
                                       backdrop: false,
                                       resolve: {
                                           Details: {
                                               EmployeeDetails: EmployeeDetailObjct,
                                               AssignmentData: response.data.record.AssignmentsLog,
                                               PracticeDetail: response.data.Practice
                                           }
                                       }
                                   }
                               );
                                    EmailConfirmationModal.result.then(function (response) {
                                        if (response.msg == "success") {
                                            swal("Success", "Email sent successfully to your supervisor", "success");
                                        }
                                    });
                                }
                                else if (EmployeeDetailObjct.DepartmentID == 9) {
                                    var EmailConfirmationModal = $uibModal.open(
                                   {
                                       animation: true,
                                       templateUrl: 'EmailConfirmationModal',
                                       controller: 'EmailConfirmationController',
                                       backdrop: false,
                                       resolve: {
                                           Details: {
                                               EmployeeDetails: EmployeeDetailObjct,
                                               AssignmentData: response.data.record.AssignmentsLog,
                                               FinancialData: response.data.record.FinancialLog,
                                               PracticeDetail: response.data.Practice,
                                               FilterDate: response.data.FilterDate
                                           }
                                       }
                                   }
                               );
                                    EmailConfirmationModal.result.then(function (response) {
                                        if (response.msg == "success") {
                                            swal("Success", "Email sent successfully to your supervisor", "success");
                                        }
                                    });
                                }
                            }


                        });
                    }
                }
                $scope.Cancel = function () {
                    window.location.href = "/home/dashboard";
                }
            }
            else {
                swal("Warning", "You are not authorised to access this page", "error").then(value=> {
                    window.location.href = "/auth/logout";
                });
            }
        }
    });





});

App.controller("FindProductivityController", function ($scope, $uibModalInstance, EmployeeDetails, App, $filter) {

    $scope.ObjEmployee = EmployeeDetails;
    App.POST(EmployeeDetails, $services + "GetEmployeesPracticesList").then(function (response) {

        if (response.data == "null") {
            swal("Error ", "Invalid server response", "error");
        }
        else if (response.data == "exception") {
            swal("Error ", "Invalid server response", "error");
        }
        else {

            $scope.EmployeePracticesList = response.data;
        }
        $("#loader").hide();
    });

    if (EmployeeDetails.DepartmentID == 1) {
        $scope.FindProductivity = function (DateTime, Practice) {
            SelectedFilter = { EmployeeID: EmployeeDetails.EmployeeID, CredentialingDateTime: $filter('date')(DateTime, 'yyyy-MM-dd 00:00:00.000'), PracticeID: Practice.PracticeID };
            App.POST(SelectedFilter, $services + "GetEmployeesCredentialingRecord").then(function (response) {

                if (response.data == "null") {
                    swal("Sorry no data found");
                }
                else if (response.data == "exception") {
                    swal("Error ", "Invalid server response", "error");
                }
                else {
                    $uibModalInstance.close({ msg: "found", data: { record: response.data, Practice: Practice } });
                }
            });
        };
    }
    else if (EmployeeDetails.DepartmentID == 2) {
        $scope.FindProductivity = function (DateTime) {
            SelectedFilter = { EmployeeID: EmployeeDetails.EmployeeID, PatientHelpDeskDateTime: $filter('date')(DateTime, 'yyyy-MM-dd 00:00:00.000') };
            App.POST(SelectedFilter, $services + "GetEmployeesPaitentHelpDeskRecord").then(function (response) {
                console.log(response);
                if (response.data == "null") {
                    swal("Sorry no data found");
                }
                else if (response.data == "exception") {
                    swal("Error ", "Invalid server response", "error");
                }
                else {
                    $uibModalInstance.close({ msg: "found", data: { record: response.data } });
                }
            });

        };
    }
    else if (EmployeeDetails.DepartmentID == 3 || EmployeeDetails.DepartmentID == 4 || EmployeeDetails.DepartmentID == 5 || EmployeeDetails.DepartmentID == 6 || EmployeeDetails.DepartmentID == 7 || EmployeeDetails.DepartmentID == 8 || EmployeeDetails.DepartmentID == 9) {
        $scope.FindProductivity = function (DateTime, Practice) {
            AssignmentsFilter = { EmployeeID: EmployeeDetails.EmployeeID, AssignmentDateTime: $filter('date')(DateTime, 'yyyy-MM-dd 00:00:00.000'), PracticeID: Practice.PracticeID, DepartmentID: EmployeeDetails.DepartmentID };
            FinancialFilter = { DepartmentID: EmployeeDetails.DepartmentID, EmployeeID: EmployeeDetails.EmployeeID, FinancialDateTime: $filter('date')(DateTime, 'yyyy-MM-dd 00:00:00.000'), PracticeID: Practice.PracticeID, DepartmentID: EmployeeDetails.DepartmentID };
            AgingFilter = { EmployeeID: EmployeeDetails.EmployeeID, AgingDateTime: $filter('date')(DateTime, 'yyyy-MM-dd 00:00:00.000'), PracticeID: Practice.PracticeID, DepartmentID: EmployeeDetails.DepartmentID };
            AssignmentsRecord = {};
            FinancialRecord = {};
            AgingRecord = {};
            App.POST(AssignmentsFilter, $services + "GetEmployeesAssignmentsRecord").then(function (AssignmentResponse) {
                AssignmentsRecord = AssignmentResponse.data;
                if (EmployeeDetails.DepartmentID != 9) {
                    App.POST(FinancialFilter, $services + "GetEmployeesFinancialRecord").then(function (FinancialResponse) {
                        FinancialRecord = FinancialResponse.data;

                        App.POST(AgingFilter, $services + "GetEmployeesAgingRecord").then(function (AgingResponse) {
                            AgingRecord = AgingResponse.data;

                            if (AssignmentsRecord == "null" && FinancialRecord == "null" && AgingRecord == "null") {
                                swal("Sorry no data found");
                            }
                            else if (AssignmentsRecord == "exception" && FinancialRecord == "exception" && AgingRecord == "exception") {
                                swal("Error", "Invalid Server Response", "error");
                            }
                            else {


                                if (EmployeeDetails.DepartmentID == 6 || EmployeeDetails.DepartmentID == 8) {
                                    if (AssignmentsRecord == "null") {
                                        swal("Sorry no data found");
                                    } else {
                                        response = {
                                            AssignmentsLog: AssignmentsRecord,
                                        }
                                        console.log(response); debugger;
                                        $uibModalInstance.close({ msg: "found", data: { record: response, Practice: Practice, FilterDate: DateTime } });
                                    }
                                }
                                else if (EmployeeDetails.DepartmentID == 7) {
                                    if (AssignmentsRecord == "null" && FinancialRecord == "null") {
                                        swal("Sorry no data found");
                                    }
                                    else {
                                        response = {
                                            AssignmentsLog: AssignmentsRecord,
                                            FinancialLog: FinancialRecord,

                                        }
                                        $uibModalInstance.close({ msg: "found", data: { record: response, Practice: Practice, FilterDate: DateTime } });
                                    }

                                }
                                else {
                                    response = {
                                        AssignmentsLog: AssignmentsRecord,
                                        FinancialLog: FinancialRecord,
                                        AgingLog: AgingRecord,
                                    }
                                    $uibModalInstance.close({ msg: "found", data: { record: response, Practice: Practice, FilterDate: DateTime } });
                                }


                            }
                        });
                    });
                }
                else {
                    App.POST(FinancialFilter, $services + "GetEmployeesPPFinancialRecord").then(function (FinancialResponse) {
                        FinancialRecord = FinancialResponse.data;
                        if (AssignmentsRecord == "null" && FinancialRecord == "null") {
                            swal("Sorry no data found");
                        }
                        else if (AssignmentsRecord == "exception" && FinancialRecord == "exception" ) {
                            swal("Error", "Invalid Server Response", "error");
                        }
                        else {
                            response = {
                                AssignmentsLog: AssignmentsRecord,
                                FinancialLog: FinancialRecord,

                            }
                            $uibModalInstance.close({ msg: "found", data: { record: response, Practice: Practice, FilterDate: DateTime } });
                        }
                    });
                }

            });



        };
    }





    $scope.Cancel = function () {
        $uibModalInstance.close({ msg: "cancel" });
    }


});
App.controller("CredentialModelController", function ($scope, Details, $uibModalInstance, $uibModal, App) {
    $scope.ObjEmployee = Details.EmployeeDetails;
    $scope.Credentialing = Details.CredentialingData;
    $scope.EmployeePractices = Details.PracticeDetail;

    $scope.UpdateCredentialing = function (Credentialing) {
        if (this.C_form.$valid) {
            var AuthencationModel = $uibModal.open({
                templateUrl: 'AdminAuthencationModel',
                controller: function ($scope, $uibModalInstance) {
                    $scope.Authencate = function (username, password) {
                        App.POST({ UserName: username, Password: password }, $services + "AuthencateAdmin").then(function (response) {

                            if (response.data == "null") {
                                swal("Failed to verify, please contact your supervisor or admin");
                            }
                            else if (response.data == "exception") {
                                swal("Error ", "Invalid server response", "error");
                            }
                            else {
                                $uibModalInstance.close({ msg: "found", AdminDetails: response.data });
                            }
                        });
                    }
                    $scope.close = function () {
                        $uibModalInstance.dismiss('cancel');
                    }
                }

            });
            AuthencationModel.result.then(function (response) {
                if (response.msg == "found") {
                    Credentialing.UpdatedBy = response.AdminDetails.ManagerID;
                    Credentialing.CredentialingDateTime = new Date(parseInt(Credentialing.CredentialingDateTime.substr(6)));;
                    App.POST(Credentialing, $services + "SaveCredentials").then(function (response) {

                        if (response.data == "exception") {
                            swal("Error ", "Invalid server response", "error");
                        }
                        else if (response.data == "fails") {
                            swal("Error ", "An error occure while saving", "error");
                        }

                        else if (response.data == "updated") {


                            $uibModalInstance.close({ msg: "success" });


                        }
                    });;
                }
            });
        }
        else {
            swal("Error ", "Data is not in well managed form", "error");
        }
    }

});
App.controller("AssignmentModelController", function ($scope, Details, $uibModalInstance, $uibModal, App) {
   
    $scope.ObjEmployee = Details.EmployeeDetails;
    $scope.EmployeePractices = Details.PracticeDetail;
    console.log(Details.LogDetail.FinancialLog); 
    if (Details.LogDetail.AssignmentsLog == "null") {
        Details.LogDetail.AssignmentsLog = {
            AssignmentDateTime: Details.LogDetail.FilterDate,
            PracticeID: Details.PracticeDetail.PracticeID,
            EmployeeID: Details.EmployeeDetails.EmployeeID,
            DepartmentID: Details.EmployeeDetails.DepartmentID
        }
    }
    if (Details.LogDetail.FinancialLog == "null") {
        Details.LogDetail.FinancialLog = {
            FinancialDateTime: Details.LogDetail.FilterDate,
            PracticeID: Details.PracticeDetail.PracticeID,
            EmployeeID: Details.EmployeeDetails.EmployeeID,
            DepartmentID: Details.EmployeeDetails.DepartmentID
        }
    }
    if (Details.LogDetail.AgingLog == "null") {

        Details.LogDetail.AgingLog = {
            AgingDateTime: Details.LogDetail.FilterDate,
            PracticeID: Details.PracticeDetail.PracticeID,
            EmployeeID: Details.EmployeeDetails.EmployeeID,
            DepartmentID: Details.EmployeeDetails.DepartmentID
        }
    }
    $scope.Assignment = Details.LogDetail.AssignmentsLog;
    $scope.Financial = Details.LogDetail.FinancialLog;
    $scope.Aging = Details.LogDetail.AgingLog;

    $scope.UpdateAssignmentData = function (RCMGMAAssignmentData) {
        console.log(RCMGMAAssignmentData);
        debugger;
        if (this.a_form.$valid) {
            var AuthencationModel = $uibModal.open({
                templateUrl: 'AdminAuthencationModel',
                controller: function ($scope, $uibModalInstance) {
                    $scope.Authencate = function (username, password) {
                        App.POST({ UserName: username, Password: password }, $services + "AuthencateAdmin").then(function (response) {

                            if (response.data == "null") {
                                swal("Failed to verify, please contact your supervisor or admin");
                            }
                            else if (response.data == "exception") {
                                swal("Error ", "Invalid server response", "error");
                            }
                            else {
                                $uibModalInstance.close({ msg: "found", AdminDetails: response.data });
                            }
                        });
                    }
                    $scope.close = function () {
                        $uibModalInstance.dismiss('cancel');
                    }
                }

            });
            AuthencationModel.result.then(function (response) {
                if (response.msg == "found") {
                    RCMGMAAssignmentData.UpdatedBy = response.AdminDetails.ManagerID;
                    if (typeof RCMGMAAssignmentData.AssignmentID != "undefined") {
                        RCMGMAAssignmentData.AssignmentDateTime = new Date(parseInt(RCMGMAAssignmentData.AssignmentDateTime.substr(6)));;

                    }
                    App.POST(RCMGMAAssignmentData, $services + "SaveAssignmentData").then(function (response) {

                        if (response.data == "exception") {
                            swal("Error ", "Invalid server response", "error");
                        }
                        else if (response.data == "fails") {
                            swal("Error ", "An error occure while saving", "error");
                        }

                        else if (response.data == "updated") {


                            $uibModalInstance.close({ msg: "success" });


                        }
                        else if (response.data == "success") {


                            $uibModalInstance.close({ msg: "success" });


                        }
                    });;
                }
            });
        }
        else {
            swal("Error ", "Data is not in well managed form", "error");
        }
    }
    $scope.UpdateFinancials = function (RCMGMAFinancialsData) {
        if (this.f_form.$valid) {
            var AuthencationModel = $uibModal.open({
                templateUrl: 'AdminAuthencationModel',
                controller: function ($scope, $uibModalInstance) {
                    $scope.Authencate = function (username, password) {
                        App.POST({ UserName: username, Password: password }, $services + "AuthencateAdmin").then(function (response) {

                            if (response.data == "null") {
                                swal("Failed to verify, please contact your supervisor or admin");
                            }
                            else if (response.data == "exception") {
                                swal("Error ", "Invalid server response", "error");
                            }
                            else {
                                $uibModalInstance.close({ msg: "found", AdminDetails: response.data });
                            }
                        });
                    }
                    $scope.close = function () {
                        $uibModalInstance.dismiss('cancel');
                    }
                }

            });
            AuthencationModel.result.then(function (response) {
                if (response.msg == "found") {
                    RCMGMAFinancialsData.UpdatedBy = response.AdminDetails.ManagerID;
                    if (typeof RCMGMAFinancialsData.FinancialDateTime != "undefined") {
                        RCMGMAFinancialsData.FinancialDateTime = new Date(parseInt(RCMGMAFinancialsData.FinancialDateTime.substr(6)));;
                    }
                    App.POST(RCMGMAFinancialsData, $services + "SaveFinancialsData").then(function (response) {

                        if (response.data == "exception") {
                            swal("Error ", "Invalid server response", "error");
                        }
                        else if (response.data == "fails") {
                            swal("Error ", "An error occure while saving", "error");
                        }

                        else if (response.data == "updated") {

                            debugger;
                            $uibModalInstance.close({ msg: "success" });


                        }
                        else if (response.data == "success") {


                            $uibModalInstance.close({ msg: "success" });


                        }
                    });;
                }
            });
        }
        else {
            swal("Error ", "Data is not in well managed form", "error");
        }
    }
    $scope.UpdateAgings = function (AgingsData) {
        if (this.AG_form.$valid) {
            var AuthencationModel = $uibModal.open({
                templateUrl: 'AdminAuthencationModel',
                controller: function ($scope, $uibModalInstance) {
                    $scope.Authencate = function (username, password) {
                        App.POST({ UserName: username, Password: password }, $services + "AuthencateAdmin").then(function (response) {

                            if (response.data == "null") {
                                swal("Failed to verify, please contact your supervisor or admin");
                            }
                            else if (response.data == "exception") {
                                swal("Error ", "Invalid server response", "error");
                            }
                            else {
                                $uibModalInstance.close({ msg: "found", AdminDetails: response.data });
                            }
                        });
                    }
                    $scope.close = function () {
                        $uibModalInstance.dismiss('cancel');
                    }
                }

            });
            AuthencationModel.result.then(function (response) {
                if (response.msg == "found") {

                    if (typeof AgingsData.AgingID != "undefined") {
                        AgingsData.AgingDateTime = new Date(parseInt(AgingsData.AgingDateTime.substr(6)));;
                    }
                    AgingsData.UpdatedBy = response.AdminDetails.ManagerID;
                    App.POST(AgingsData, $services + "SaveAgings").then(function (response) {

                        if (response.data == "exception") {
                            swal("Error ", "Invalid server response", "error");
                        }
                        else if (response.data == "fails") {
                            swal("Error ", "An error occure while saving", "error");
                        }

                        else if (response.data == "updated") {


                            $uibModalInstance.close({ msg: "success" });


                        }
                        else if (response.data == "success") {


                            $uibModalInstance.close({ msg: "success" });


                        }
                    });;
                }
            });
        }
        else {
            swal("Error ", "Data is not in well managed form", "error");
        }
    }
    $scope.UpdatePPFinancials = function (FinancialsData) {

        if (this.f_form.$valid) {
            var AuthencationModel = $uibModal.open({
                templateUrl: 'AdminAuthencationModel',
                controller: function ($scope, $uibModalInstance) {
                    $scope.Authencate = function (username, password) {
                        App.POST({ UserName: username, Password: password }, $services + "AuthencateAdmin").then(function (response) {

                            if (response.data == "null") {
                                swal("Failed to verify, please contact your supervisor or admin");
                            }
                            else if (response.data == "exception") {
                                swal("Error ", "Invalid server response", "error");
                            }
                            else {
                                $uibModalInstance.close({ msg: "found", AdminDetails: response.data });
                            }
                        });
                    }
                    $scope.close = function () {
                        $uibModalInstance.dismiss('cancel');
                    }
                }

            });
            AuthencationModel.result.then(function (response) {
                if (response.msg == "found") {
                    console.log(FinancialsData); debugger;
                    FinancialsData.UpdatedBy = response.AdminDetails.ManagerID;
                    if (typeof FinancialsData.FinancialID != "undefined") {
                        FinancialsData.FinancialDateTime = new Date(parseInt(FinancialsData.FinancialDateTime.substr(6)));;
                    }
                    App.POST(FinancialsData, $services + "SavePPFinancialsData").then(function (response) {
                        if (response.data == "exception") {
                            swal("Error ", "Invalid server response", "error");
                        }
                        else if (response.data == "fails") {
                            swal("Error ", "An error occure while saving", "error");
                        }

                        else if (response.data == "updated") {

                           
                            $uibModalInstance.close({ msg: "success" });


                        }
                        else if (response.data == "success") {


                            $uibModalInstance.close({ msg: "success" });


                        }
                      

                    });
                }
            });
        }
    
        
    }
    $scope.Cancel = function () {
        $uibModalInstance.close({ msg: "cancel" });
    }
});
App.controller("EmailConfirmationController", function ($scope, $uibModalInstance, App, Details) {

    $scope.SendEmail = function () {

        if (Details.EmployeeDetails.DepartmentID == 1) {
            console.log(Details);
            Details.CredentialingData.CredentialingDateTime = new Date(parseInt(Details.CredentialingData.CredentialingDateTime.substr(6)));
            App.POST({ ObjCredentialing: Details.CredentialingData, ObjPractices: Details.PracticeDetail, ObjEmployee: Details.EmployeeDetails }, $services + "SendCredentialingEmail").then(function (response) {

                if (response.data == "exception") {
                    swal("error", "Failed to send email, please try again later", "error");
                }
                else if (response.data == "success") { $uibModalInstance.close({ msg: "success" }) }
            });;
        }
        else if (Details.EmployeeDetails.DepartmentID == 2) {

        }
        else if (Details.EmployeeDetails.DepartmentID == 3 || Details.EmployeeDetails.DepartmentID == 4 || Details.EmployeeDetails.DepartmentID == 5) {
            if (Details.AssignmentData == "null") {
                Details.AssignmentData = { AssignmentDateTime: Details.FilterDate, }
            }
            else {
                Details.AssignmentData.AssignmentDateTime = new Date(parseInt(Details.AssignmentData.AssignmentDateTime.substr(6)));
            }
            if (Details.FinancialData == "null") {
                Details.FinancialData = {}
            }
            else {
                Details.FinancialData.FinancialDateTime = new Date(parseInt(Details.FinancialData.FinancialDateTime.substr(6)));
            }
            if (Details.AgingData == "null") {

                Details.AgingData = {}
            }
            else {
                Details.AgingData.AgingDateTime = new Date(parseInt(Details.AgingData.AgingDateTime.substr(6)));
            }

            if (Details.EmployeeDetails.DepartmentID == 3) {
                App.POST({ ObjAssignment: Details.AssignmentData, ObjFinancial: Details.FinancialData, ObjAging: Details.AgingData, ObjPractices: Details.PracticeDetail, ObjEmployee: Details.EmployeeDetails }, $services + "SendRcmgMaEmail").then(function (response) {

                    if (response.data == "exception") {
                        swal("error", "Failed to send email, please try again later", "error");
                    }
                    else if (response.data == "success") { $uibModalInstance.close({ msg: "success" }) }
                });;

            }
            else if (Details.EmployeeDetails.DepartmentID == 4) {
                App.POST({ ObjAssignment: Details.AssignmentData, ObjFinancial: Details.FinancialData, ObjAging: Details.AgingData, ObjPractices: Details.PracticeDetail, ObjEmployee: Details.EmployeeDetails }, $services + "SendRcmgEmail").then(function (response) {

                    if (response.data == "exception") {
                        swal("error", "Failed to send email, please try again later", "error");
                    }
                    else if (response.data == "success") { $uibModalInstance.close({ msg: "success" }) }
                });;

            }
            else if (Details.EmployeeDetails.DepartmentID == 5) {
                App.POST({ ObjAssignment: Details.AssignmentData, ObjFinancial: Details.FinancialData, ObjAging: Details.AgingData, ObjPractices: Details.PracticeDetail, ObjEmployee: Details.EmployeeDetails }, $services + "SendDMEEmail").then(function (response) {

                    if (response.data == "exception") {
                        swal("error", "Failed to send email, please try again later", "error");
                    }
                    else if (response.data == "success") { $uibModalInstance.close({ msg: "success" }) }
                });;

            }
        }
        else if (Details.EmployeeDetails.DepartmentID == 6) {
            console.log(Details.AssignmentData);
            debugger;
            Details.AssignmentData.AssignmentDateTime = new Date(parseInt(Details.AssignmentData.AssignmentDateTime.substr(6)));
            App.POST({ ObjAssignment: Details.AssignmentData, ObjPractices: Details.PracticeDetail, ObjEmployee: Details.EmployeeDetails }, $services + "SendEligibilityEmail").then(function (response) {

                if (response.data == "exception") {
                    swal("error", "Failed to send email, please try again later", "error");
                }
                else if (response.data == "success") { $uibModalInstance.close({ msg: "success" }) }
            });;

        }
        else if (Details.EmployeeDetails.DepartmentID == 7) {
            console.log(Details);
            debugger;
            var count = Details.AssignmentData.length;
            if (Details.FinancialData.data.length > count) {
                count = Details.FinancialData.data.length;
            }
            console.log(count);
            AssgData = {}; FinData = {}; vm = [];
            for (i = 0; i < count; i++) {
                if (typeof Details.AssignmentData[i] == "undefined") {
                    Details.AssignmentData[i] = {}
                }
                if (typeof Details.FinancialData.data[i] == "undefined") {
                    Details.FinancialData.data[i] = {}
                }
                else {
                    vm[i] = {
                        PracticeID: (typeof Details.AssignmentData[i].PracticeID == "undefined" ? Details.FinancialData.data[i].PracticeID : Details.AssignmentData[i].PracticeID),
                        EnteredClaims: Details.AssignmentData[i].EnteredClaims,
                        PendingClaims: Details.AssignmentData[i].PendingClaims,
                        PaidClaims: Details.AssignmentData[i].PaidClaims,
                        PreparedClaims: Details.AssignmentData[i].PreparedClaims,
                        PreparedEncounters: Details.AssignmentData[i].PreparedEncounters,
                        MissingLog: Details.AssignmentData[i].MissingLog,
                        DateOfService: Details.FilterDate,
                        WorkDate: (typeof Details.AssignmentData[i].CreatedDate == "undefined" ? new Date(parseInt(Details.FinancialData.data[i].CreatedDate.substr(6))) : new Date(parseInt(Details.AssignmentData[i].CreatedDate.substr(6)))),
                        QA: Details.AssignmentData[i].QA,
                        Email: Details.AssignmentData[i].Email,
                        EnteredChargeI: Details.FinancialData.data[i].EnteredChargeI,
                        EnteredChargeP: Details.FinancialData.data[i].EnteredChargeP,

                        Eligibility: Details.AssignmentData[i].Eligibility,
                        BillingAnalysis: Details.AssignmentData[i].BillingAnalysis,
                        ReceivedBills: Details.AssignmentData[i].ReceivedBills,
                    }; debugger;
                }
            }
            console.log(vm);
            debugger;
            //  Details.AssignmentData.AssignmentDateTime = new Date(parseInt(Details.AssignmentData.AssignmentDateTime.substr(6)));
            App.POST({ VMBillingEmail: vm, ObjEmployee: Details.EmployeeDetails, ObjPractice: Details.PracticeDetail }, $services + "SendBillingEmail").then(function (response) {

                if (response.data == "exception") {
                    swal("error", "Failed to send email, please try again later", "error");
                }
                else if (response.data == "success") { $uibModalInstance.close({ msg: "success" }) }
            });;

        }
        else if (Details.EmployeeDetails.DepartmentID == 8) {
            console.log(Details.AssignmentData);
            debugger;
            Details.AssignmentData.AssignmentDateTime = new Date(parseInt(Details.AssignmentData.AssignmentDateTime.substr(6)));
            App.POST({ ObjAssignment: Details.AssignmentData, ObjPractices: Details.PracticeDetail, ObjEmployee: Details.EmployeeDetails }, $services + "SendPSMEmail").then(function (response) {

                if (response.data == "exception") {
                    swal("error", "Failed to send email, please try again later", "error");
                }
                else if (response.data == "success") { $uibModalInstance.close({ msg: "success" }) }
            });;

        }
        else if (Details.EmployeeDetails.DepartmentID == 9) {
            console.log(Details);
            debugger;
            AssignmentData = {}; FinalcialData = {};
            if (Details.AssignmentData=="null") {
                Details.AssignmentData = {
                    AssignmentDateTime: Details.FilterDate
                }
            }
            else {
                Details.AssignmentData.AssignmentDateTime = new Date(parseInt(Details.AssignmentData.AssignmentDateTime.substr(6)));
            }
            if (Details.FinancialData == "null") {
                Details.FinancialData = {
                    FinancialDateTime: Details.FilterDate
                }
            }
            else {
                Details.FinancialData.FinancialDateTime = new Date(parseInt(Details.FinancialData.FinancialDateTime.substr(6)));
            }
            debugger;
            App.POST({ ObjAssignment: Details.AssignmentData, ObjFinincial: Details.FinancialData, ObjPractices: Details.PracticeDetail, ObjEmployee: Details.EmployeeDetails }, $services + "SendPaymentPostingEmail").then(function (response) {

                if (response.data == "exception") {
                    swal("error", "Failed to send email, please try again later", "error");
                }
                else if (response.data == "success") { $uibModalInstance.close({ msg: "success" }) }
            });;
        }
    }
    $scope.Cancel = function () {
        $uibModalInstance.close({ msg: "cancel" });
    }
});
