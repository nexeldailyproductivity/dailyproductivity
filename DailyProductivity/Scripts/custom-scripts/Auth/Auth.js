﻿var App = angular.module("PMS", []);

App.controller("Login", function ($scope, $http, myService) {
    $scope.Validate = function (User) {




        var getData = myService.UserLogin(User);
        getData.then(function (response) {
            console.log(response.data);
            $("#loader").hide();
            if (response.data.msg == "exception") {
                swal({
                    title: "Error !!",
                    text: "Invalid Server Response.Please Try Again Later",
                    icon: "error",
                });
            }
            else if (response.data.msg == "notfound") {
                swal({
                    title: "Error !!",
                    text: "Invalid username or password!",
                    icon: "error",
                });
            }
            else if (response.data.msg == "inactive") {
                swal({
                    title: "Error !!",
                    text: "your account is deactivated plesae contact to administrator!",
                    icon: "error",
                });
            }
            else if (response.data.msg == "found") {
                if (response.data.returnUri != "") {
                    window.location.href = response.data.returnUri;
                }
                else if (response.data.RoleID == 1) {
                    window.location.href = $rootpath + "admin/home/dashboard";
                }
                else if (response.data.RoleID == 2) {
                    window.location.href = $rootpath + "home/dashboard";
                }
            }
        });
        //  debugger;


    }
    function clearFields() {
        $scope.UserName = '';
        $scope.Password = '';
    }
    $http.get($rootpath + '/auth/getrememberme').then(function (result) {
        $scope.User = {
            UserName: result.data.uname,
            RememberMe: result.data.rem
        }

    });
});
App.service("myService", function ($http) {
    this.UserLogin = function (User) {
        var response = $http({
            method: "post",
            url: $rootpath + "/auth/Loginvalidate",
            data: JSON.stringify(User),
            dataType: "json"
        });
        return response;
    }



});