﻿
using DailyProductivity.App_Start;
using DailyProductivity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DailyProductivity.Controllers
{
    [SessionExpire]
    public class ProductivityController : Controller
    {
        private DBContextDataBase db = new DBContextDataBase();
        // GET: Productivity
        public ActionResult Assignments()
        {
            //long EmployeeID = Convert.ToInt64(Session["EmployeeID"]);
            //var PHD = db.PhdAssignments.ToList();
            //var prcts = (from ep in db.EmployeePractices join p in db.Practices on ep.PracticesID equals p.PracticeID where ep.EmployeeID==EmployeeID select new { p.PracticeID,p.PracticeName,p.CategoryID,p.PracticeShortName}).ToList();
            //ViewBag.PHDAssignment = PHD;
            //ViewBag.PHDPractices = prcts;
            var PHD = db.PhdAssignments.ToList();
            var prcts = db.Practices.ToList();
            ViewBag.PHDAssignment = PHD;
            ViewBag.PHDPractices = prcts;
            return View();
        }
        public ActionResult Financials()
        {
            return View();
        }

        public ActionResult RenderPopup()
        {
            //long EmployeeID = Convert.ToInt64(Session["EmployeeID"]);
            //var PHD = db.PhdAssignments.ToList();
            //var prcts = (from ep in db.EmployeePractices join p in db.Practices on ep.PracticesID equals p.PracticeID where ep.EmployeeID == EmployeeID select new { p.PracticeID, p.PracticeName }).ToList();
            //ViewBag.PHDAssignment = PHD;
            //ViewBag.PHDPractices = prcts; var PHD = db.PhdAssignments.ToList();
            var PHD = db.PhdAssignments.ToList();
            var prcts = db.Practices.ToList();
            ViewBag.PHDAssignment = PHD;
            ViewBag.PHDPractices = prcts;

            return PartialView("~/Views/Productivity/Forms/PatientHelpDesk.cshtml");
        }
    }
    public class AssgnArary
    {
        public string AsssigmentName { get; set; }
        public string AssisgnmentShortName { get; set; }
        public string PracticeName { get; set; }





    }
}