﻿using DailyProductivity.Models;
using DailyProductivity.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.UI;

namespace DailyProductivity.Controllers
{
    public class ServicesController : Controller
    {
        DBContextDataBase db = new DBContextDataBase();
        // GET: Services
        public JsonResult GetEmployeesDetail(Employees Employee)
        {
            try
            {
                // var query = db.Employees    
                //.Join(db.Department, 
                //   usr => usr.DepartmentID,        
                //   dpt => dpt.DepartmentID,   
                //   (usr, dpt) => new { usr = usr, Meta = dpt }) 
                //.Where(usrAnddpt => usrAnddpt.usr.EmployeeID == Employees.EmployeeID).FirstOrDefault();
                var ObjEmployees = db.Employees.Where(u => u.EmployeeID == Employee.EmployeeID).FirstOrDefault();
                if (ObjEmployees != null)
                {
                    return Json(ObjEmployees, JsonRequestBehavior.AllowGet);
                }
                return Json("null", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetEmployeesPracticesList(Employees Employees)
        {
            try
            {

                var Practices = (
                                   from up in db.EmployeePractices
                                   join p in db.Practices on up.PracticesID equals p.PracticeID
                                   where up.EmployeeID == Employees.EmployeeID
                                   select new
                                   {
                                       p.PracticeID,
                                       p.PracticeName,
                                       p.CategoryID,
                                       p.PracticeShortName
                                   }
                                 ).ToList();
                if (Practices.Count > 0)
                {
                    return Json(Practices, JsonRequestBehavior.AllowGet);
                }
                return Json("null", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult SaveCredentials(Credentialing ObJCredentialing)
        {
            try
            {
                if (ObJCredentialing != null || ObJCredentialing.EmployeeID <= 0)
                {
                    if (ObJCredentialing.CredentialingID == 0)
                    {
                        var CredentialingLog = db.Credentialing.Where(c => c.CredentialingDateTime == ObJCredentialing.CredentialingDateTime && c.EmployeeID == ObJCredentialing.EmployeeID && c.PracticeID == ObJCredentialing.PracticeID).FirstOrDefault();
                        if (CredentialingLog == null)
                        {
                            db.Credentialing.Add(ObJCredentialing);
                            if (db.SaveChanges() == 1)
                            {
                                return Json("success", JsonRequestBehavior.AllowGet);
                            }
                            return Json("fail", JsonRequestBehavior.AllowGet);
                        }
                        return Json("exists", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        db.Entry(ObJCredentialing).State = EntityState.Modified;
                        if (db.SaveChanges() == 1)
                        {
                            return Json("updated", JsonRequestBehavior.AllowGet);
                        }
                        return Json("fail", JsonRequestBehavior.AllowGet);
                    }
                }
                return Json("null", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult SavePatientHelpDeskPHD(List<PatientHelpDesk> ObJPhDList)
        {
            try
            {
                if (ObJPhDList != null)
                {
                    int SuccessCount = 0;
                    foreach (var ObJPhD in ObJPhDList)
                    {
                        if (ObJPhD.PHDID == 0)
                        {
                            var PatientHelpDeskLog = db.PatientHelpDesk.Where(c => c.PatientHelpDeskDateTime == ObJPhD.PatientHelpDeskDateTime && c.EmployeeID == ObJPhD.EmployeeID && c.PracticeID == ObJPhD.PracticeID).FirstOrDefault();
                            if (PatientHelpDeskLog == null)
                            {
                                db.PatientHelpDesk.Add(ObJPhD);
                                if (db.SaveChanges() == 1)
                                {
                                    SuccessCount++;
                                }
                                else
                                {
                                    return Json("fail", JsonRequestBehavior.AllowGet);
                                }

                            }
                            else
                            {
                                return Json("exists", JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            db.Entry(ObJPhD).State = EntityState.Modified;
                            if (db.SaveChanges() == 1)
                            {
                                SuccessCount++;
                            }
                            else
                            {
                                return Json("fail", JsonRequestBehavior.AllowGet);
                            }

                        }
                    }
                    if (SuccessCount == ObJPhDList.Count)
                    {
                        return Json("success", JsonRequestBehavior.AllowGet);
                    }
                }
                return Json("null", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult DetelePHDRecordByDate(PatientHelpDesk ObJPhD)
        {
            try
            {
                if (ObJPhD != null)
                {
                    var PatientHelpDeskLog = db.PatientHelpDesk.Where(c => c.PatientHelpDeskDateTime == ObJPhD.PatientHelpDeskDateTime && c.EmployeeID == ObJPhD.EmployeeID).FirstOrDefault();
                    if (PatientHelpDeskLog != null)
                    {
                        db.PatientHelpDesk.Remove(PatientHelpDeskLog);
                        if (db.SaveChanges() == 1)
                        {
                            return Json("deleted", JsonRequestBehavior.AllowGet);
                        }
                        return Json("error", JsonRequestBehavior.AllowGet);
                    }
                    return Json("notfound", JsonRequestBehavior.AllowGet);
                }
                return Json("null", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult SaveAssignmentData(Assignments Assignments)
        {
            try
            {
                if (Assignments != null && Assignments.EmployeeID > 0)
                {
                    if (Assignments.AssignmentID == 0)
                    {
                        var AssignmentsLog = db.Assignments.Where(c => c.AssignmentDateTime == Assignments.AssignmentDateTime && c.EmployeeID == Assignments.EmployeeID && c.DepartmentID == Assignments.DepartmentID && c.PracticeID == Assignments.PracticeID).FirstOrDefault();
                        if (AssignmentsLog == null)
                        {
                            db.Assignments.Add(Assignments);
                            if (db.SaveChanges() == 1)
                            {
                                return Json("success", JsonRequestBehavior.AllowGet);
                            }
                            return Json("fail", JsonRequestBehavior.AllowGet);
                        }
                        return Json("exists", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        db.Entry(Assignments).State = EntityState.Modified;
                        if (db.SaveChanges() == 1)
                        {
                            return Json("updated", JsonRequestBehavior.AllowGet);
                        }
                        return Json("fail", JsonRequestBehavior.AllowGet);

                    }
                }
                return Json("null", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult SaveAgings(Agings Agings)
        {
            try
            {
                if (Agings != null || Agings.EmployeeID <= 0)
                {
                    if (Agings.AgingID == 0)
                    {
                        var AgingsLog = db.Aging.Where(c => c.AgingDateTime == Agings.AgingDateTime && c.EmployeeID == Agings.EmployeeID && c.PracticeID == Agings.PracticeID && c.DepartmentID == Agings.DepartmentID).FirstOrDefault();
                        if (AgingsLog == null)
                        {
                            db.Aging.Add(Agings);
                            if (db.SaveChanges() == 1)
                            {
                                return Json("success", JsonRequestBehavior.AllowGet);
                            }
                            return Json("fail", JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json("exists", JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        db.Entry(Agings).State = EntityState.Modified;
                        if (db.SaveChanges() == 1)
                        {
                            return Json("updated", JsonRequestBehavior.AllowGet);
                        }
                        return Json("fail", JsonRequestBehavior.AllowGet);
                    }

                }
                return Json("null", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SaveFinancialsData(Financial Financial)
        {
            try
            {
                if (Financial != null || Financial.EmployeeID <= 0)
                {
                    if (Financial.FinancialID == 0)
                    {
                        var FinancialLog = db.Financials.Where(c => c.FinancialDateTime == Financial.FinancialDateTime && c.EmployeeID == Financial.EmployeeID && c.PracticeID == Financial.PracticeID && c.DepartmentID == Financial.DepartmentID).FirstOrDefault();
                        if (FinancialLog == null)
                        {
                            db.Financials.Add(Financial);
                            if (db.SaveChanges() == 1)
                            {
                                return Json("success", JsonRequestBehavior.AllowGet);
                            }
                            return Json("fail", JsonRequestBehavior.AllowGet);
                        }
                        return Json("exists", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        db.Entry(Financial).State = EntityState.Modified;
                        if (db.SaveChanges() == 1)
                        {
                            return Json("updated", JsonRequestBehavior.AllowGet);
                        }
                        return Json("fail", JsonRequestBehavior.AllowGet);
                    }
                }
                return Json("null", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult SavePPFinancialsData(PPFinancials Financial)
        {
            try
            {
                if (Financial != null || Financial.EmployeeID <= 0)
                {
                    if (Financial.FinancialID == 0)
                    {
                        var FinancialLog = db.PPFinancials.Where(c => c.FinancialDateTime == Financial.FinancialDateTime && c.EmployeeID == Financial.EmployeeID && c.PracticeID == Financial.PracticeID && c.DepartmentID == Financial.DepartmentID).FirstOrDefault();
                        if (FinancialLog == null)
                        {
                            db.PPFinancials.Add(Financial);
                            if (db.SaveChanges() == 1)
                            {
                                return Json("success", JsonRequestBehavior.AllowGet);
                            }
                            return Json("fail", JsonRequestBehavior.AllowGet);
                        }
                        return Json("exists", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        db.Entry(Financial).State = EntityState.Modified;
                        if (db.SaveChanges() == 1)
                        {
                            return Json("updated", JsonRequestBehavior.AllowGet);
                        }
                        return Json("fail", JsonRequestBehavior.AllowGet);
                    }
                }
                return Json("null", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }

        /// Notifications
        public JsonResult SaveNotifications(Notificatoins Notificatoins)
        {
            try
            {
                if (Notificatoins != null || Notificatoins.EmployeeID <= 0)
                {

                    db.Notificatoins.Add(Notificatoins);
                    if (db.SaveChanges() == 1)
                    {
                        return Json("success", JsonRequestBehavior.AllowGet);
                    }
                    return Json("fail", JsonRequestBehavior.AllowGet);

                }
                return Json("null", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult lastthreemonthreport()
        {
            try
            {
                long EmployeeID = Convert.ToInt64(Session["EmployeeID"]);
                string Years = "";
                int Months = 0;
                DateTime CurrentDateTime = DateTime.Now;
                DateTime ThirdLastMonth = DateTime.Now.AddMonths(-3);

                if (ThirdLastMonth.ToString("yyyy") != CurrentDateTime.ToString("yyyy"))
                {
                    Years = ThirdLastMonth.ToString("yyyy") + "," + CurrentDateTime.ToString("yyyy");
                }
                else
                {
                    Years = CurrentDateTime.ToString("yyyy");
                }
                // Months = ThirdLastMonth.Month;

                var EmployeesPractices = (from up in db.EmployeePractices
                                          join p in db.Practices
                                          on up.PracticesID equals p.PracticeID
                                          where up.EmployeeID == EmployeeID
                                          select new
                                          {
                                              p.PracticeID,
                                              p.PracticeName
                                          }
                                ).ToList(); List<Object> report = new List<Object>();
                foreach (var P in EmployeesPractices)
                {

                    string query = @"SELECT 
                                    SUM(f.DailyCharges) DailyCharges,
									SUM(f.DailyPayments) DailyPayments,
									SUM(f.AgingAmount) AgingAmount,
                                    p.PracticeID,
                                    p.PracticeName,
                                    DATENAME(month,FinancialDateTime)    Month
                                FROM
                                    EmployeesPractices up,
                                    Financial f,
                                    Practices p
                                WHERE
                                    p.PracticeID = up.PracticesID
                                        AND up.PracticesID = f.PracticeID
                                        AND up.EmployeeID = '" + EmployeeID + @"'
                                        AND MONTH(FinancialDateTime) between '" + ThirdLastMonth.Month + @"' 
                                        AND '" + (CurrentDateTime.Month - 1) + @"'
                                        AND YEAR(FinancialDateTime) in (" + Years + @") 
                                        AND up.PracticesID='" + P.PracticeID + @"' 
                                GROUP BY p.PracticeID , p.PracticeName , MONTH(FinancialDateTime) ,
                                YEAR(FinancialDateTime) ,DATENAME(month,FinancialDateTime) ";
                    var result = db.Database.SqlQuery<VMUserDashboardChargesSummary>(query).ToArray();
                    report.Add(result);

                }
                //       string query1 = @"SELECT 
                //                           SUM(f.DailyCharges) DailyCharges,
                //SUM(f.DailyPayments) DailyPayments,
                //SUM(f.AgingAmount) AgingAmount,
                //                           p.PracticeID,
                //                           p.PracticeName,
                //                           DATENAME(month,FinancialDateTime)    Month
                //                       FROM
                //                           EmployeesPractices up,
                //                           Financial f,
                //                           Practices p
                //                       WHERE
                //                           p.PracticeID = up.PracticesID
                //                               AND up.PracticesID = f.PracticeID
                //                               AND up.EmployeeID = '" + EmployeeID + @"'
                //                               AND MONTH(FinancialDateTime) between '" + ThirdLastMonth.Month + @"' 
                //                               AND '" + (CurrentDateTime.Month - 1) + @"'
                //                               AND YEAR(FinancialDateTime) in (" + Years + @") 

                //                       GROUP BY p.PracticeID , p.PracticeName , MONTH(FinancialDateTime) ,
                //                       YEAR(FinancialDateTime) ,DATENAME(month,FinancialDateTime)";

                //   var result= db.Database.SqlQuery<VMEmployeesDashboardChargesSummary>(query).ToArray();


                return Json(report, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getassignmentcount()
        {
            try
            {
                long EmployeeID = Convert.ToInt64(Session["EmployeeID"]);
                string query = @"SELECT 
                    ISNULL(  DATENAME(month,AssignmentDateTime),0)    Month,
                    CAST(  COUNT(EnteredClaims) AS BIGINT) EnteredClaims,
                    CAST(  COUNT(Eligibility) AS BIGINT)Eligibility,
                    CAST(  COUNT(PF_eBridge) AS BIGINT),
                    CAST(   COUNT(ERADenials) AS BIGINT),
                    CAST(   COUNT(ERAUnmatched )AS BIGINT) ERAUnmatched,
                    CAST( COUNT (Appeals) AS BIGINT) Appeals,
                    CAST( COUNT(Calls) AS BIGINT),
                    CAST( COUNT(AgingClaims) AS BIGINT) AgingClaims,
                    CAST( COUNT(Email) AS BIGINT) Email
            FROM
                Assignments
            WHERE
                EmployeeID = '" + EmployeeID + "' 	group by DATENAME(month,AssignmentDateTime)";
                var result = db.Database.SqlQuery<AssignmentsCounts>(query).ToArray();
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Yearly Reporting
        /// </summary>
        /// <param name="EmployeeID"></param>
        /// <param name="FilterDT"></param>
        /// <returns></returns>
        public JsonResult GetCurrentYearPracticeSummary(Employees ObjEmployees, Practices Practices)
        {
            try
            {
                DateTime CurrentDateTime = DateTime.Now;
                string condtion = "", sum = "";
                if (Practices.PracticeID <= 0)
                {
                    condtion = "AND f.PracticeID=" + db.EmployeePractices.Where(p => p.EmployeeID == ObjEmployees.EmployeeID).FirstOrDefault().PracticesID.ToString();
                }
                else
                {
                    condtion = "AND f.PracticeID=" + Practices.PracticeID;
                }
                if (ObjEmployees.DepartmentID == 3)
                {
                    sum = @"               
                                               sum( DailyChargesECW) DailyCharges
                                              ,sum( DailyPaymentsECW) + sum( DailyPaymentseTeClinic) DailyPayments
                                              ,sum( AgingAmounteTeClinic) + sum( AgingAmounteCW) AgingAmount,";
                }
                else
                {
                    sum = @"                SUM(f.DailyCharges) DailyCharges,
                                            SUM(f.AgingAmount)AgingAmount,
                                            SUM(f.DailyPayments) DailyPayments,";
                }
                string query = @" SELECT 
                                           p.PracticeName,
                                           " + sum + @"
                                            DATENAME(month, FinancialDateTime) Month,
                                            DATEPART(month, FinancialDateTime) MonthValue
                                        FROM
                                            Financial f
	                                        Join Practices p on p.PracticeID=f.PracticeID
                                        WHERE
                                            f.EmployeeID = " + ObjEmployees.EmployeeID + @"
                                            And f.DepartmentID=" + ObjEmployees.DepartmentID + @"
                                                 AND YEAR(FinancialDateTime) = " + CurrentDateTime.Year + condtion + @"
                                        GROUP BY f.PracticeID , MONTH(FinancialDateTime) , YEAR(FinancialDateTime) , DATENAME(month, FinancialDateTime) , p.PracticeName
                                        ORDER BY DATEPART(month, FinancialDateTime) ASC";
                var result = db.Database.SqlQuery<VMUserDashboardChargesSummary>(query).ToList();
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetCurrentYearPracticePayments(Employees ObjEmployees, Practices Practices)
        {

            try
            {
                DateTime CurrentDateTime = DateTime.Now;
                string condtion = "";
                if (Practices == null)
                {
                    condtion = "AND f.PracticeID=" + db.EmployeePractices.Where(p => p.EmployeeID == ObjEmployees.EmployeeID).FirstOrDefault().PracticesID.ToString();
                }
                string query = @" SELECT 
                                           p.PracticeName,
                                           SUM(f.DailyPayments) DailyPayments,
                                            DATENAME(month, FinancialDateTime) Month,
                                            DATEPART(month, FinancialDateTime) MonthValue
                                        FROM
                                            Financial f
	                                        Join Practices p on p.PracticeID=f.PracticeID
                                        WHERE
                                            f.EmployeeID = " + ObjEmployees.EmployeeID + @"
                                                AND YEAR(FinancialDateTime) = " + CurrentDateTime.Year + condtion + @"
                                        GROUP BY f.PracticeID , MONTH(FinancialDateTime) , YEAR(FinancialDateTime) , DATENAME(month, FinancialDateTime) , p.PracticeName
                                        ORDER BY DATEPART(month, FinancialDateTime) ASC";
                var result = db.Database.SqlQuery<VMUserDashboardChargesSummary>(query).ToList();
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetCurrentYearPracticeAging(Employees ObjEmployees, Practices Practices)
        {
            try
            {
                DateTime CurrentDateTime = DateTime.Now;
                string condtion = "";
                if (Practices == null)
                {
                    condtion = "AND f.PracticeID=" + db.EmployeePractices.Where(p => p.EmployeeID == ObjEmployees.EmployeeID).FirstOrDefault().PracticesID.ToString();
                }
                string query = @" SELECT 
                                           p.PracticeName,
                                           SUM(f.AgingAmount) AgingAmount,
                                            DATENAME(month, FinancialDateTime) Month,
                                            DATEPART(month, FinancialDateTime) MonthValue
                                        FROM
                                            Financial f
	                                        Join Practices p on p.PracticeID=f.PracticeID
                                        WHERE
                                            f.EmployeeID = " + ObjEmployees.EmployeeID + @"
                                                AND YEAR(FinancialDateTime) = " + CurrentDateTime.Year + condtion + @"
                                        GROUP BY f.PracticeID , MONTH(FinancialDateTime) , YEAR(FinancialDateTime) , DATENAME(month, FinancialDateTime) , p.PracticeName
                                        ORDER BY DATEPART(month, FinancialDateTime) ASC";
                var result = db.Database.SqlQuery<VMUserDashboardChargesSummary>(query).ToList();
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetCurrentYearAssignCount(Employees ObjEmployees, Practices Practices)
        {
            try
            {
                DateTime CurrentDateTime = DateTime.Now;
                string query = "";
                string condtion = "";
                if (Practices.PracticeID <= 0)
                {
                    condtion = "AND A.PracticeID=" + db.EmployeePractices.Where(p => p.EmployeeID == ObjEmployees.EmployeeID).FirstOrDefault().PracticesID.ToString();
                }
                else
                {
                    condtion = "AND A.PracticeID=" + Practices.PracticeID;
                }
                if (ObjEmployees.DepartmentID == 6)
                {
                    query = @"SELECT 
                                    SUM(A.EmailVerification) EmailVerification,
                                    SUM(A.FaxedVerification) FaxedVerification,
                                    SUM(A.WebVerification) WebVerification,
                                    SUM(A.Others) Others,
                                    SUM(A.Calls) Calls,
                                    P.PracticeID,
                                    P.PracticeName
                                FROM
                                    Assignments A
                                        JOIN
                                    Practices p ON p.PracticeID = A.PracticeID
                                WHERE
                                    A.EmployeeID = " + ObjEmployees.EmployeeID + @"
                                    And A.DepartmentID=" + ObjEmployees.DepartmentID + @"
                                        AND YEAR(AssignmentDateTime) = " + CurrentDateTime.Year + " " + condtion + @"
                                GROUP BY P.PracticeID , P.PracticeName";
                }
                else if (ObjEmployees.DepartmentID == 1)
                {
                    query = @"SELECT 
                                    SUM(A.CredentialingCalls) CredentialingCalls,
                                    SUM(A.CredentialingDocuments) CredentialingDocuments,
                                    SUM(A.EDICalls) EDICalls,
                                    SUM(A.EDISetup) EDISetup,
                                    SUM(A.ERACalls) ERACalls,
									SUM(A.ERASetup) ERASetup,
									SUM(A.Emails) Emails,
                                    SUM(A.EnrollmentTrackingSheet) EnrollmentTrackingSheet,
									SUM(A.WebLogins) WebLogins,
								    SUM(A.EFTCalls) EFTCalls,
                                    SUM(A.EFTSetup) EFTSetup,
                                    P.PracticeID,
                                    P.PracticeName
                                FROM
                                    Credentialing A
                                        JOIN
                                    Practices p ON p.PracticeID = A.PracticeID
                                WHERE
                                    A.EmployeeID = " + ObjEmployees.EmployeeID + @"
                                   " + condtion + @"
                                        AND YEAR(CredentialingDateTime) = " + CurrentDateTime.Year + @"
                                GROUP BY P.PracticeID , P.PracticeName";
                }
                else if (ObjEmployees.DepartmentID == 2)
                {
                    query = @"SELECT 
                                   SUM(A.AVM) AVM,
                                    SUM(A.Emails) Emails,
                                    SUM(A.IC) IC,
                                    SUM(A.ICIC) ICIC,
                                    SUM(A.ICPC) ICPC,
									SUM(A.[IS]) [IS],
									SUM(A.LVM) LVM,
                                    SUM(A.OGIC) OGIC,
									SUM(A.OGPC) OGPC,
								    SUM(A.PC) PC,
                                    SUM(A.PS) PS,
									SUM(A.Pendings) Pendings,
                                    P.PracticeID,
                                    P.PracticeName
                                FROM
                                    PatientHelpDesk A
                                        JOIN
                                    Practices p ON p.PracticeID = A.PracticeID
                                WHERE
                                    A.EmployeeID = " + ObjEmployees.EmployeeID + @"
                                   " + condtion + @"
                                        AND YEAR(PatientHelpDeskDateTime) = " + CurrentDateTime.Year + @"
                                GROUP BY P.PracticeID , P.PracticeName";
                }
                else if (ObjEmployees.DepartmentID == 7)
                {
                    query = @"SELECT 
                                    SUM(A.Eligibility) Eligibility,
                                    SUM(A.ReceivedBills) ReceivedBills,
                                    SUM(A.BillingAnalysis) BillingAnalysis,

                                    SUM(A.EnteredClaims) EnteredClaims,
                                    SUM(A.PendingClaims) PendingClaims,
                                    SUM(A.PaidClaims) PaidClaims,
                                    SUM(A.PreparedClaims) PreparedClaims,
                                    SUM(A.PreparedEncounters) PreparedEncounters,
									SUM(A.MissingLog) MissingLog,
									SUM(A.Emails) Emails,
                                    SUM(A.Qa) Qa,
                                    P.PracticeID,
                                    P.PracticeName
                                FROM
                                    Assignments A
                                        JOIN
                                    Practices p ON p.PracticeID = A.PracticeID
                                WHERE
                                    A.EmployeeID = " + ObjEmployees.EmployeeID + @"
                                   " + condtion + @"
                                        AND YEAR(AssignmentDateTime) = " + CurrentDateTime.Year + @"
                                GROUP BY P.PracticeID , P.PracticeName";
                }
                else if (ObjEmployees.DepartmentID == 9)
                {
                    query = @"SELECT 
                                     SUM(A.PF_eBridge) PF_eBridge,
                                    SUM(A.Rejections) Rejections,
                                    SUM(A.ERADenials) ERADenials,
                                    SUM(A.ERAUnmatched) ERAUnmatched,
                                    SUM(A.Appeals) Appeals,
									SUM(A.ManuallyAdjustments) ManuallyAdjustments,
									SUM(A.AgingClaims) AgingClaims,
                                    SUM(A.BillingETC) BillingETC,
									SUM(A.Emails) Emails,
									SUM(A.PTStatements) PTStatements,
									SUM(A.VerificationClaims) VerificationClaims,
                                    P.PracticeID,
                                    P.PracticeName
                                FROM
                                    Assignments A
                                        JOIN
                                    Practices p ON p.PracticeID = A.PracticeID
                                WHERE
                                    A.EmployeeID = " + ObjEmployees.EmployeeID + @"
                                   " + condtion + @"
                                        AND YEAR(AssignmentDateTime) = " + CurrentDateTime.Year + @"
                                GROUP BY P.PracticeID , P.PracticeName";
                }
                else if (ObjEmployees.DepartmentID == 8)
                {
                    query = @"SELECT 
                                    SUM(A.Multiple) Multiple,
                                    SUM(A.NoPatientBill) NoPatientBill,
                                    SUM(A.UVTotalClaims) UVTotalClaims,
                                    SUM(A.Done) Done,
                                    SUM(A.PendingClaims) PendingClaims,
                                    P.PracticeID,
                                    P.PracticeName
                                FROM
                                    Assignments A
                                        JOIN
                                    Practices p ON p.PracticeID = A.PracticeID
                                WHERE
                                    A.EmployeeID = " + ObjEmployees.EmployeeID + @"
                                   " + condtion + @"
                                        AND YEAR(AssignmentDateTime) = " + CurrentDateTime.Year + @"
                                GROUP BY P.PracticeID , P.PracticeName";
                }
                var Result = db.Database.SqlQuery<AssignmentsCounts>(query).FirstOrDefault();

                if (Result == null)
                {
                    return Json(new { data = "null", Practice = Practices.PracticeName }, JsonRequestBehavior.AllowGet);
                }
                return Json(Result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }

        }
        public JsonResult GetCurrentYearCredentialingAssignCount(Employees ObjEmployees)
        {
            try
            {
                DateTime CurrentDateTime = DateTime.Now;
                string query = @"SELECT 
                                SUM(CredentialingCalls) CredentialingCalls,
                                SUM(CredentialingDocuments) CredentialingDocuments,
                                SUM(EDICalls) EDICalls,
                                SUM(EDISetup) EDISetup,
                                SUM(EFTCalls) EFTCalls,
                                SUM(EFTSetup) EFTSetup,
                                SUM(ERACalls) ERACalls,
                                SUM(ERASetup) ERASetup,
                                SUM(Emails) Emails,
                                SUM(EnrollmentTrackingSheet) EnrollmentTrackingSheet,
                                SUM(WebLogins) WebLogins
                            FROM
                                Credentialing C
                            WHERE
                                C.EmployeeID = '" + ObjEmployees.EmployeeID + @"'
                        AND YEAR(CredentialingDateTime) = '" + CurrentDateTime.Year + "'";
                var Result = db.Database.SqlQuery<VMCredentialingCount>(query).ToList();

                return Json(Result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }

        }
        public JsonResult GetCurrentYearPHDAssignCount(Employees ObjEmployees)
        {
            try
            {
                DateTime CurrentDateTime = DateTime.Now;
                string query = @"SELECT 
                SUM(ICPC_UCMDS) 'In-Coming Patient Calls-UCMDS',    SUM(IC_UCMDS) 'In-Coming Insurance Calls-UCMDS',    SUM(IS_UCMDS) 'Itemized Statements-UCMDS',    SUM(LVM_UCMDS) 'Left Voice Msgs-UCMDS',    SUM(OGIC_UCMDS) 'Out Going Insurance Calls-UCMDS',    SUM(OGPC_UCMDS) 'Out Going Patient Calls-UCMDS',    SUM(PC_UCMDS) 'Patient Correspondence-UCMDS',    SUM(PS_UCMDS) 'Patient Statements-UCMDS',    SUM(AVM_UCMDS) 'Answered Voice Msgs-UCMDS',    SUM(Emails_UCMDS) 'Emails-UCMDS',    SUM(ICIC_UCMDS) 'In-Coming Insurance Calls-UCMDS',    SUM(ICPC_ETC) 'In-Coming Patient Calls-ETC',
                SUM(IC_ETC) 'In-Coming Insurance Calls-ETC',    SUM(IS_ETC) 'Itemized Statements-ETC',    SUM(LVM_ETC) 'Left Voice Msgs-ETC',    SUM(OGIC_ETC) 'Out Going Insurance Calls-ETC',    SUM(OGPC_ETC) 'Out Going Patient Calls-ETC',    SUM(PC_ETC) 'Patient Correspondence-ETC',    SUM(PS_ETC) 'Patient Statements-ETC',    SUM(AVM_ETC) 'Answered Voice Msgs-ETC',    SUM(Emails_ETC) 'Emails-ETC',    SUM(ICIC_ETC) 'In-Coming Insurance Calls-ETC',    SUM(ICPC_MAMEdical) 'In-Coming Patient Calls-MAMEdical',    SUM(IC_MAMEdical) 'In-Coming Insurance Calls-MAMEdical',
                SUM(IS_MAMEdical) 'Itemized Statements-MAMEdical',    SUM(LVM_MAMEdical) 'Left Voice Msgs-MAMEdical',    SUM(OGIC_MAMEdical) 'Out Going Insurance Calls-MAMEdical',    SUM(OGPC_MAMEdical) 'Out Going Patient Calls-MAMEdical',    SUM(PC_MAMEdical) 'Patient Correspondence-MAMEdical',    SUM(PS_MAMEdical) 'Patient Statements-MAMEdical',    SUM(AVM_MAMEdical) 'Answered Voice Msgs-MAMEdical',    SUM(Emails_MAMEdical) 'Emails-MAMEdical',    SUM(ICIC_MAMEdical) 'In-Coming Insurance Calls-MAMEdical',    SUM(ICPC_Webster) 'In-Coming Patient Calls-Webster',    SUM(IC_Webster) 'In-Coming Insurance Calls-Webster',
                SUM(IS_Webster) 'Itemized Statements-Webster',    SUM(LVM_Webster) 'Left Voice Msgs-Webster',    SUM(OGIC_Webster) 'Out Going Insurance Calls-Webster',    SUM(OGPC_Webster) 'Out Going Patient Calls-Webster',    SUM(PC_Webster) 'Patient Correspondence-Webster',    SUM(PS_Webster) 'Patient Statements-Webster',    SUM(AVM_Webster) 'Answered Voice Msgs-Webster',    SUM(Emails_Webster) 'Emails-Webster',    SUM(ICIC_Webster) 'In-Coming Insurance Calls-Webster',    SUM(ICPC_Affordable) 'In-Coming Patient Calls-Affordable',    SUM(IC_Affordable) 'In-Coming Insurance Calls-Affordable',    SUM(IS_Affordable) 'Itemized Statements-Affordable',
                SUM(LVM_Affordable) 'Left Voice Msgs-Affordable',    SUM(OGIC_Affordable) 'Out Going Insurance Calls-Affordable',    SUM(OGPC_Affordable) 'Out Going Patient Calls-Affordable',    SUM(PC_Affordable) 'Patient Correspondence-Affordable',    SUM(PS_Affordable) 'Patient Statements-Affordable',    SUM(Pendings_AUC) 'Pendings-AUC',    SUM(AVM_Affordable) 'Answered Voice Msgs-Affordable',    SUM(Emails_Affordable) 'Emails-Affordable',    SUM(ICIC_Affordable) 'In-Coming Insurance Calls-Affordable',    SUM(ICPC_MEP) 'In-Coming Patient Calls-MEP',    SUM(IC_MEP) 'In-Coming Insurance Calls-MEP',    SUM(IS_MEP) 'Itemized Statements-MEP',
                SUM(LVM_MEP) 'Left Voice Msgs-MEP',    SUM(OGIC_MEP) 'Out Going Insurance Calls-MEP',    SUM(OGPC_MEP) 'Out Going Patient Calls-MEP',    SUM(PC_MEP) 'Patient Correspondence-MEP',    SUM(PS_MEP) 'Patient Statements-MEP',    SUM(AVM_MEP) 'Answered Voice Msgs-MEP',    SUM(Emails_MEP) 'Emails-MEP',    SUM(ICIC_MEP) 'In-Coming Insurance Calls-MEP',    SUM(ICPC_PEP) 'In-Coming Patient Calls-PEP',    SUM(IC_PEP) 'In-Coming Insurance Calls-PEP',    SUM(IS_PEP) 'Itemized Statements-PEP',    SUM(LVM_PEP) 'Left Voice Msgs-PEP',    SUM(OGIC_PEP) 'Out Going Insurance Calls-PEP',    SUM(OGPC_PEP) 'Out Going Patient Calls-PEP',  
                SUM(PC_PEP) 'Patient Correspondence-PEP',    SUM(PS_PEP) 'Patient Statements-PEP',    SUM(AVM_PEP) 'Answered Voice Msgs-PEP',    SUM(Emails_PEP) 'Emails-PEP',    SUM(ICIC_PEP) 'In-Coming Insurance Calls-PEP',    SUM(ICPC_Xenialz) 'In-Coming Patient Calls-Xenialz',    SUM(IC_Xenialz) 'In-Coming Insurance Calls-Xenialz',    SUM(IS_Xenialz) 'Itemized Statements-Xenialz',    SUM(LVM_Xenialz) 'Left Voice Msgs-Xenialz',    SUM(OGIC_Xenialz) 'Out Going Insurance Calls-Xenialz',    SUM(OGPC_Xenialz) 'Out Going Patient Calls-Xenialz',    SUM(PC_Xenialz) 'Patient Correspondence-Xenialz',    SUM(PS_Xenialz) 'Patient Statements-Xenialz',  
                SUM(AVM_Xenialz) 'Answered Voice Msgs-Xenialz',    SUM(Emails_Xenialz) 'Emails-Xenialz',    SUM(ICIC_Xenialz) 'In-Coming Insurance Calls-Xenialz',    SUM(ICPC_Brace) 'In-Coming Patient Calls-Brace',    SUM(IC_Brace) 'In-Coming Insurance Calls-Brace',    SUM(IS_Brace) 'Itemized Statements-Brace',    SUM(LVM_Brace) 'Left Voice Msgs-Brace',    SUM(OGIC_Brace) 'Out Going Insurance Calls-Brace',    SUM(OGPC_Brace) 'Out Going Patient Calls-Brace',    SUM(PC_Brace) 'Patient Correspondence-Brace',    SUM(PS_Brace) 'Patient Statements-Brace',
                SUM(AVM_Brace) 'Answered Voice Msgs-Brace',    SUM(Emails_Brace) 'Emails-Brace',    SUM(ICIC_Brace) 'In-Coming Insurance Calls-Brace'
            FROM
                PatientHelpDesk WHERE
                                EmployeeID = '" + ObjEmployees.EmployeeID + @"'
                        AND YEAR(PatientHelpDeskDateTime) = '" + CurrentDateTime.Year + "'";
                var Result = db.Database.SqlQuery<VMPHDClaimsCount>(query).FirstOrDefault();

                return Json(Result, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }
        //////////////////////////////////////// Check Record for update

        public JsonResult GetEmployeesCredentialingRecord(Credentialing ObjCredentialing)
        {
            try
            {
                //  DateTime Dt = Convert.ToDateTime(FilterDT);
                var Credentialing = db.Credentialing.Where(c => c.EmployeeID == ObjCredentialing.EmployeeID && c.CredentialingDateTime == ObjCredentialing.CredentialingDateTime && c.PracticeID == ObjCredentialing.PracticeID).FirstOrDefault();

                if (Credentialing != null)
                {
                    return Json(Credentialing, JsonRequestBehavior.AllowGet);
                }
                return Json("null", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetEmployeesAssignmentsRecord(Assignments Assignments)
        {
            try
            {
                //  DateTime Dt = Convert.ToDateTime(FilterDT);
                var ObjAssignments = db.Assignments.Where(a => a.EmployeeID == Assignments.EmployeeID && a.AssignmentDateTime == Assignments.AssignmentDateTime && a.PracticeID == Assignments.PracticeID && a.DepartmentID == Assignments.DepartmentID).FirstOrDefault();

                if (ObjAssignments != null)
                {
                    return Json(ObjAssignments, JsonRequestBehavior.AllowGet);
                }
                return Json("null", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetEmployeesFinancialRecord(Financial ObjFinancial)
        {
            try
            {
                //  DateTime Dt = Convert.ToDateTime(FilterDT);
                var FinancialRecord = db.Financials.Where(f => f.EmployeeID == ObjFinancial.EmployeeID && f.FinancialDateTime == ObjFinancial.FinancialDateTime && f.PracticeID == ObjFinancial.PracticeID && f.DepartmentID == ObjFinancial.DepartmentID).FirstOrDefault();
                if (FinancialRecord != null)
                {
                    return Json(FinancialRecord, JsonRequestBehavior.AllowGet);
                }
                return Json("null", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetEmployeesPPFinancialRecord(PPFinancials ObjFinancial)
        {
            try
            {
                //  DateTime Dt = Convert.ToDateTime(FilterDT);
                var FinancialRecord = db.PPFinancials.Where(f => f.EmployeeID == ObjFinancial.EmployeeID && f.FinancialDateTime == ObjFinancial.FinancialDateTime && f.PracticeID == ObjFinancial.PracticeID && f.DepartmentID == ObjFinancial.DepartmentID).FirstOrDefault();
                if (FinancialRecord != null)
                {
                    return Json(FinancialRecord, JsonRequestBehavior.AllowGet);
                }
                return Json("null", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetEmployeesPaitentHelpDeskRecord(PatientHelpDesk PatientHelpDesk)
        {
            try
            {
                //  DateTime Dt = Convert.ToDateTime(FilterDT);
                var PaitentHelpDesk = db.PatientHelpDesk.Where(p => p.EmployeeID == PatientHelpDesk.EmployeeID && p.PatientHelpDeskDateTime == PatientHelpDesk.PatientHelpDeskDateTime).ToList();


                if (PaitentHelpDesk != null)
                {
                    return Json(PaitentHelpDesk, JsonRequestBehavior.AllowGet);
                }
                return Json("null", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetEmployeesAgingRecord(Agings ObjAgings)
        {
            try
            {
                //  DateTime Dt = Convert.ToDateTime(FilterDT);
                var AgingRecord = db.Aging.Where(a => a.EmployeeID == ObjAgings.EmployeeID && a.AgingDateTime == ObjAgings.AgingDateTime && a.PracticeID == ObjAgings.PracticeID).FirstOrDefault();
                if (AgingRecord != null)
                {
                    return Json(AgingRecord, JsonRequestBehavior.AllowGet);
                }
                return Json("null", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetEmployeesAssignmentsList(Assignments ObjFilter)
        {
            try
            {

                var AssignmentRecord = db.Assignments.Where(a => a.EmployeeID == ObjFilter.EmployeeID && a.AssignmentDateTime == ObjFilter.AssignmentDateTime).ToList();
                if (AssignmentRecord != null)
                {
                    return Json(AssignmentRecord, JsonRequestBehavior.AllowGet);
                }
                return Json("null", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetEmployeesFinancialList(Financial ObjFilter)
        {
            try
            {

                var FinancialRecord = db.Financials.Where(a => a.EmployeeID == ObjFilter.EmployeeID && a.FinancialDateTime == ObjFilter.FinancialDateTime).ToList();
                if (FinancialRecord != null)
                {
                    return Json(FinancialRecord, JsonRequestBehavior.AllowGet);
                }
                return Json("null", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }
        //auth for update productivity

        public JsonResult AuthencateAdmin(Managers ObjManager)
        {
            try
            {
                ObjManager.Password = EncryptDecrypt.Encrypt(ObjManager.Password, true);
                var Manager = db.Managers.Where(u => u.UserName == ObjManager.UserName && u.Password == ObjManager.Password && u.RoleID == 1).FirstOrDefault();
                if (Manager != null)
                {
                    return Json(Manager, JsonRequestBehavior.AllowGet);
                }

                return Json("null", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }


        ////// Check Employees Login Session

        public JsonResult CheckEmployeeLoginSession()
        {
            try
            {
                if (Session["EmployeeID"] != null)
                {
                    if (Convert.ToInt64(Session["EmployeeID"]) > 0)
                    {
                        var EmployeesInfo = db.Employees.Find(Convert.ToInt64(Session["EmployeeID"]));
                        return Json(EmployeesInfo, JsonRequestBehavior.AllowGet);
                    }
                    return Json("invalid", JsonRequestBehavior.AllowGet);
                }
                return Json("null", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }



        ////////// Email Sending/////////////

        public JsonResult SendCredentialingEmail(Credentialing ObjCredentialing, Practices ObjPractices, Employees ObjEmployee)
        {
            try
            {
                string body = string.Empty;
                //using streamreader for reading my htmltemplate   

                using (StreamReader reader = new StreamReader(Server.MapPath("~/Templates/Email/Credentialing.html")))

                {

                    body = reader.ReadToEnd();

                }

                body = body.Replace("{CredentialingDateTime}", ObjCredentialing.CredentialingDateTime.ToShortDateString()); //replacing the required things  

                body = body.Replace("{Header}", "Daily Productivity Report");
                body = body.Replace("{PracticeName}", ObjPractices.PracticeName);
                body = body.Replace("{SendToName}", "Tahir Ali");

                body = body.Replace("{CredentialingCalls}", ObjCredentialing.CredentialingCalls.ToString());

                body = body.Replace("{CredentialingDocuments}", ObjCredentialing.CredentialingDocuments.ToString());
                body = body.Replace("{EDICalls}", ObjCredentialing.EDICalls.ToString());
                body = body.Replace("{EDISetup}", ObjCredentialing.EDISetup.ToString());
                body = body.Replace("{ERACalls}", ObjCredentialing.ERACalls.ToString());
                body = body.Replace("{ERASetup}", ObjCredentialing.ERASetup.ToString());
                body = body.Replace("{EFTCalls}", ObjCredentialing.EFTCalls.ToString());
                body = body.Replace("{EFTSetup}", ObjCredentialing.EFTSetup.ToString());
                body = body.Replace("{WebLogins}", ObjCredentialing.WebLogins.ToString());
                body = body.Replace("{EnrollmentTrackingSheet}", ObjCredentialing.EnrollmentTrackingSheet.ToString());
                body = body.Replace("{Emails}", ObjCredentialing.Emails.ToString());
                body = body.Replace("{Total}", ObjCredentialing.CredentialingSum().ToString());

                return Json(SendHtmlFormattedEmail("Pain Equipment Professionals LLC-Daily Producitivity & Financial Report", ObjEmployee.Email, body), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult SendRcmgMaEmail(Assignments ObjAssignment, Financial ObjFinancial, Agings ObjAging, Practices ObjPractices, Employees ObjEmployee)
        {
            try
            {
                string body = string.Empty;
                //using streamreader for reading my htmltemplate   

                using (StreamReader reader = new StreamReader(Server.MapPath("~/Templates/Email/RCMG-MA.html")))

                {

                    body = reader.ReadToEnd();

                }

                body = body.Replace("{AssignmentDateTime}", ObjAssignment.AssignmentDateTime.ToShortDateString()); //replacing the required things  

                body = body.Replace("{Header}", "Daily Productivity & Financial Report");
                body = body.Replace("{PracticeName}", ObjPractices.PracticeName);
                body = body.Replace("{SendToName}", "Manager");

                body = body.Replace("{EnteredClaims}", ObjAssignment.EnteredClaims.ToString());
                body = body.Replace("{DailyChargesECW}", ObjFinancial.DailyChargesECW.ToString());
                body = body.Replace("{Eligibility}", ObjAssignment.Eligibility.ToString());
                body = body.Replace("{DailyPaymentseTeClinic}", ObjFinancial.DailyPaymentseTeClinic.ToString());
                body = body.Replace("{PF_eBridge}", ObjAssignment.PF_eBridge.ToString());
                body = body.Replace("{DailyPaymentsECW}", ObjFinancial.DailyPaymentsECW.ToString());
                body = body.Replace("{RejectionsEmdeon}", ObjAssignment.RejectionsEmdeon.ToString());
                body = body.Replace("{MTDCharges}", ObjFinancial.MTDCharges.ToString());
                body = body.Replace("{RejectionsAvaility}", ObjAssignment.RejectionsAvaility.ToString());
                body = body.Replace("{MTDPaymentseTeClinic}", ObjFinancial.MTDPaymentseTeClinic.ToString());
                body = body.Replace("{ERADenials}", ObjAssignment.ERADenials.ToString());
                body = body.Replace("{MTDPaymentsECW}", ObjFinancial.MTDPaymentsECW.ToString());
                body = body.Replace("{ERAUnmatched}", ObjAssignment.ERAUnmatched.ToString());
                body = body.Replace("{AgingAmounteTeClinic}", ObjFinancial.AgingAmounteTeClinic.ToString());
                body = body.Replace("{Appeals}", ObjAssignment.Appeals.ToString());
                body = body.Replace("{AgingAmounteCW}", ObjFinancial.AgingAmounteCW.ToString());
                body = body.Replace("{Calls}", ObjAssignment.Calls.ToString());
                body = body.Replace("{AgingClaims}", ObjAssignment.AgingClaims.ToString());
                body = body.Replace("{Email}", ObjAssignment.Emails.ToString());
                body = body.Replace("{PTStatements}", ObjAssignment.PTStatements.ToString());
                body = body.Replace("{EOBPayment}", ObjAssignment.EOBpayment.ToString());
                body = body.Replace("{Total}", ObjAssignment.RcmgMASum().ToString());

                body = body.Replace("{PracticeName}", ObjPractices.PracticeName);
                body = body.Replace("{A_30}", ObjAging.A_30.ToString());
                body = body.Replace("{A_60}", ObjAging.A_60.ToString());
                body = body.Replace("{A_90}", ObjAging.A_90.ToString());
                body = body.Replace("{A_120}", ObjAging.A_120.ToString());
                body = body.Replace("{A_120_Up}", ObjAging.A_120_up.ToString());
                body = body.Replace("{A_Total}", ObjAging.AgingsSum().ToString());
                body = body.Replace("{Issues_Of_120_Up_Claims}", ObjAging.Issues_Of_120_Up_Claims.ToString());
                return Json(SendHtmlFormattedEmail("Pain Equipment Professionals LLC-Daily Producitivity & Financial Report", ObjEmployee.Email, body), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult SendRcmgEmail(Assignments ObjAssignment, Financial ObjFinancial, Agings ObjAging, Practices ObjPractices, Employees ObjEmployee)
        {
            try
            {
                string body = string.Empty;
                //using streamreader for reading my htmltemplate   

                using (StreamReader reader = new StreamReader(Server.MapPath("~/Templates/Email/RCMG.html")))

                {

                    body = reader.ReadToEnd();

                }

                body = body.Replace("{AssignmentDateTime}", ObjAssignment.AssignmentDateTime.ToShortDateString()); //replacing the required things  

                body = body.Replace("{Header}", "Daily Productivity & Financial Report");
                body = body.Replace("{PracticeName}", ObjPractices.PracticeName);
                body = body.Replace("{SendToName}", "Manager");

                body = body.Replace("{PF_eBridge}", ObjAssignment.PF_eBridge.ToString());
                body = body.Replace("{DailyCharges}", ObjFinancial.DailyCharges.ToString());
                body = body.Replace("{Rejections}", ObjAssignment.Rejections.ToString());
                body = body.Replace("{DailyPayments}", ObjFinancial.DailyPayments.ToString());
                body = body.Replace("{ERADenials}", ObjAssignment.ERADenials.ToString());
                body = body.Replace("{MTDCharges}", ObjFinancial.MTDCharges.ToString());
                body = body.Replace("{ERAUnmatched}", ObjAssignment.ERAUnmatched.ToString());
                body = body.Replace("{MTDPayments}", ObjFinancial.MTDPayments.ToString());
                body = body.Replace("{PTLResponses}", ObjAssignment.PTLResponses.ToString());
                body = body.Replace("{AgingAmount}", ObjFinancial.AgingAmount.ToString());
                body = body.Replace("{Appeals}", ObjAssignment.Appeals.ToString());
                body = body.Replace("{Calls}", ObjAssignment.Calls.ToString());
                body = body.Replace("{AgingClaims}", ObjAssignment.AgingClaims.ToString());
                body = body.Replace("{Emails}", ObjAssignment.Emails.ToString());
                body = body.Replace("{PTStatements}", ObjAssignment.PTStatements.ToString());

                body = body.Replace("{Total}", ObjAssignment.RcmgSum().ToString());

                body = body.Replace("{PracticeName}", ObjPractices.PracticeName);
                body = body.Replace("{A_30}", ObjAging.A_30.ToString());
                body = body.Replace("{A_60}", ObjAging.A_60.ToString());
                body = body.Replace("{A_90}", ObjAging.A_90.ToString());
                body = body.Replace("{A_120}", ObjAging.A_120.ToString());
                body = body.Replace("{A_120_Up}", ObjAging.A_120_up.ToString());
                body = body.Replace("{A_Total}", ObjAging.AgingsSum().ToString());
                body = body.Replace("{Issues_Of_120_Up_Claims}", ObjAging.Issues_Of_120_Up_Claims.ToString());
                return Json(SendHtmlFormattedEmail("Pain Equipment Professionals LLC-Daily Producitivity & Financial Report", ObjEmployee.Email, body), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult SendDMEEmail(Assignments ObjAssignment, Financial ObjFinancial, Agings ObjAging, Practices ObjPractices, Employees ObjEmployee)
        {
            try
            {
                string body = string.Empty;
                //using streamreader for reading my htmltemplate   

                using (StreamReader reader = new StreamReader(Server.MapPath("~/Templates/Email/DME.html")))

                {

                    body = reader.ReadToEnd();

                }

                body = body.Replace("{AssignmentDateTime}", ObjAssignment.AssignmentDateTime.ToShortDateString()); //replacing the required things  

                body = body.Replace("{Header}", "Daily Productivity & Financial Report");
                body = body.Replace("{PracticeName}", ObjPractices.PracticeName);
                body = body.Replace("{SendToName}", "Manager");
                body = body.Replace("{Calls}", ObjAssignment.Calls.ToString());
                body = body.Replace("{AgingClaims}", ObjAssignment.AgingClaims.ToString());
                body = body.Replace("{Emails}", ObjAssignment.Emails.ToString());
                body = body.Replace("{Others}", ObjAssignment.Others.ToString());
                body = body.Replace("{Appeals}", ObjAssignment.Appeals.ToString());

                body = body.Replace("{DailyCharges}", ObjFinancial.DailyCharges.ToString());
                body = body.Replace("{DailyPayments}", ObjFinancial.DailyPayments.ToString());
                body = body.Replace("{MTDCharges}", ObjFinancial.MTDCharges.ToString());
                body = body.Replace("{MTDPayments}", ObjFinancial.MTDPayments.ToString());
                body = body.Replace("{AgingAmount}", ObjFinancial.AgingAmount.ToString());


                body = body.Replace("{Total}", ObjAssignment.DMESum().ToString());

                body = body.Replace("{PracticeName}", ObjPractices.PracticeName);
                body = body.Replace("{A_30}", ObjAging.A_30.ToString());
                body = body.Replace("{A_60}", ObjAging.A_60.ToString());
                body = body.Replace("{A_90}", ObjAging.A_90.ToString());
                body = body.Replace("{A_120}", ObjAging.A_120.ToString());
                body = body.Replace("{A_120_Up}", ObjAging.A_120_up.ToString());
                body = body.Replace("{A_Total}", ObjAging.AgingsSum().ToString());
                body = body.Replace("{Issues_Of_120_Up_Claims}", ObjAging.Issues_Of_120_Up_Claims.ToString());
                return Json(SendHtmlFormattedEmail("Pain Equipment Professionals LLC-Daily Producitivity & Financial Report", ObjEmployee.Email, body), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult SendEligibilityEmail(Assignments ObjAssignment, Practices ObjPractices, Employees ObjEmployee)
        {
            try
            {
                string body = string.Empty;
                //using streamreader for reading my htmltemplate   

                using (StreamReader reader = new StreamReader(Server.MapPath("~/Templates/Email/Eligibility.html")))

                {

                    body = reader.ReadToEnd();

                }

                body = body.Replace("{AssignmentDateTime}", ObjAssignment.AssignmentDateTime.ToShortDateString()); //replacing the required things  

                body = body.Replace("{Header}", "Daily Eligibility Report");
                body = body.Replace("{PracticeName}", ObjPractices.PracticeName);
                body = body.Replace("{SendToName}", "Manager");
                body = body.Replace("{Calls}", ObjAssignment.Calls.ToString());
                body = body.Replace("{WebVerification}", ObjAssignment.WebVerification.ToString());
                body = body.Replace("{FaxedVerification}", ObjAssignment.FaxedVerification.ToString());
                body = body.Replace("{Others}", ObjAssignment.Others.ToString());
                body = body.Replace("{EmailVerification }", ObjAssignment.EmailVerification.ToString());
                body = body.Replace("{Total}", ObjAssignment.EligibilitySum().ToString());

                return Json(SendHtmlFormattedEmail("Pain Equipment Professionals LLC-Daily Producitivity & Financial Report", ObjEmployee.Email, body), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult SendBillingEmail(List<BillingEmail> VMBillingEmail, Employees ObjEmployee)
        {
            try
            {
                string body = string.Empty;
                string Master = string.Empty;
                string table = "";
                using (StreamReader reader = new StreamReader(Server.MapPath("~/Templates/Email/Master.html")))

                {

                    Master = reader.ReadToEnd();

                }
                foreach (var email in VMBillingEmail)
                {
                    using (StreamReader reader = new StreamReader(Server.MapPath("~/Templates/Email/Billing.html")))

                    {

                        body = reader.ReadToEnd();

                    }
                    body = body.Replace("{Name}", ObjEmployee.FirstName + "" + ObjEmployee.LastName); //replacing the required things  

                    body = body.Replace("{WorkDate}", email.WorkDate.ToShortDateString());
                    body = body.Replace("{PracticeName}", (db.Practices.Find(email.PracticeID).PracticeName));

                    body = body.Replace("{ReceivedBills}", email.ReceivedBills.ToString());
                    body = body.Replace("{Eligibility}", email.Eligibility.ToString());
                    body = body.Replace("{BillingAnalysis}", email.BillingAnalysis.ToString());

                    body = body.Replace("{DateofService}", email.DateOfService.ToShortDateString());
                    body = body.Replace("{EnteredClaims}", email.EnteredClaims.ToString());
                    body = body.Replace("{PendingClaims}", email.PendingClaims.ToString());

                    body = body.Replace("{PaidClaims}", email.PaidClaims.ToString());
                    body = body.Replace("{PreparedClaims}", email.PreparedClaims.ToString());
                    body = body.Replace("{EnteredChargesP}", email.EnteredChargeP.ToString());
                    body = body.Replace("{EnteredChargesI}", email.EnteredChargeI.ToString());
                    body = body.Replace("{MissingLog}", email.MissingLog.ToString());

                    body = body.Replace("{Emails}", email.Emails.ToString());
                    body = body.Replace("{QA}", email.QA.ToString());
                    table += body;
                }
                Master = Master.Replace("{table}", table);
                return Json(SendHtmlFormattedEmail("Pain Equipment Professionals LLC-Daily Producitivity & Financial Report", ObjEmployee.Email, Master), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SendPSMEmail(Assignments ObjAssignment, Practices ObjPractices, Employees ObjEmployee)
        {
            try
            {
                string body = "";
                using (StreamReader reader = new StreamReader(Server.MapPath("~/Templates/Email/PSVEmail.html")))

                {

                    body = reader.ReadToEnd();

                }

                body = body.Replace("{CreatedDate}", ObjAssignment.AssignmentDateTime.ToShortDateString()); //replacing the required things  
                body = body.Replace("{CurrMonth}", ObjAssignment.AssignmentDateTime.ToString("MMMM - yyyy")); //replacing the required things  


                body = body.Replace("{PracticeName}", ObjPractices.PracticeName);
                body = body.Replace("{SendToName}", "Manager");
                body = body.Replace("{Multiple}", ObjAssignment.Multiple.ToString());
                body = body.Replace("{NoPatientBill}", ObjAssignment.NoPatientBill.ToString());
                body = body.Replace("{Total}", (ObjAssignment.Multiple + ObjAssignment.NoPatientBill).ToString());
                body = body.Replace("{UVTotalClaims}", ObjAssignment.UVTotalClaims.ToString());
                body = body.Replace("{Done}", ObjAssignment.Done.ToString());
                body = body.Replace("{PendingClaims}", ObjAssignment.PendingClaims.ToString());

                return Json(SendHtmlFormattedEmail("Pain Equipment Professionals LLC-Daily Producitivity & Financial Report", ObjEmployee.Email, body), JsonRequestBehavior.AllowGet);



                return Json("exception", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SendPaymentPostingEmail(Assignments ObjAssignment, PPFinancials ObjFinincial, Practices ObjPractices, Employees ObjEmployee)
        {
            try
            {
                string body = string.Empty;
                //using streamreader for reading my htmltemplate   

                using (StreamReader reader = new StreamReader(Server.MapPath("~/Templates/Email/PaymentPostingEmail.html")))

                {

                    body = reader.ReadToEnd();

                }

                body = body.Replace("{AssignmentDateTime}", ObjAssignment.AssignmentDateTime.ToShortDateString()); //replacing the required things  
                body = body.Replace("{PracticeName}", ObjPractices.PracticeName);
                body = body.Replace("{SendToName}", "Manager");
                body = body.Replace("{PF_eBridge}", ObjAssignment.PF_eBridge.ToString());
                body = body.Replace("{Rejections}", ObjAssignment.Rejections.ToString());
                body = body.Replace("{ERADenials}", ObjAssignment.ERADenials.ToString());
                body = body.Replace("{ERAUnmatched}", ObjAssignment.ERAUnmatched.ToString());
                body = body.Replace("{Appeals}", ObjAssignment.Appeals.ToString());
                body = body.Replace("{ManuallyAdjustments}", ObjAssignment.ManuallyAdjustments.ToString());
                body = body.Replace("{AgingClaims}", ObjAssignment.AgingClaims.ToString());
                body = body.Replace("{BillingETC}", ObjAssignment.BillingETC.ToString());
                body = body.Replace("{Emails}", ObjAssignment.Emails.ToString());
                body = body.Replace("{PTStatements}", ObjAssignment.PTStatements.ToString());
                body = body.Replace("{VerificationClaims}", ObjAssignment.VerificationClaims.ToString());
                body = body.Replace("{Total}", ObjAssignment.PPostingSum().ToString());

                body = body.Replace("{FPF_eBridge}", ObjFinincial.PF_eBridge.ToString());
                body = body.Replace("{FRejections}", ObjFinincial.Rejections.ToString());
                body = body.Replace("{FERADenials}", ObjFinincial.ERADenials.ToString());
                body = body.Replace("{FERAUnmatched}", ObjFinincial.ERAUnmatched.ToString());
                body = body.Replace("{FAppeals}", ObjFinincial.Appeals.ToString());
                body = body.Replace("{FManuallyAdjustments}", ObjFinincial.ManuallyAdjustments.ToString());
                body = body.Replace("{FAgingClaims}", ObjFinincial.AgingClaims.ToString());
                body = body.Replace("{FBillingETC}", ObjFinincial.BillingETC.ToString());
                body = body.Replace("{FEmails}", ObjFinincial.Emails.ToString());
                body = body.Replace("{FPTStatements}", ObjFinincial.PTStatements.ToString());
                body = body.Replace("{FVerificationClaims}", ObjFinincial.VerificationClaims.ToString());
                body = body.Replace("{FTotal}", ObjFinincial.PPostingSum().ToString());
                return Json(SendHtmlFormattedEmail("Pain Equipment Professionals LLC-Daily Producitivity & Financial Report", ObjEmployee.Email, body), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }
        private string SendHtmlFormattedEmail(string Subject, string From, string Body)

        {

            try
            {
                using (MailMessage mailMessage = new MailMessage())

                {
                    mailMessage.From = new MailAddress(From);
                    mailMessage.Subject = Subject;
                    mailMessage.Body = Body;
                    mailMessage.IsBodyHtml = true;
                    mailMessage.To.Add(new MailAddress("haris.khurshid@nexelservices.com"));
                    mailMessage.CC.Add(new MailAddress("harishashmi252@hotmail.com"));
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = WebConfigurationManager.AppSettings["HOST"];
                    smtp.EnableSsl = true;
                    System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
                    NetworkCred.UserName = WebConfigurationManager.AppSettings["RegistrationEmail"];
                    NetworkCred.Password = WebConfigurationManager.AppSettings["RegPassword"];
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = Convert.ToInt32(WebConfigurationManager.AppSettings["PORT"]);
                    smtp.Send(mailMessage);
                    return "success";
                }
            }
            catch (SmtpFailedRecipientException ex)
            {
                return "exception";
            }
        }
    }
}