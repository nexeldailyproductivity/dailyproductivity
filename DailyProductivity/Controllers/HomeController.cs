﻿
using DailyProductivity.App_Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DailyProductivity.Controllers
{
    [SessionExpire]
    public class HomeController : Controller
    {
       public ActionResult Dashboard()
        {
            return View();
        }
    }
}