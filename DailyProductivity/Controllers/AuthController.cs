﻿using DailyProductivity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DailyProductivity.Controllers
{
    public class AuthController : Controller
    {
        DBContextDataBase db = new DBContextDataBase();
        // GET: Auth
        public ActionResult Login(string returnUrl)
        
{
            if (Session["EmployeeID"] != null || Session["ManagerID"] != null)
            {
                return Redirect();
            }
            if (returnUrl==null)
            {
                returnUrl = "";
            }
            TempData["returnUrl"] = returnUrl;
            ViewBag.returnUrl = returnUrl;
            return View();
        }
        #region UserAttentication

       
        public ActionResult Redirect()
        {
            if (Session["EmployeeID"] != null || Session["ManagerID"] != null)
            {
                if (Session["RoleID"].ToString()=="1")
                {
                    return RedirectToAction("dashboard", "home",new {Area="admin" });
                }
                else if (Session["RoleID"].ToString() == "2")
                {
                    return RedirectToAction("dashboard", "home", new { Area = "" });
                }
                
            }
            return null;
        }
        public JsonResult GetRememberMe()
        {
            ///string value = Session["user"].ToString();
            bool RememberME = false;
            string UserName = "";
            if (Request.Cookies["PMS"] != null)
            {
                string username = Request.Cookies["PMS"].Values["UserName"];

                UserName = username;
                RememberME = true;
            };
            var result = new { uname = UserName, rem = RememberME };
            return Json(result, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public JsonResult Loginvalidate(Employees objRegistration)
        {
            long RoleID = 0;

            string msgtype = "";

            try
            {
                
                string password = EncryptDecrypt.Encrypt(objRegistration.Password, true);
                var ObjAdmin = (from reg in db.Managers
                                where reg.IsActive == true && reg.UserName == objRegistration.UserName && reg.Password == password && reg.RoleID==1
                           select reg).SingleOrDefault();

                Employees ObjUser = (from reg in db.Employees
                               where reg.IsActive == true && reg.UserName == objRegistration.UserName && reg.Password == password && reg.RoleID == 2
                               select reg).SingleOrDefault();
                if (ObjAdmin != null)
                {
                    if (ObjAdmin.IsActive)
                    {
                        RoleID = ObjAdmin.RoleID;
                        Session["FirstName"] = ObjAdmin.FirstName;
                        Session["LastName"] = ObjAdmin.LastName;
                        Session["Username"] = ObjAdmin.UserName;
                        Session["Password"] = EncryptDecrypt.Decrypt(password, true);
                        Session["Email"] = ObjAdmin.Email;
                        Session["ManagerID"] = ObjAdmin.ManagerID.ToString();
                        Session["RoleID"] = ObjAdmin.RoleID;
                        Session["Designation"] = ObjAdmin.Designation;

                        if (TempData.Count > 0)
                        {
                            objRegistration.returnUrl = TempData["returnUrl"].ToString();

                        }
                        if (ObjAdmin.RoleID == 2)
                        {
                            Session["RoleName"] = "Manager";
                        }
                        Session["Phone"] = ObjAdmin.Phone;

                        if (objRegistration.RememberMe == true)
                        {

                            HttpCookie cookie = new HttpCookie("PMS");
                            cookie.Values.Add("UserName", ObjAdmin.UserName);
                            cookie.Expires = DateTime.Now.AddDays(15);
                            Response.Cookies.Add(cookie);
                        }
                        else
                        {
                            HttpCookie myCookie = new HttpCookie("PMS");
                            myCookie.Expires = DateTime.Now.AddDays(-1d);
                            Response.Cookies.Add(myCookie);
                        }
                        return Json(new { msg = "found", RoleID = 1, Role = "Admin",returnUri= objRegistration.returnUrl }, JsonRequestBehavior.AllowGet);
                    }
                    return Json(new { msg = "inactive" }, JsonRequestBehavior.AllowGet);
                }
                else if (ObjUser!=null)
                {
                    if (ObjUser.IsActive)
                    {
                        RoleID = ObjUser.RoleID;
                        Session["FirstName"] = ObjUser.FirstName;
                        Session["LastName"] = ObjUser.LastName;
                        Session["username"] = ObjUser.UserName;
                        Session["Password"] = EncryptDecrypt.Decrypt(password, true);
                        Session["Email"] = ObjUser.Email;
                        Session["EmployeeID"] = ObjUser.EmployeeID.ToString();
                        Session["RoleID"] = ObjUser.RoleID;
                        Session["ShiftTime"] = ObjUser.ShiftTiming;
                        Session["Designation"] = ObjUser.Designation;
                        Session["DepartmentID"] = ObjUser.DepartmentID;
                        Session["ProfilePic"] = ObjUser.UserPic;
                        ViewBag.DepartmentID = ObjUser.DepartmentID;

                        if (TempData.Count > 0)
                        {
                            objRegistration.returnUrl = TempData["returnUrl"].ToString();

                        }
                        if (ObjUser.RoleID == 2)
                        {
                            Session["RoleName"] = "Employee";
                        }
                        Session["Phone"] = ObjUser.Phone;
                        //  Session["IsAdmin"] = obj.IsAdmin;
                        if (String.IsNullOrEmpty(ObjUser.UserPic))
                        {

                            Session["UserPic"] = "dummy.png";
                        }
                        else
                        {
                            Session["UserPic"] = ObjUser.UserPic;
                        }


                        if (objRegistration.RememberMe == true)
                        {

                            HttpCookie cookie = new HttpCookie("PMS");
                            cookie.Values.Add("UserName", ObjUser.UserName);
                            cookie.Expires = DateTime.Now.AddDays(15);
                            Response.Cookies.Add(cookie);
                        }
                        else
                        {
                            HttpCookie myCookie = new HttpCookie("PMS");
                            myCookie.Expires = DateTime.Now.AddDays(-1d);
                            Response.Cookies.Add(myCookie);
                        }
                        return Json(new { msg = "found", RoleID = 2, Role = "Employee", returnUri = objRegistration.returnUrl }, JsonRequestBehavior.AllowGet);


                    }
                    return Json(new { msg = "inactive" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { msg = "notfound"}, JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ee)
            {
                return Json(new { msg= "exception" },JsonRequestBehavior.AllowGet);

            }


        }

        #endregion
        public ActionResult Logout()
        {
            Session.RemoveAll();
            return RedirectToAction("Login");

        }
    }
}