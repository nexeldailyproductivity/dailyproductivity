﻿using System.Web.Mvc;

namespace DailyProductivity.Areas.Admin
{
    public class AdminAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Admin";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Admin_default",
                "admin/{controller}/{action}/{id}",
                 defaults: new {controller="auth", action = "login", id = UrlParameter.Optional,AreaName="" },
            namespaces: new[] { "DailyProductivity.Areas.Admin.Controllers" }
                 );
        }
    }
}