﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DailyProductivity.App_Start;

namespace DailyProductivity.Areas.Admin.Controllers
{
    [SessionExpire, IsAdmin]

    public class ManagerController : Controller
    {
        public ActionResult Manager()
        {
            return View();
        }
        public ActionResult Managers()
        {
            return View();
        }
        public ActionResult Manage(string id)
        {
            ViewBag.ManagerID = Convert.ToInt64(EncryptDecrypt.Decrypt(id, true));
            return View();
        }
    }
}