﻿//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using DailyProductivity.App_Start;

namespace DailyProductivity.Areas.Admin.Controllers
{
    [SessionExpire,IsAdmin]
    public class PracticesController : Controller
	{
		public ActionResult AddPractice(){
			return View();
		}
		public ActionResult Manage()
		{
			return View();
		}
		public ActionResult Forcast(){
			return View();
		}
        public ActionResult EditPractice(string id)
        {
            ViewBag.PracticeID =Convert.ToInt64(EncryptDecrypt.Decrypt(id, true));
            return View();
        }
    }


}
