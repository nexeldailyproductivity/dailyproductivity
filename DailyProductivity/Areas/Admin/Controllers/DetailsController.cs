﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DailyProductivity.App_Start;

namespace DailyProductivity.Areas.Admin.Controllers
{
    [SessionExpire, IsAdmin]

    public class DetailsController : Controller
    {
        // GET: Admin/Details
        public ActionResult Departments() => View();

        public ActionResult Employees() => View();

        public ActionResult Practices() => View();
    }
}