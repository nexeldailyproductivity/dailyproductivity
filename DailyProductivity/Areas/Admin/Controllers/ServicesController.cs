﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using DailyProductivity.App_Start;
using DailyProductivity.Models;
using DailyProductivity.Models.ViewModels;

namespace DailyProductivity.Areas.Admin.Controllers
{
    [SessionExpire, IsAdmin]

    public class ServicesController : Controller
    {
        private readonly DBContextDataBase db = new DBContextDataBase();

        #region Employees-Manager section

        public JsonResult AddEmployees(Employees ObjEmployees)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Employees.Add(ObjEmployees);
                    db.SaveChanges();
                    return Json("", JsonRequestBehavior.AllowGet);
                }

                return Json("model", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetRoles()
        {
            try
            {
                var Roles = db.Role.OrderBy(r => r.RoleID).ToList();
                if (Roles.Count > 0) return Json(Roles, JsonRequestBehavior.AllowGet);
                return Json("null", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetDepartments()
        {
            try
            {
                //var Department = db.Department.OrderBy(d => d.DepartmentID) .ToList();
                var Departments = (from d in db.Department
                                   select new
                                   {
                                       d.DepartmentID,
                                       d.DepartmentName
                                   }).AsEnumerable().Select(d => new
                                   {
                                       d.DepartmentID,
                                       d.DepartmentName,
                                       EDID = EncryptDecrypt.Encrypt(d.DepartmentID.ToString(), true)
                                   }).ToList();
                if (Departments.Count > 0) return Json(Departments, JsonRequestBehavior.AllowGet);
                return Json("null", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetAllPractices()
        {
            try
            {
                var Practices = db.Practices.OrderBy(p => p.PracticeID).ToList();
                if (Practices.Count > 0) return Json(Practices, JsonRequestBehavior.AllowGet);
                return Json("null", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetShiftTimings()
        {
            try
            {
                var Timings = db.ShiftTiming.OrderBy(t => t.STimingID).ToList();
                if (Timings.Count > 0) return Json(Timings, JsonRequestBehavior.AllowGet);
                return Json("null", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult SaveManager(Managers ObjManager)
        {
            try
            {
                var message = "";
                var Manager = db.Managers.Where(a => a.UserName == ObjManager.UserName).FirstOrDefault();
                if (Manager == null)
                {
                    ObjManager.Password = EncryptDecrypt.Encrypt(ObjManager.Password, true);

                    db.Managers.Add(ObjManager);

                    db.SaveChanges();

                    message = "success";
                }
                else
                {
                    message = "alreadyexisit";
                }

                return Json(message, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult SaveEmployee(Employees ObjEmployees)
        {
            try
            {
                var message = "";


                var Employees = db.Employees.Where(a => a.UserName == ObjEmployees.UserName).FirstOrDefault();
                if (Employees == null)
                {
                    ObjEmployees.Password = EncryptDecrypt.Encrypt(ObjEmployees.Password, true);
                    db.Employees.Add(ObjEmployees);
                    db.SaveChanges();
                    foreach (var item in ObjEmployees.Practices)
                    {
                        var ObjEmployeesPractice = new EmployeePractices();
                        ObjEmployeesPractice.EmployeeID = ObjEmployees.EmployeeID;
                        ObjEmployeesPractice.PracticesID = Convert.ToInt32(item);
                        db.EmployeePractices.Add(ObjEmployeesPractice);
                        db.SaveChanges();
                    }


                    message = "success";
                }
                else
                {
                    message = "alreadyexisit";
                }


                return Json(message, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetEmployeeDetailList()
        {
            try
            {
                var EmployeesDetail = (from u in db.Employees
                                       join r in db.Role on u.RoleID equals r.RoleID
                                       join t in db.ShiftTiming on u.ShiftTiming equals t.STimingID
                                       join d in db.Department on u.DepartmentID equals d.DepartmentID
                                       where u.RoleID == 2
                                       select new
                                       {
                                           u.EmployeeID,
                                           u.FirstName,
                                           u.LastName,
                                           u.UserName,
                                           u.Email,
                                           u.EmployeeNo,
                                           u.Designation,
                                           d.DepartmentID,
                                           d.DepartmentName,
                                           r.RoleName,
                                           t.Title,
                                           Status = u.IsActive ? "Active" : "In Active"
                                       }
                    ).AsEnumerable()
                    .Select(u => new
                    {
                        u.EmployeeID,
                        u.FirstName,
                        u.LastName,
                        u.UserName,
                        u.Email,
                        u.EmployeeNo,
                        u.Designation,
                        u.DepartmentID,
                        u.DepartmentName,
                        u.RoleName,
                        u.Title,
                        u.Status,
                        EUID = EncryptDecrypt.Encrypt(u.EmployeeID.ToString(), true)
                    }).ToList();
                if (EmployeesDetail.Count > 0) return Json(EmployeesDetail, JsonRequestBehavior.AllowGet);
                return Json("null", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetManagersDetailList()
        {
            try
            {
                var Managers = (from u in db.Managers
                                join r in db.Role on u.RoleID equals r.RoleID into RoleList
                                join t in db.ShiftTiming on u.ShiftTiming equals t.STimingID
                                from r in RoleList.DefaultIfEmpty()
                                where u.RoleID == 1
                                select new
                                {
                                    //      EmployeeID = "<button class='btn btn-info-outline ks-light ks-solid' ng-click="+ "EditEmployees("+ u.EmployeeID+")" +" >"+ u.EmployeeID+"</button>",
                                    //   EmployeeID = "<a class='btn btn-info' href=" + "/admin/Employees/editEmployees/" + u.EmployeeID + ">Action </a>",
                                    u.ManagerID,
                                    u.FirstName,
                                    u.LastName,
                                    u.UserName,
                                    u.Email,
                                    u.EmployeeNo,
                                    u.Designation,
                                    t.Title,
                                    r.RoleName,
                                    Status = u.IsActive ? "Active" : "In Active"
                                }
                    ).AsEnumerable()
                    .Select(u => new
                    {
                        u.ManagerID,
                        u.FirstName,
                        u.LastName,
                        u.UserName,
                        u.Email,
                        u.EmployeeNo,
                        u.Designation,
                        u.RoleName,
                        u.Title,
                        u.Status,
                        EMID = EncryptDecrypt.Encrypt(u.ManagerID.ToString(), true)
                    }).ToList();
                if (Managers.Count > 0) return Json(Managers, JsonRequestBehavior.AllowGet);
                return Json("null", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetEmployeeDetailByID(Employees ObjEmployee)
        {
            try
            {
                var Employees = db.Employees.Where(a => a.EmployeeID == ObjEmployee.EmployeeID).FirstOrDefault();
                Employees.Password = EncryptDecrypt.Decrypt(Employees.Password, true);


                if (Employees != null)
                    return Json(new
                    {
                        Employee = Employees,
                        Practices = (from ep in db.EmployeePractices
                                     join p in db.Practices on ep.PracticesID equals p.PracticeID
                                     where ep.EmployeeID == ObjEmployee.EmployeeID
                                     select new
                                     {
                                         p.PracticeID,
                                         p.PracticeName
                                     }
                                ).ToList()
                    }
                        , JsonRequestBehavior.AllowGet);
                return Json("null", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetManagerDetailByID(Managers ObjManager)
        {
            try
            {
                var Managers = db.Managers.Where(a => a.ManagerID == ObjManager.ManagerID).FirstOrDefault();
                Managers.Password = EncryptDecrypt.Decrypt(ObjManager.Password, true);


                if (Managers != null)
                    return Json(new
                    {
                        Manager = Managers
                    }
                        , JsonRequestBehavior.AllowGet);
                return Json("null", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult UpdateEmployees(Employees ObjEmployees)
        {
            try
            {
                ObjEmployees.Password = EncryptDecrypt.Encrypt(ObjEmployees.Password, true);
                db.Entry(ObjEmployees).State = EntityState.Modified;
                db.SaveChanges();

                db.Database.ExecuteSqlCommand("Delete from EmployeePractices Where EmployeeID=" +
                                              ObjEmployees.EmployeeID);
                if (ObjEmployees.Practices != null)
                    foreach (var item in ObjEmployees.Practices)
                    {
                        var objEmployeesPractice = new EmployeePractices();
                        objEmployeesPractice.EmployeeID = ObjEmployees.EmployeeID;
                        objEmployeesPractice.PracticesID = Convert.ToInt32(item);
                        db.EmployeePractices.Add(objEmployeesPractice);
                        db.SaveChanges();
                    }

                return Json("updated", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult UpdateManager(Managers ObjManager)
        {
            try
            {
                ObjManager.Password = EncryptDecrypt.Encrypt(ObjManager.Password, true);
                db.Entry(ObjManager).State = EntityState.Modified;
                db.SaveChanges();
                return Json("updated", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DeleteEmployees(Employees Employees)
        {
            try
            {
                db.Entry(Employees).State = EntityState.Modified;
                db.Employees.Remove(Employees);
                db.Database.ExecuteSqlCommand("Delete from EmployeePractices Where EmployeeID=" + Employees.EmployeeID);
                if (db.SaveChanges() == 1) return Json("deleted", JsonRequestBehavior.AllowGet);
                return Json("fails", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DeleteManager(Managers ObjManager)
        {
            try
            {
                db.Entry(ObjManager).State = EntityState.Modified;
                db.Managers.Remove(ObjManager);
                if (db.SaveChanges() == 1) return Json("deleted", JsonRequestBehavior.AllowGet);
                return Json("fails", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Practices Section

        public JsonResult GetPracticeCategories()
        {
            try
            {
                var Categories = db.PracticesCategories.OrderBy(p => p.CategoryID).ToList();
                if (Categories.Count > 0) return Json(Categories, JsonRequestBehavior.AllowGet);
                return Json("null", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SaveNewPractice(Practices Practices)
        {
            try
            {
                db.Practices.Add(Practices);
                if (db.SaveChanges() == 1) return Json("added", JsonRequestBehavior.AllowGet);
                return Json("fails", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetPracticeDetailList()
        {
            try
            {
                //var Practices = (from p in db.Practices
                //                 join pc in db.PracticesCategories on p.CategoryID equals pc.CategoryID
                //                 select new
                //                 {
                //                     p.PracticeID,
                //                     p.PracticeName,
                //                     pc.CategoryID,
                //                     pc.CategoryName
                //                 }
                //                 ).ToArray();
                var Practices = (from p in db.Practices
                                 join pc in db.PracticesCategories on p.CategoryID equals pc.CategoryID
                                 select new
                                 {
                                     p.PracticeID,
                                     p.PracticeName,
                                     pc.CategoryID,
                                     pc.CategoryName
                                 })
                    .AsEnumerable()
                    .Select(x => new
                    {
                        x.PracticeID,
                        x.PracticeName,
                        x.CategoryID,
                        x.CategoryName,
                        EPID = EncryptDecrypt.Encrypt(x.PracticeID.ToString(), true).ToString()
                    }).ToList();
                if (Practices.Count() > 0) return Json(Practices, JsonRequestBehavior.AllowGet);
                return Json("null", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetPracticeDetailByID(long PracticeID)
        {
            try
            {
                var Practices = db.Practices.Where(p => p.PracticeID == PracticeID).FirstOrDefault();
                if (Practices != null) return Json(Practices, JsonRequestBehavior.AllowGet);
                return Json("null", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult UpdatePractice(Practices ObjPractice)
        {
            try
            {
                db.Entry(ObjPractice).State = EntityState.Modified;
                db.SaveChanges();
                return Json("updated", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DeletePractice(Practices practices)
        {
            try
            {
                db.Entry(practices).State = EntityState.Modified;
                db.Practices.Remove(practices);
                if (db.SaveChanges() == 1) return Json("deleted", JsonRequestBehavior.AllowGet);
                return Json("fails", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        /* Notifications */
        public JsonResult GetAllNotifications(string filter = "")
        {
            try
            {
                var Notifications = new List<Notificatoins>();


                if (filter != "all")
                    Notifications = db.Notificatoins.Take(5).OrderByDescending(n => n.NotificationID).ToList();
                else
                    Notifications = db.Notificatoins.OrderByDescending(n => n.NotificationID).ToList();
                if (Notifications.Count > 0) return Json(Notifications, JsonRequestBehavior.AllowGet);
                return Json("null", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }


        ////// Check Employees Login Session

        public JsonResult CheckManagerLoginSession()
        {
            try
            {
                if (Session["ManagerID"] != null)
                {
                    if (Convert.ToInt64(Session["ManagerID"]) > 0)
                    {
                        var EmployeeID = Convert.ToInt64(Session["ManagerID"]);
                        var EmployeesInfo = db.Managers
                            .Where(u => u.ManagerID == EmployeeID && u.IsActive && u.RoleID == 1).FirstOrDefault();
                        return Json(EmployeesInfo, JsonRequestBehavior.AllowGet);
                    }

                    return Json("invalid", JsonRequestBehavior.AllowGet);
                }

                return Json("null", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }

        #region Departments

        public JsonResult SaveDepartmentDetails(Departments ObjDept)
        {
            try
            {
                if (ObjDept != null)
                {
                    if (ObjDept.DepartmentID <= 0)
                        db.Department.Add(ObjDept);
                    else
                        db.Entry(ObjDept).State = EntityState.Modified;
                    if (db.SaveChanges() == 1) return Json("success", JsonRequestBehavior.AllowGet);
                    return Json("fails", JsonRequestBehavior.AllowGet);
                }

                return Json("null", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetDepartmentDetails(Departments ObjDept)
        {
            try
            {
                if (ObjDept != null)
                {
                    var Department = db.Department.Find(ObjDept.DepartmentID);
                    if (Department != null) return Json(Department, JsonRequestBehavior.AllowGet);
                    return Json("null", JsonRequestBehavior.AllowGet);
                }

                return Json("null", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DeleteDepartment(Departments ObjDepartment)
        {
            try
            {
                db.Entry(ObjDepartment).State = EntityState.Modified;
                db.Department.Remove(ObjDepartment);
                if (db.SaveChanges() == 1) return Json("deleted", JsonRequestBehavior.AllowGet);
                return Json("fails", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }

        #endregion


        ///////////////////File upload////////////////////////*

        public JsonResult Upload()
        {
            try
            {
                if (Request.Files.Count > 0)
                {
                    var file = Request.Files[0];

                    if (file != null && file.ContentLength > 0)
                    {
                        var fileName = Path.GetFileName(file.FileName);
                        var path = Path.Combine(Server.MapPath("~/Uploads/"), fileName + ".png");
                        file.SaveAs(path);

                        return Json(new { msg = "success", path = file.FileName + ".png" }, JsonRequestBehavior.AllowGet);
                    }
                }

                return Json("failed", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }


        #region Reporting/ Details

        #region Departmental Data

        public JsonResult GetAssignmentDataByDepartment(Filters ObjFilter)
        {
            try
            {
                string Condition = "", Query = "";
                long DateTime;
                DateTime = ObjFilter.FilterDateTime.Month;
                // DateTime FilterDate = Convert.ToDateTime(ObjFilter.FilterDateTime);


                if (ObjFilter.AssType == 1)
                {
                    if (ObjFilter.Department.DepartmentID == 1)
                    {
                        Condition += "    C.CredentialingDateTime = '" +
                                     ObjFilter.FilterDateTime.ToString("yyyy-MM-dd 00:00:00.000") + "'";
                    }
                    else if (ObjFilter.Department.DepartmentID == 2)
                    {
                        Condition += "    PatientHelpDeskDateTime = '" +
                                     ObjFilter.FilterDateTime.ToString("yyyy-MM-dd 00:00:00.000") + "'";
                        ;
                    }
                    else
                    {
                        Condition += " And   AssignmentDateTime = '" +
                                     ObjFilter.FilterDateTime.ToString("yyyy-MM-dd 00:00:00.000") + "'";
                        ;
                    }
                }


                else if (ObjFilter.AssType == 2)
                {
                    if (ObjFilter.Department.DepartmentID == 1)
                    {
                        Condition += "   DATEPART(month, CredentialingDateTime) = '" + ObjFilter.FilterDateTime.Month +
                                     "'";
                        ;
                    }
                    else if (ObjFilter.Department.DepartmentID == 2)
                    {
                        Condition += " And  DATEPART(month , PatientHelpDeskDateTime) = '" +
                                     ObjFilter.FilterDateTime.Month + "'";
                        ;
                    }
                    else
                    {
                        Condition += " And  DATEPART( month , AssignmentDateTime) = '" +
                                     ObjFilter.FilterDateTime.Month + "'";
                        ;
                    }
                }
                else if (ObjFilter.AssType == 3)
                {
                    if (ObjFilter.Department.DepartmentID == 1)
                    {
                        Condition += "   DATEPART(month, CredentialingDateTime) = '" + ObjFilter.FilterDateTime.Month +
                                     "'  And  DATEPART(year, CredentialingDateTime) = '" +
                                     ObjFilter.FilterDateTime.Year + "'";
                        ;
                    }
                    else if (ObjFilter.Department.DepartmentID == 2)
                    {
                        Condition += " And  DATEPART(month , PatientHelpDeskDateTime) = '" +
                                     ObjFilter.FilterDateTime.Month +
                                     "'  And  DATEPART(year, PatientHelpDeskDateTime) = '" +
                                     ObjFilter.FilterDateTime.Year + "'";
                        ;
                    }
                    else
                    {
                        Condition += " And  DATEPART( month , AssignmentDateTime) = '" +
                                     ObjFilter.FilterDateTime.Month + "'  And  DATEPART(year, AssignmentDateTime) = '" +
                                     ObjFilter.FilterDateTime.Year + "'";
                        ;
                    }
                }

                switch (ObjFilter.Department.DepartmentID)
                {
                    case 1:
                        {
                            Query = @"SELECT  
                                    ISNULL( SUM(C.CredentialingCalls) , 0 ) CredentialingCalls,
                                    ISNULL( SUM(C.CredentialingDocuments) , 0 ) CredentialingDocuments,
                                    ISNULL( SUM(C.EDICalls) , 0 ) EDICalls,
                                    ISNULL(SUM(C.EDISetup) , 0 ) EDISetup,
                                    ISNULL( SUM(C.ERACalls) , 0 ) ERACalls,
									ISNULL( SUM(C.ERASetup) , 0 ) ERASetup,
									ISNULL( SUM(C.Emails) , 0 ) Emails,
                                    ISNULL( SUM(C.EnrollmentTrackingSheet) , 0 ) EnrollmentTrackingSheet,
									ISNULL( SUM(C.WebLogins) , 0 ) WebLogins,
								    ISNULL( SUM(C.EFTCalls) , 0 ) EFTCalls,
                                    ISNULL( SUM(C.EFTSetup) , 0 ) EFTSetup
                                    FROM Credentialing C WHERE  " + Condition;
                            var Result = db.Database.SqlQuery<VMCredentialingCount>(Query).FirstOrDefault();
                            if (Result != null) return Json(Result, JsonRequestBehavior.AllowGet);
                            break;
                        }
                    case 2:
                        {
                            Query = @"SELECT  
                                    ISNULL(SUM(AVM),0) AVM
                                      ,ISNULL(SUM(Emails),0) Emails
                                      ,ISNULL(SUM(IC),0) IC
                                      ,ISNULL(SUM(ICIC),0) ICIC
                                      ,ISNULL(SUM(ICPC),0) ICPC
                                      ,ISNULL(SUM([IS]),0) [IS]
                                      ,ISNULL(SUM(LVM),0) LVM
                                      ,ISNULL(SUM(OGIC),0) OGIC
                                      ,ISNULL(SUM(OGPC),0) OGPC
                                      ,ISNULL(SUM(PC),0) PC
                                      ,ISNULL(SUM(PS),0) PS
                                      ,ISNULL(SUM(Pendings),0) Pendings
                                        FROM PatientHelpDesk P  WHERE  " + Condition;
                            var Result = db.Database.SqlQuery<AssignmentsCounts>(Query).FirstOrDefault();
                            if (Result != null) return Json(Result, JsonRequestBehavior.AllowGet);
                            break;
                        }
                    case 3:
                        {
                            Query = @"SELECT  
                                    ISNULL( SUM(A.EnteredClaims) , 0 ) EnteredClaims,
                                    ISNULL( SUM(A.Eligibility) , 0 ) Eligibility,
                                    ISNULL( SUM(A.PF_eBridge) , 0 ) PF_eBridge,
                                    ISNULL( SUM(A.RejectionsEmdeon) , 0 ) RejectionsEmdeon,
                                    ISNULL( SUM(A.RejectionsAvaility) , 0 ) RejectionsAvaility,
									ISNULL( SUM(A.ERADenials) , 0 ) ERADenials,
								    ISNULL( SUM(A.ERAUnmatched) , 0 ) ERAUnmatched,
                                    ISNULL( SUM(A.Appeals) , 0 ) Appeals,
									ISNULL( SUM(A.Calls) , 0 ) Calls,
									ISNULL( SUM(A.AgingClaims) , 0 ) AgingClaims,
									ISNULL( SUM(A.Emails) , 0 ) Emails ,
                                    ISNULL( SUM(A.PTStatements) , 0 ) PTStatements,
                                    ISNULL( SUM(A.EOBpayment) , 0 ) EOBpayment
                                    FROM Assignments A WHERE A.DepartmentID = " + ObjFilter.Department.DepartmentID +
                                    " " + Condition;
                            var Result = db.Database.SqlQuery<AssignmentsCounts>(Query).FirstOrDefault();
                            if (Result != null) return Json(Result, JsonRequestBehavior.AllowGet);
                            break;
                        }
                    case 4:
                        {
                            Query = @"SELECT 
                                        ISNULL( SUM(A.PF_eBridge) , 0 ) PF_eBridge,
                                        ISNULL( SUM(A.Rejections) , 0 ) Rejections,
									    ISNULL( SUM(A.ERADenials) , 0 ) ERADenials,
								        ISNULL( SUM(A.ERAUnmatched) , 0 ) ERAUnmatched,
                                        ISNULL( SUM(A.PTLResponses) , 0 ) PTLResponses,
                                        ISNULL( SUM(A.Appeals) , 0 ) Appeals,
									    ISNULL( SUM(A.Calls) , 0 ) Calls,
									    ISNULL( SUM(A.AgingClaims) , 0 ) AgingClaims,
									    ISNULL( SUM(A.Emails) , 0 ) Emails ,
                                        ISNULL( SUM(A.PTStatements) , 0 ) PTStatements
                                        FROM Assignments A  WHERE A.DepartmentID = " +
                                    ObjFilter.Department.DepartmentID + " " + Condition;
                            var Result = db.Database.SqlQuery<AssignmentsCounts>(Query).FirstOrDefault();
                            if (Result != null) return Json(Result, JsonRequestBehavior.AllowGet);
                            break;
                        }
                    case 5:
                        {
                            Query = @"SELECT
                                        ISNULL( SUM(A.Appeals) , 0 ) Appeals,
									    ISNULL( SUM(A.Calls) , 0 ) Calls,
                                        ISNULL( SUM(A.AgingClaims) , 0 ) AgingClaims,
                                        ISNULL( SUM(A.Emails) , 0 ) Emails ,
                                        ISNULL( SUM(A.Others ) , 0 ) Others  
                                        FROM Assignments A  WHERE A.DepartmentID = " +
                                    ObjFilter.Department.DepartmentID + " " + Condition;
                            var Result = db.Database.SqlQuery<AssignmentsCounts>(Query).FirstOrDefault();
                            if (Result != null) return Json(Result, JsonRequestBehavior.AllowGet);
                            break;
                        }
                    case 6:
                        {
                            Query = @"SELECT  
                                        ISNULL( SUM(A.Calls) , 0 ) Calls,
									    ISNULL( SUM(A.WebVerification ) , 0 ) WebVerification ,
                                        ISNULL( SUM(A.FaxedVerification ) , 0 ) FaxedVerification ,
                                        ISNULL( SUM(A.EmailVerification ) , 0 ) EmailVerification ,
                                        ISNULL( SUM(A.Others ) , 0 ) Others  
                                        FROM Assignments A  WHERE A.DepartmentID = " +
                                    ObjFilter.Department.DepartmentID + " " + Condition;
                            var Result = db.Database.SqlQuery<AssignmentsCounts>(Query).FirstOrDefault();
                            if (Result != null) return Json(Result, JsonRequestBehavior.AllowGet);
                            break;
                        }
                    case 7:
                        {
                            Query = @"SELECT  
                                        ISNULL( SUM(A.Eligibility ) , 0 ) Eligibility ,
									    ISNULL( SUM(A.ReceivedBills  ) , 0 ) ReceivedBills ,
                                        ISNULL( SUM(A.EnteredClaims  ) , 0 ) EnteredClaims  ,
                                        ISNULL( SUM(A.PendingClaims  ) , 0 ) PendingClaims ,
                                        ISNULL( SUM(A.PaidClaims  ) , 0 ) PaidClaims , 
                                        ISNULL( SUM(A.Eligibility ) , 0 ) Eligibility  ,
									    ISNULL( SUM(A.PreparedEncounters   ) , 0 ) PreparedEncounters ,
                                        ISNULL( SUM(A.MissingLog   ) , 0 ) MissingLog  ,
                                        ISNULL( SUM(A.Emails   ) , 0 ) Emails ,
                                        ISNULL( SUM(A.QA   ) , 0 ) QA , 
                                        ISNULL( SUM(A.BillingAnalysis    ) , 0 ) BillingAnalysis  
                                        FROM Assignments A  WHERE A.DepartmentID = " +
                                    ObjFilter.Department.DepartmentID + " " + Condition;

                            var Result = db.Database.SqlQuery<AssignmentsCounts>(Query).FirstOrDefault();
                            if (Result != null) return Json(Result, JsonRequestBehavior.AllowGet);
                            break;
                        }
                    case 8:
                        {
                            Query = @"SELECT 
                                        ISNULL( SUM(A.Multiple) , 0 ) Multiple,
									    ISNULL( SUM(A.NoPatientBill ) , 0 ) NoPatientBill ,
                                        ISNULL( SUM(A.UVTotalClaims ) , 0 ) UVTotalClaims ,
                                        ISNULL( SUM(A.Done ) , 0 ) Done ,
                                        ISNULL( SUM(A.PendingClaims ) , 0 ) PendingClaims  
                                        FROM Assignments A  WHERE A.DepartmentID = " +
                                    ObjFilter.Department.DepartmentID + " " + Condition;
                            var Result = db.Database.SqlQuery<AssignmentsCounts>(Query).FirstOrDefault();
                            if (Result != null) return Json(Result, JsonRequestBehavior.AllowGet);
                            break;
                        }
                    case 9:
                        {
                            Query = @"SELECT  
                                        ISNULL( SUM(A.PF_eBridge) , 0 ) PF_eBridge,
                                        ISNULL( SUM(A.Rejections) , 0 ) Rejections,
									    ISNULL( SUM(A.ERADenials) , 0 ) ERADenials,
								        ISNULL( SUM(A.ERAUnmatched) , 0 ) ERAUnmatched,
                                        ISNULL( SUM(A.Appeals) , 0 ) Appeals,
                                        ISNULL( SUM(A.ManuallyAdjustments ) , 0 ) ManuallyAdjustments ,
                                        ISNULL( SUM(A.AgingClaims) , 0 ) AgingClaims,
									    ISNULL( SUM(A.BillingETC ) , 0 ) BillingETC ,
                                        ISNULL( SUM(A.Emails) , 0 ) Emails ,
                                        ISNULL( SUM(A.PTStatements) , 0 ) PTStatements,
                                        ISNULL( SUM(A.VerificationClaims ) , 0 ) VerificationClaims  

                                        FROM Assignments A  WHERE A.DepartmentID = " +
                                    ObjFilter.Department.DepartmentID + " " + Condition;
                            var Result = db.Database.SqlQuery<AssignmentsCounts>(Query).FirstOrDefault();
                            if (Result != null) return Json(Result, JsonRequestBehavior.AllowGet);
                            break;
                        }
                    default:
                        {
                            return Json("exception", JsonRequestBehavior.AllowGet);
                        }
                }

                return Json("null", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetFinancialsDataByDepartment(Filters ObjFilter)
        {
            try
            {
                string Condition = "", Query = "";
                long DateTime;
                DateTime = ObjFilter.FilterDateTime.Month;
                // DateTime FilterDate = Convert.ToDateTime(ObjFilter.FilterDateTime);


                if (ObjFilter.AssType == 1)
                {
                    Condition += " AND F.FinancialDateTime = '" +
                                 ObjFilter.FilterDateTime.ToString("yyyy-MM-dd 00:00:00.000") + "'";
                }
                else if (ObjFilter.AssType == 2)
                {
                    Condition += " AND DATEPART(month, F.FinancialDateTime) = '" + ObjFilter.FilterDateTime.Month + "'";
                    ;
                }
                else if (ObjFilter.AssType == 3)
                {
                    Condition += " AND DATEPART(month, FinancialDateTime) = '" + ObjFilter.FilterDateTime.Month +
                                 "'  And  DATEPART(year, FinancialDateTime) = '" + ObjFilter.FilterDateTime.Year + "'";
                    ;
                }

                if (ObjFilter.Department.DepartmentID == 9)
                {
                    Query = @"SELECT 
                                   ISNULL(SUM(Rejections),0) Rejections
                                  , ISNULL(SUM(ERADenials),0) ERADenials
                                  , ISNULL(SUM(ERAUnmatched),0) ERAUnmatched
                                  , ISNULL(SUM(Appeals),0) Appeals
                                  , ISNULL(SUM(ManuallyAdjustments),0) ManuallyAdjustments
                                  , ISNULL(SUM(AgingClaims),0) AgingClaims
                                  , ISNULL(SUM(BillingETC),0) BillingETC
                                  , ISNULL(SUM(Emails),0) Emails
                                  , ISNULL(SUM(PTStatements),0) PTStatements
                                  , ISNULL(SUM(VerificationClaims),0) VerificationClaims
                                  , ISNULL(SUM(PF_eBridge),0) PF_eBridge
                                FROM
                                    PPFinancials F
                                WHERE
                                    F.DepartmentID =" + ObjFilter.Department.DepartmentID + " " + Condition;
                }
                else
                {
                    if (ObjFilter.Department.DepartmentID == 3)
                        Query = @"SELECT 
                                    ISNULL(SUM(F.DailyChargesECW), 0) DailyChargesECW,
                                    ISNULL(SUM(F.DailyPaymentseTeClinic), 0) DailyPaymentseTeClinic,
                                    ISNULL(SUM(F.DailyPaymentsECW), 0) DailyPaymentsECW,
                                    ISNULL(SUM(F.MTDCharges), 0) MTDCharges,
                                    ISNULL(SUM(F.MTDPaymentseTeClinic), 0) MTDPaymentseTeClinic,
                                    ISNULL(SUM(F.MTDPaymentsECW), 0) MTDPaymentsECW,
                                    ISNULL(SUM(F.AgingAmounteTeClinic), 0) AgingAmounteTeClinic,
                                    ISNULL(SUM(F.AgingAmounteCW), 0) AgingAmounteCW
                                FROM
                                    Financial F
                                WHERE
                                    F.DepartmentID =" + ObjFilter.Department.DepartmentID + " " + Condition;
                    else if (ObjFilter.Department.DepartmentID == 7)
                        Query = @"SELECT 
                                   ISNULL(SUM(EnteredChargeI),0) EnteredChargeI
                                  , ISNULL(SUM(EnteredChargeP),0) EnteredChargeP
                                  
                                FROM
                                    Financial F
                                WHERE
                                    F.DepartmentID =" + ObjFilter.Department.DepartmentID + " " + Condition;
                    else
                        Query = @"SELECT
                                   ISNULL(SUM(F.DailyCharges),0) DailyCharges,
                                    ISNULL(SUM(F.DailyPayments),0) DailyPayments,
                                   ISNULL(SUM(F.MTDCharges),0) MTDCharges,
                                  
                                   ISNULL(SUM(F.MTDPayments),0) MTDPayments,
                                   ISNULL(SUM(F.AgingAmount),0) AgingAmount
                                   FROM 
                                        Financial F 
                                    WHERE 
                                     F.DepartmentID =" + ObjFilter.Department.DepartmentID + " " + Condition;
                }

                var Result = db.Database.SqlQuery<VMFinancials>(Query).FirstOrDefault();
                if (Result != null) return Json(Result, JsonRequestBehavior.AllowGet);
                return Json("null", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetAgingsDataDataByDepartment(Filters ObjFilter)
        {
            try
            {
                string Condition = "", Query = "";
                long DateTime;
                DateTime = ObjFilter.FilterDateTime.Month;
                // DateTime FilterDate = Convert.ToDateTime(ObjFilter.FilterDateTime);


                if (ObjFilter.AssType == 1)
                {
                    Condition += " AND F.AgingDateTime = '" +
                                 ObjFilter.FilterDateTime.ToString("yyyy-MM-dd 00:00:00.000") + "'";
                }
                else if (ObjFilter.AssType == 2)
                {
                    Condition += " AND DATEPART(month, F.AgingDateTime) = '" + ObjFilter.FilterDateTime.Month + "'";
                }
                else if (ObjFilter.AssType == 3)
                {
                    Condition += " AND DATEPART(month, AgingDateTime) = '" + ObjFilter.FilterDateTime.Month +
                                 "'  And  DATEPART(year, AgingDateTime) = '" + ObjFilter.FilterDateTime.Year + "'";
                }


                Query = @"SELECT 
                                   ISNULL(SUM(A_30),0) A_30
                                  ,ISNULL(SUM(A_60),0) A_60
                                  ,ISNULL(SUM(A_90),0) A_90
                                  ,ISNULL(SUM(A_120),0) A_120
                                  ,ISNULL(SUM(A_120_up),0) A_120_up
                                  ,ISNULL(SUM(Issues_Of_120_Up_Claims),0) Issues_Of_120_Up_Claims
                                FROM
                                    Agings F
                                WHERE
                                    F.DepartmentID =" + ObjFilter.Department.DepartmentID + " " + Condition;


                var Result = db.Database.SqlQuery<VMAgings>(Query).FirstOrDefault();
                if (Result != null) return Json(Result, JsonRequestBehavior.AllowGet);
                return Json("null", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }

        #endregion
        #region Employyeee performance
        public JsonResult GetAssignmentDataOfEmployee(Filters ObjFilter)
        {
            try
            {
                string Condition = "", Query = "";
                long DateTime;
                DateTime = ObjFilter.FilterDateTime.Month;
                // DateTime FilterDate = Convert.ToDateTime(ObjFilter.FilterDateTime);


                if (ObjFilter.AssType == 1)
                {
                    if (ObjFilter.Employee.DepartmentID == 1)
                    {
                        Condition += "    C.CredentialingDateTime = '" +
                                     ObjFilter.FilterDateTime.ToString("yyyy-MM-dd 00:00:00.000") + "' AND EmployeeID = " + ObjFilter.Employee.EmployeeID;
                    }
                    else if (ObjFilter.Employee.DepartmentID == 2)
                    {
                        Condition += "    PatientHelpDeskDateTime = '" +
                                     ObjFilter.FilterDateTime.ToString("yyyy-MM-dd 00:00:00.000") + "' AND EmployeeID = " + ObjFilter.Employee.EmployeeID;
                        ;
                    }
                    else
                    {
                        Condition += " And   AssignmentDateTime = '" +
                                     ObjFilter.FilterDateTime.ToString("yyyy-MM-dd 00:00:00.000") + "'  AND EmployeeID = " + ObjFilter.Employee.EmployeeID;
                        ;
                    }
                }


                else if (ObjFilter.AssType == 2)
                {
                    if (ObjFilter.Employee.DepartmentID == 1)
                    {
                        Condition += "   DATEPART(month, CredentialingDateTime) = '" + ObjFilter.FilterDateTime.Month +
                                     "' AND EmployeeID = " + ObjFilter.Employee.EmployeeID;
                        ;
                    }
                    else if (ObjFilter.Employee.DepartmentID == 2)
                    {
                        Condition += " And  DATEPART(month , PatientHelpDeskDateTime) = '" +
                                     ObjFilter.FilterDateTime.Month + "' AND EmployeeID = " + ObjFilter.Employee.EmployeeID;
                        ;
                    }
                    else
                    {
                        Condition += " And  DATEPART( month , AssignmentDateTime) = '" +
                                     ObjFilter.FilterDateTime.Month + "' AND EmployeeID = " + ObjFilter.Employee.EmployeeID;
                        ;
                    }
                }
                else if (ObjFilter.AssType == 3)
                {
                    if (ObjFilter.Employee.DepartmentID == 1)
                    {
                        Condition += "   DATEPART(month, CredentialingDateTime) = '" + ObjFilter.FilterDateTime.Month +
                                     "'  And  DATEPART(year, CredentialingDateTime) = '" +
                                     ObjFilter.FilterDateTime.Year + "' AND EmployeeID = " + ObjFilter.Employee.EmployeeID;
                        ;
                    }
                    else if (ObjFilter.Employee.DepartmentID == 2)
                    {
                        Condition += " And  DATEPART(month , PatientHelpDeskDateTime) = '" +
                                     ObjFilter.FilterDateTime.Month +
                                     "'  And  DATEPART(year, PatientHelpDeskDateTime) = '" +
                                     ObjFilter.FilterDateTime.Year + "' AND EmployeeID = " + ObjFilter.Employee.EmployeeID;
                        ;
                    }
                    else
                    {
                        Condition += " And  DATEPART( month , AssignmentDateTime) = '" +
                                     ObjFilter.FilterDateTime.Month + "'  And  DATEPART(year, AssignmentDateTime) = '" +
                                     ObjFilter.FilterDateTime.Year + "' AND EmployeeID = " + ObjFilter.Employee.EmployeeID;
                        ;
                    }
                }

                switch (ObjFilter.Employee.DepartmentID)
                {
                    case 1:
                        {
                            Query = @"SELECT  
                                    ISNULL( SUM(C.CredentialingCalls) , 0 ) CredentialingCalls,
                                    ISNULL( SUM(C.CredentialingDocuments) , 0 ) CredentialingDocuments,
                                    ISNULL( SUM(C.EDICalls) , 0 ) EDICalls,
                                    ISNULL(SUM(C.EDISetup) , 0 ) EDISetup,
                                    ISNULL( SUM(C.ERACalls) , 0 ) ERACalls,
									ISNULL( SUM(C.ERASetup) , 0 ) ERASetup,
									ISNULL( SUM(C.Emails) , 0 ) Emails,
                                    ISNULL( SUM(C.EnrollmentTrackingSheet) , 0 ) EnrollmentTrackingSheet,
									ISNULL( SUM(C.WebLogins) , 0 ) WebLogins,
								    ISNULL( SUM(C.EFTCalls) , 0 ) EFTCalls,
                                    ISNULL( SUM(C.EFTSetup) , 0 ) EFTSetup
                                    FROM Credentialing C WHERE  " + Condition;
                            var Result = db.Database.SqlQuery<VMCredentialingCount>(Query).FirstOrDefault();
                            if (Result != null) return Json(Result, JsonRequestBehavior.AllowGet);
                            break;
                        }
                    case 2:
                        {
                            Query = @"SELECT  
                                    ISNULL(SUM(AVM),0) AVM
                                      ,ISNULL(SUM(Emails),0) Emails
                                      ,ISNULL(SUM(IC),0) IC
                                      ,ISNULL(SUM(ICIC),0) ICIC
                                      ,ISNULL(SUM(ICPC),0) ICPC
                                      ,ISNULL(SUM([IS]),0) [IS]
                                      ,ISNULL(SUM(LVM),0) LVM
                                      ,ISNULL(SUM(OGIC),0) OGIC
                                      ,ISNULL(SUM(OGPC),0) OGPC
                                      ,ISNULL(SUM(PC),0) PC
                                      ,ISNULL(SUM(PS),0) PS
                                      ,ISNULL(SUM(Pendings),0) Pendings
                                        FROM PatientHelpDesk P  WHERE  " + Condition;
                            var Result = db.Database.SqlQuery<AssignmentsCounts>(Query).FirstOrDefault();
                            if (Result != null) return Json(Result, JsonRequestBehavior.AllowGet);
                            break;
                        }
                    case 3:
                        {
                            Query = @"SELECT  
                                    ISNULL( SUM(A.EnteredClaims) , 0 ) EnteredClaims,
                                    ISNULL( SUM(A.Eligibility) , 0 ) Eligibility,
                                    ISNULL( SUM(A.PF_eBridge) , 0 ) PF_eBridge,
                                    ISNULL( SUM(A.RejectionsEmdeon) , 0 ) RejectionsEmdeon,
                                    ISNULL( SUM(A.RejectionsAvaility) , 0 ) RejectionsAvaility,
									ISNULL( SUM(A.ERADenials) , 0 ) ERADenials,
								    ISNULL( SUM(A.ERAUnmatched) , 0 ) ERAUnmatched,
                                    ISNULL( SUM(A.Appeals) , 0 ) Appeals,
									ISNULL( SUM(A.Calls) , 0 ) Calls,
									ISNULL( SUM(A.AgingClaims) , 0 ) AgingClaims,
									ISNULL( SUM(A.Emails) , 0 ) Emails ,
                                    ISNULL( SUM(A.PTStatements) , 0 ) PTStatements,
                                    ISNULL( SUM(A.EOBpayment) , 0 ) EOBpayment
                                    FROM Assignments A WHERE A.DepartmentID = " + ObjFilter.Employee.DepartmentID +
                                    " " + Condition;
                            var Result = db.Database.SqlQuery<AssignmentsCounts>(Query).FirstOrDefault();
                            if (Result != null) return Json(Result, JsonRequestBehavior.AllowGet);
                            break;
                        }
                    case 4:
                        {
                            Query = @"SELECT 
                                        ISNULL( SUM(A.PF_eBridge) , 0 ) PF_eBridge,
                                        ISNULL( SUM(A.Rejections) , 0 ) Rejections,
									    ISNULL( SUM(A.ERADenials) , 0 ) ERADenials,
								        ISNULL( SUM(A.ERAUnmatched) , 0 ) ERAUnmatched,
                                        ISNULL( SUM(A.PTLResponses) , 0 ) PTLResponses,
                                        ISNULL( SUM(A.Appeals) , 0 ) Appeals,
									    ISNULL( SUM(A.Calls) , 0 ) Calls,
									    ISNULL( SUM(A.AgingClaims) , 0 ) AgingClaims,
									    ISNULL( SUM(A.Emails) , 0 ) Emails ,
                                        ISNULL( SUM(A.PTStatements) , 0 ) PTStatements
                                        FROM Assignments A  WHERE A.DepartmentID = " +
                                    ObjFilter.Employee.DepartmentID + " " + Condition;
                            var Result = db.Database.SqlQuery<AssignmentsCounts>(Query).FirstOrDefault();
                            if (Result != null) return Json(Result, JsonRequestBehavior.AllowGet);
                            break;
                        }
                    case 5:
                        {
                            Query = @"SELECT
                                        ISNULL( SUM(A.Appeals) , 0 ) Appeals,
									    ISNULL( SUM(A.Calls) , 0 ) Calls,
                                        ISNULL( SUM(A.AgingClaims) , 0 ) AgingClaims,
                                        ISNULL( SUM(A.Emails) , 0 ) Emails ,
                                        ISNULL( SUM(A.Others ) , 0 ) Others  
                                        FROM Assignments A  WHERE A.DepartmentID = " +
                                    ObjFilter.Employee.DepartmentID + " " + Condition;
                            var Result = db.Database.SqlQuery<AssignmentsCounts>(Query).FirstOrDefault();
                            if (Result != null) return Json(Result, JsonRequestBehavior.AllowGet);
                            break;
                        }
                    case 6:
                        {
                            Query = @"SELECT  
                                        ISNULL( SUM(A.Calls) , 0 ) Calls,
									    ISNULL( SUM(A.WebVerification ) , 0 ) WebVerification ,
                                        ISNULL( SUM(A.FaxedVerification ) , 0 ) FaxedVerification ,
                                        ISNULL( SUM(A.EmailVerification ) , 0 ) EmailVerification ,
                                        ISNULL( SUM(A.Others ) , 0 ) Others  
                                        FROM Assignments A  WHERE A.DepartmentID = " +
                                    ObjFilter.Employee.DepartmentID + " " + Condition;
                            var Result = db.Database.SqlQuery<AssignmentsCounts>(Query).FirstOrDefault();
                            if (Result != null) return Json(Result, JsonRequestBehavior.AllowGet);
                            break;
                        }
                    case 7:
                        {
                            Query = @"SELECT  
                                        ISNULL( SUM(A.Eligibility ) , 0 ) Eligibility ,
									    ISNULL( SUM(A.ReceivedBills  ) , 0 ) ReceivedBills ,
                                        ISNULL( SUM(A.EnteredClaims  ) , 0 ) EnteredClaims  ,
                                        ISNULL( SUM(A.PendingClaims  ) , 0 ) PendingClaims ,
                                        ISNULL( SUM(A.PaidClaims  ) , 0 ) PaidClaims , 
                                        ISNULL( SUM(A.Eligibility ) , 0 ) Eligibility  ,
									    ISNULL( SUM(A.PreparedEncounters   ) , 0 ) PreparedEncounters ,
                                        ISNULL( SUM(A.MissingLog   ) , 0 ) MissingLog  ,
                                        ISNULL( SUM(A.Emails   ) , 0 ) Emails ,
                                        ISNULL( SUM(A.QA   ) , 0 ) QA , 
                                        ISNULL( SUM(A.BillingAnalysis    ) , 0 ) BillingAnalysis  
                                        FROM Assignments A  WHERE A.DepartmentID = " +
                                    ObjFilter.Employee.DepartmentID + " " + Condition;

                            var Result = db.Database.SqlQuery<AssignmentsCounts>(Query).FirstOrDefault();
                            if (Result != null) return Json(Result, JsonRequestBehavior.AllowGet);
                            break;
                        }
                    case 8:
                        {
                            Query = @"SELECT 
                                        ISNULL( SUM(A.Multiple) , 0 ) Multiple,
									    ISNULL( SUM(A.NoPatientBill ) , 0 ) NoPatientBill ,
                                        ISNULL( SUM(A.UVTotalClaims ) , 0 ) UVTotalClaims ,
                                        ISNULL( SUM(A.Done ) , 0 ) Done ,
                                        ISNULL( SUM(A.PendingClaims ) , 0 ) PendingClaims  
                                        FROM Assignments A  WHERE A.DepartmentID = " +
                                    ObjFilter.Employee.DepartmentID + " " + Condition;
                            var Result = db.Database.SqlQuery<AssignmentsCounts>(Query).FirstOrDefault();
                            if (Result != null) return Json(Result, JsonRequestBehavior.AllowGet);
                            break;
                        }
                    case 9:
                        {
                            Query = @"SELECT  
                                        ISNULL( SUM(A.PF_eBridge) , 0 ) PF_eBridge,
                                        ISNULL( SUM(A.Rejections) , 0 ) Rejections,
									    ISNULL( SUM(A.ERADenials) , 0 ) ERADenials,
								        ISNULL( SUM(A.ERAUnmatched) , 0 ) ERAUnmatched,
                                        ISNULL( SUM(A.Appeals) , 0 ) Appeals,
                                        ISNULL( SUM(A.ManuallyAdjustments ) , 0 ) ManuallyAdjustments ,
                                        ISNULL( SUM(A.AgingClaims) , 0 ) AgingClaims,
									    ISNULL( SUM(A.BillingETC ) , 0 ) BillingETC ,
                                        ISNULL( SUM(A.Emails) , 0 ) Emails ,
                                        ISNULL( SUM(A.PTStatements) , 0 ) PTStatements,
                                        ISNULL( SUM(A.VerificationClaims ) , 0 ) VerificationClaims  

                                        FROM Assignments A  WHERE A.DepartmentID = " +
                                    ObjFilter.Employee.DepartmentID + " " + Condition;
                            var Result = db.Database.SqlQuery<AssignmentsCounts>(Query).FirstOrDefault();
                            if (Result != null) return Json(Result, JsonRequestBehavior.AllowGet);
                            break;
                        }
                    default:
                        {
                            return Json("exception", JsonRequestBehavior.AllowGet);
                        }
                }

                return Json("null", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetFinancialDataOfEmployee(Filters ObjFilter)
        {
            try
            {
                string Condition = "", Query = "";
                long DateTime;
                DateTime = ObjFilter.FilterDateTime.Month;
                // DateTime FilterDate = Convert.ToDateTime(ObjFilter.FilterDateTime);


                if (ObjFilter.AssType == 1)
                {
                    Condition += " AND F.FinancialDateTime = '" +
                                 ObjFilter.FilterDateTime.ToString("yyyy-MM-dd 00:00:00.000") + "' AND EmployeeID = " + ObjFilter.Employee.EmployeeID;
                }
                else if (ObjFilter.AssType == 2)
                {
                    Condition += " AND DATEPART(month, F.FinancialDateTime) = '" + ObjFilter.FilterDateTime.Month + "' AND EmployeeID = " + ObjFilter.Employee.EmployeeID;
                    ;
                }
                else if (ObjFilter.AssType == 3)
                {
                    Condition += " AND DATEPART(month, FinancialDateTime) = '" + ObjFilter.FilterDateTime.Month +
                                 "'  And  DATEPART(year, FinancialDateTime) = '" + ObjFilter.FilterDateTime.Year + "' AND EmployeeID = " + ObjFilter.Employee.EmployeeID;
                    ;
                }

                if (ObjFilter.Department.DepartmentID == 9)
                {
                    Query = @"SELECT 
                                   ISNULL(SUM(Rejections),0) Rejections
                                  , ISNULL(SUM(ERADenials),0) ERADenials
                                  , ISNULL(SUM(ERAUnmatched),0) ERAUnmatched
                                  , ISNULL(SUM(Appeals),0) Appeals
                                  , ISNULL(SUM(ManuallyAdjustments),0) ManuallyAdjustments
                                  , ISNULL(SUM(AgingClaims),0) AgingClaims
                                  , ISNULL(SUM(BillingETC),0) BillingETC
                                  , ISNULL(SUM(Emails),0) Emails
                                  , ISNULL(SUM(PTStatements),0) PTStatements
                                  , ISNULL(SUM(VerificationClaims),0) VerificationClaims
                                  , ISNULL(SUM(PF_eBridge),0) PF_eBridge
                                FROM
                                    PPFinancials F
                                WHERE
                                    F.DepartmentID =" + ObjFilter.Employee.DepartmentID + " " + Condition;
                }
                else
                {
                    if (ObjFilter.Department.DepartmentID == 3)
                        Query = @"SELECT 
                                    ISNULL(SUM(F.DailyChargesECW), 0) DailyChargesECW,
                                    ISNULL(SUM(F.DailyPaymentseTeClinic), 0) DailyPaymentseTeClinic,
                                    ISNULL(SUM(F.DailyPaymentsECW), 0) DailyPaymentsECW,
                                    ISNULL(SUM(F.MTDCharges), 0) MTDCharges,
                                    ISNULL(SUM(F.MTDPaymentseTeClinic), 0) MTDPaymentseTeClinic,
                                    ISNULL(SUM(F.MTDPaymentsECW), 0) MTDPaymentsECW,
                                    ISNULL(SUM(F.AgingAmounteTeClinic), 0) AgingAmounteTeClinic,
                                    ISNULL(SUM(F.AgingAmounteCW), 0) AgingAmounteCW
                                FROM
                                    Financial F
                                WHERE
                                    F.DepartmentID =" + ObjFilter.Employee.DepartmentID + " " + Condition;
                    else if (ObjFilter.Department.DepartmentID == 7)
                        Query = @"SELECT 
                                   ISNULL(SUM(EnteredChargeI),0) EnteredChargeI
                                  , ISNULL(SUM(EnteredChargeP),0) EnteredChargeP
                                  
                                FROM
                                    Financial F
                                WHERE
                                    F.DepartmentID =" + ObjFilter.Employee.DepartmentID + " " + Condition;
                    else
                        Query = @"SELECT
                                   ISNULL(SUM(F.DailyCharges),0) DailyCharges,
                                    ISNULL(SUM(F.DailyPayments),0) DailyPayments,
                                   ISNULL(SUM(F.MTDCharges),0) MTDCharges,
                                  
                                   ISNULL(SUM(F.MTDPayments),0) MTDPayments,
                                   ISNULL(SUM(F.AgingAmount),0) AgingAmount
                                   FROM 
                                        Financial F 
                                    WHERE 
                                     F.DepartmentID =" + ObjFilter.Employee.DepartmentID + " " + Condition;
                }

                var Result = db.Database.SqlQuery<VMFinancials>(Query).FirstOrDefault();
                if (Result != null) return Json(Result, JsonRequestBehavior.AllowGet);
                return Json("null", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetAgingDataOfEmployee(Filters ObjFilter)
        {
            try
            {
                string Condition = "", Query = "";
                long DateTime;
                DateTime = ObjFilter.FilterDateTime.Month;
                // DateTime FilterDate = Convert.ToDateTime(ObjFilter.FilterDateTime);
                

                if (ObjFilter.AssType == 1)
                {
                    Condition += " AND F.AgingDateTime = '" +
                                 ObjFilter.FilterDateTime.ToString("yyyy-MM-dd 00:00:00.000") + "'  AND EmployeeID = " + ObjFilter.Employee.EmployeeID;
                }
                else if (ObjFilter.AssType == 2)
                {
                    Condition += " AND DATEPART(month, F.AgingDateTime) = '" + ObjFilter.FilterDateTime.Month + "'  AND EmployeeID = " + ObjFilter.Employee.EmployeeID;
                }
                else if (ObjFilter.AssType == 3)
                {
                    Condition += " AND DATEPART(month, AgingDateTime) = '" + ObjFilter.FilterDateTime.Month +
                                 "'  And  DATEPART(year, AgingDateTime) = '" + ObjFilter.FilterDateTime.Year + "'  AND EmployeeID = " + ObjFilter.Employee.EmployeeID;
                }


                Query = @"SELECT 
                                   ISNULL(SUM(A_30),0) A_30
                                  ,ISNULL(SUM(A_60),0) A_60
                                  ,ISNULL(SUM(A_90),0) A_90
                                  ,ISNULL(SUM(A_120),0) A_120
                                  ,ISNULL(SUM(A_120_up),0) A_120_up
                                  ,ISNULL(SUM(Issues_Of_120_Up_Claims),0) Issues_Of_120_Up_Claims
                                FROM
                                    Agings F
                                WHERE
                                    F.DepartmentID =" + ObjFilter.Employee.DepartmentID + " " + Condition;


                var Result = db.Database.SqlQuery<VMAgings>(Query).FirstOrDefault();
                if (Result != null) return Json(Result, JsonRequestBehavior.AllowGet);
                return Json("null", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("exception", JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
        #endregion
    }
}