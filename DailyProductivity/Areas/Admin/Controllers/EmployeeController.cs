﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DailyProductivity.App_Start;

namespace DailyProductivity.Areas.Admin.Controllers
{
    [SessionExpire, IsAdmin]

    public class EmployeeController : Controller
    {
        public ActionResult Employee()
        {
            return View();
        }
        public ActionResult Employees()
        {
            return View();
        }
        public ActionResult Manage(string id)
        {
            ViewBag.EmployeeID = Convert.ToInt64(EncryptDecrypt.Decrypt(id,true));
            return View();
        }
    }
}