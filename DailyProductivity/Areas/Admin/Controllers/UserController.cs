﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using DailyProductivity.App_Start;

namespace DailyProductivity.Areas.Admin.Controllers
{
        [SessionExpire,IsAdmin]
   // 
    public class UserController : Controller
    {
		public ActionResult AddUser(){
            
			return View();
		}
        public ActionResult EditUser(long? id)
        {
            if (id!=null || id!=0)
            {
                ViewBag.UserID = id;
               
            }
           
            return View();
        }
        public ActionResult Managers()
        {
            return View();
        }
        public ActionResult Employees()
        {
            return View();
        }
    }
}
