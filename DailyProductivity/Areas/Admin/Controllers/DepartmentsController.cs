﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DailyProductivity.App_Start;

namespace DailyProductivity.Areas.Admin.Controllers
{
    [SessionExpire, IsAdmin]

    public class DepartmentController : Controller
    {
        // GET: Admin/Departments
        public ActionResult Department()
        {
            return View();
        }
        public ActionResult Departments()
        {
            return View();
        }
        public ActionResult Manage(string id="")
        {
           
                ViewBag.DeptID = Convert.ToInt64(EncryptDecrypt.Decrypt(id,true));
           
            return View();
        }
    }
}